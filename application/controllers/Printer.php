<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Printer extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// if (!isset($_SESSION['userid'])) {
		// 	redirect(base_url() . 'masuk', 'refresh');
		// } else {
		// }
	}

	public function struk($id_transaksi = '')
	{
		$this->load->library('ciqrcode');

		$setting = $this->global->getSetting("semua");

		$this->db->select('b.*,transaksi.no_resit, transaksi.total, transaksi.bayar, transaksi.kembali, c.nama as provinsi, d.nama as kota, tu.ukuran');
		$this->db->join('transaksi_data_pelanggan b', 'b.id_transaksi = transaksi.id', 'left');
		$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
		$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');
		$this->db->join('data_tarif_ukuran tu', 'transaksi.id_size = tu.id', 'left');
		$this->db->where('transaksi.id', $id_transaksi);
		$get = $this->db->get('transaksi')->row_array();


		$detail = $this->db->query("SELECT a.*, b.ukuran  FROM transaksi_detail a 
			LEFT JOIN data_tarif_ukuran b ON a.id_size = b.id
			WHERE a.id_transaksi ='" . $id_transaksi . "' ")->result_array();

		$data['tr'] = $get;
		$data['detail'] = $detail;

		$data['tipe'] = 'Outlet';

		if ($id_agen = $this->session->userdata('id_agen')) {
			$get = $this->db->get_where('data_outlet', array('id' => $id_agen))->row_array();
			$data['tipe'] = $get['type'];
		}


		$code = $get['no_resit'];

		$qr_image = $code . '.png';
		$params['data'] = $code;
		$params['level'] = 'H';
		$params['size'] = 8;
		$params['savename'] = FCPATH . "barcode/" . $qr_image;
		if ($this->ciqrcode->generate($params)) {
			$data['img_url'] = $qr_image;
		}

		$data['setting'] = $setting;
		$this->load->view('print/transaksi_struk', $data);
	}

	public function html_to_pdf($no_resit)
	{

		$get = $this->db->query("SELECT
			e.*,
			b.nama_negara,
			c.nama AS provinsi,
			d.nama AS kota ,
			a.no_resit,
			a.total,
			a.isi,
			a.created_at,
			a.note,
			f.ukuran,
			h.mata_uang
			FROM
			transaksi a
			LEFT JOIN transaksi_data_pelanggan e ON a.id = e.id_transaksi 
			LEFT JOIN data_negara b ON e.id_negara_penerima = b.id
			LEFT JOIN t_provinsi c ON e.id_provinsi_penerima = c.id
			LEFT JOIN t_kabupaten d ON e.id_kota_penerima = d.id
			LEFT JOIN data_tarif_ukuran f ON a.id_size = f.id
			LEFT JOIN data_negara h ON a.kode_negara = h.id
			
			WHERE
			a.no_resit = '" . $no_resit . "'")->row_array();


		$data['tr'] =  $get;



		$this->load->library('pdf');

		//$this->pdf->setPaper('A4', 'potrait');

		$customPaper = array(0, 0, 283, 425);
		$this->pdf->setPaper($customPaper);

		$this->pdf->filename = "barcode-" . $get['no_resit'] . ".pdf";

		$this->pdf->load_view('print/barcode_pdf', $data);
	}

	public function invoice($id_transaksi = '')
	{

		if ($id_transaksi != '') {

			$id_user = $this->session->userdata('userid');
			$print = 'invoice';
			$this->global->create_log_print($id_transaksi, $id_user, $print);

			$this->db->select('b.*,transaksi.no_resit,transaksi.no_resit_lama, transaksi.total, transaksi.bayar, transaksi.kembali,
			 c.nama as provinsi, d.nama as kota, transaksi.created_at, transaksi.id_agen, transaksi.total, transaksi.isi, transaksi.note ,
			  transaksi.container, tu.ukuran, do.kode ');

			$this->db->join('transaksi_data_pelanggan b', 'b.id_transaksi = transaksi.id', 'left');
			$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
			$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');
			$this->db->join('data_outlet do', 'transaksi.id_agen = do.id', 'left');
			$this->db->join('data_tarif_ukuran tu', 'transaksi.id_size = tu.id', 'left');
			$this->db->where('transaksi.id', $id_transaksi);
			$get = $this->db->get('transaksi');
			//echo $this->db->last_query();exit;
			$get = $get->row_array();


			$setting = $this->global->getSetting("semua");

			$data['tr'] = $get;
			$data['setting'] = $setting;
			$this->load->view('print/invoice', $data);
		}
	}

	public function invoice_pdf($id_transaksi)
	{
		$this->db->select('b.*,transaksi.no_resit,transaksi.no_resit_lama, transaksi.total, transaksi.note, transaksi.bayar, transaksi.kembali, c.nama as provinsi, d.nama as kota, transaksi.created_at, transaksi.id_agen, transaksi.total, transaksi.isi, transaksi.container, tu.ukuran, do.kode ');

		$this->db->join('transaksi_data_pelanggan b', 'b.id_transaksi = transaksi.id', 'left');
		$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
		$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');
		$this->db->join('data_outlet do', 'transaksi.id_agen = do.id', 'left');
		$this->db->join('data_tarif_ukuran tu', 'transaksi.id_size = tu.id', 'left');
		$this->db->where('transaksi.id', $id_transaksi);
		$get = $this->db->get('transaksi');
		//echo $this->db->last_query();exit;
		$get = $get->row_array();


		$setting = $this->global->getSetting("semua");

		$data['tr'] = $get;
		$data['setting'] = $setting;

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');

		$this->pdf->filename = "barcode-" . $get['no_resit'] . ".pdf";

		$output = $this->pdf->load_view('print/invoice_pdf', $data);

		file_put_contents("barcode-" . $get['no_resit'] . ".pdf", $output);
	}

	public function gen_pdf($id_transaksi)
	{
		$this->load->library('pdf');
		$this->db->select('b.*,transaksi.no_resit,transaksi.no_resit_lama, transaksi.total, transaksi.note, transaksi.bayar, transaksi.kembali, c.nama as provinsi, d.nama as kota, transaksi.created_at, transaksi.id_agen, transaksi.total, transaksi.isi, transaksi.container, tu.ukuran, do.kode ');

		$this->db->join('transaksi_data_pelanggan b', 'b.id_transaksi = transaksi.id', 'left');
		$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
		$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');
		$this->db->join('data_outlet do', 'transaksi.id_agen = do.id', 'left');
		$this->db->join('data_tarif_ukuran tu', 'transaksi.id_size = tu.id', 'left');
		$this->db->where('transaksi.id', $id_transaksi);
		$get = $this->db->get('transaksi');
		//echo $this->db->last_query();exit;
		$get = $get->row_array();


		$setting = $this->global->getSetting("semua");

		$data['tr'] = $get;
		$data['setting'] = $setting;
		$html = $this->load->view('print/invoice_pdf', $data, true);
		$filename = 'invoce_' . $get['no_resit'];
		$this->pdf->generate($html, $filename, true, 'A4', 'portrait');
	}

	public function barcode_box($no_resit)
	{



		$get = $this->db->query("SELECT
			e.*,
			b.nama_negara,
			c.nama AS provinsi,
			d.nama AS kota ,
			a.no_resit,
			a.no_resit_lama,
			a.total,
			a.isi,
			a.created_at,
			a.note,
			f.ukuran,
			h.mata_uang
			FROM
			transaksi a
			LEFT JOIN transaksi_data_pelanggan e ON a.id = e.id_transaksi 
			LEFT JOIN data_negara b ON e.id_negara_penerima = b.id
			LEFT JOIN t_provinsi c ON e.id_provinsi_penerima = c.id
			LEFT JOIN t_kabupaten d ON e.id_kota_penerima = d.id
			LEFT JOIN data_tarif_ukuran f ON a.id_size = f.id
			LEFT JOIN data_negara h ON a.kode_negara = h.id

			WHERE
			a.no_resit = '" . $no_resit . "'")->row_array();


		$data['tr'] =  $get;

		$id_transaksi = $get['id_transaksi'];
		$id_user = $this->session->userdata('userid');
		$print = 'barcode';
		$this->global->create_log_print($id_transaksi, $id_user, $print);



		$this->load->view('print/barcode_box', $data);
	}

	public function barcode_box_kecil($no_resit)
	{

		$get = $this->db->query("SELECT
			e.*,
			b.nama_negara,
			c.nama AS provinsi,
			d.nama AS kota ,
			a.no_resit,
			a.total,
			a.isi,
			a.created_at,
			a.note,
			f.ukuran,
			h.mata_uang
			FROM
			transaksi a
			LEFT JOIN transaksi_data_pelanggan e ON a.id = e.id_transaksi 
			LEFT JOIN data_negara b ON e.id_negara_penerima = b.id
			LEFT JOIN t_provinsi c ON e.id_provinsi_penerima = c.id
			LEFT JOIN t_kabupaten d ON e.id_kota_penerima = d.id
			LEFT JOIN data_tarif_ukuran f ON a.id_size = f.id
			LEFT JOIN data_negara h ON a.kode_negara = h.id

			WHERE
			a.no_resit = '" . $no_resit . "'")->row_array();


		$data['tr'] =  $get;



		$this->load->view('print/barcode_box_kecil', $data);
	}
}

/* End of file Print.php */
/* Location: ./application/controllers/Print.php */
