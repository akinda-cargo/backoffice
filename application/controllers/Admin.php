<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userid'])) {
			redirect(base_url() . 'masuk', 'refresh');
		} else {
		}
	}

	public function index()
	{
		$this->db->select('COUNT(id) as total_transaksi, SUM(total) as total_pendapatan ');
		$this->db->where('DATE(created_at)', date('Y-m-d'));
		if ($id_agen = $this->session->userdata('id_agen')) {
			$this->db->where('id_agen', $id_agen);
		}
		$get = $this->db->get('transaksi')->row_array();

		$outlet = $this->global->outlet();
		$chart = $this->global->income_daily();

		$loading = $this->global->total_loading();

		$page['menu'] = 'dashboard';
		$sub['chart'] = true;
		$data['total_transaksi'] = $get['total_transaksi'];
		$data['total_pendapatan'] = $get['total_pendapatan'];
		$data['outlet'] = $outlet;
		$sub['chart'] = $chart;
		$data['loading'] = $loading;


		$this->load->view('admin/header', $page);
		$this->load->view('admin/home', $data);
		$this->load->view('admin/footer', $sub);
	}

	public function get_prov($id = '')
	{

		$get = $this->db->get_where('t_provinsi', array('id_negara' => $id))->result_array();

			echo '<option value="">Pilih Provinsi</option>';
		foreach ($get as $row) {
			echo "<option value='" . $row['id'] . "'>" . $row['nama'] . "</option>";
		}
	}

	public function get_kota($id = '')
	{

		$get = $this->db->get_where('t_kabupaten', array('id_prov' => $id))->result_array();

			echo '<option value="">Pilih Kota</option>';
		foreach ($get as $row) {
			echo "<option value='" . $row['id'] . "'>" . $row['nama'] . "</option>";
		}
	}

	public function get_kecamatan($id = '')
	{

		$get = $this->db->get_where('t_kecamatan', array('id_kabupaten' => $id))->result_array();

		echo '<option value="">Pilih Kecamatan</option>';
		foreach ($get as $row) {
			echo "<option value='" . $row['id'] . "'>" . $row['nama'] . "</option>";
		}
	}



	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER SLIDER
	 *
	 * Description
	 * -
	 */

	public function slider()
	{
		$list = $this->db->get('data_slider')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'slider';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/slider', $data);
		$this->load->view('admin/footer');
	}
	public function form_slider($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'slider';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_slider')->row_array();
		}
		$this->load->view('admin/header', $page);
		$this->load->view('admin/slider_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_slider()
	{
		$post = $this->input->post();

		// CONFIGURATION UPLOAD FILE
		$random_name = 'slider_' . date('YmdHis');
		$ext = '.' . pathinfo($_FILES['gambar']['name'], PATHINFO_EXTENSION);
		$config['upload_path'] = 'assets/banner/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = $random_name;
		$this->load->library('upload', $config);

		if (@$post['id']) {
			// check file
			if ($_FILES['gambar']['name']) {
				// upload file
				$this->upload->do_upload('gambar');
				$post['gambar'] = $random_name . $ext;
				@unlink('assets/banner/' . $post['old_gambar']);
				unset($post['old_gambar']);
			}

			$this->db->where('id', $post['id']);
			$this->db->update('data_slider', $post);
		} else {
			// upload file
			$this->upload->do_upload('gambar');

			// insert data
			$post['created_at'] = date('Y-m-d H:i:s');
			$post['gambar'] = $random_name . $ext;
			$this->db->insert('data_slider', $post);
		}

		$json['res'] = 'success';
		echo json_encode($json);
	}
	public function delete_slider($id = '')
	{
		$this->db->where('id', $id);
		$data = $this->db->get('data_slider')->row_array();
		@unlink('assets/banner/' . $data['gambar']);


		$this->db->where('id', $id);
		$this->db->delete('data_slider');

		redirect(base_url('admin/slider'), 'refresh');
	}


	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER NEGARA
	 *
	 * Description
	 * -
	 */

	public function negara()
	{
		$list = $this->db->get('data_negara')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'negara';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/negara', $data);
		$this->load->view('admin/footer');
	}
	public function form_negara($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'negara';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_negara')->row_array();
		}
		$this->load->view('admin/header', $page);
		$this->load->view('admin/negara_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_negara()
	{
		$post = $this->input->post();
		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('data_negara', $post);
		} else {
			$this->db->insert('data_negara', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}





	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER PROVINSI
	 *
	 * Description
	 * -
	 */

	public function provinsi()
	{
		$this->db->select('tp.id, tp.nama, dn.nama_negara');

		$this->db->join('data_negara dn', 'dn.id = tp.id_negara', 'left');
		$list = $this->db->get('t_provinsi tp')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'provinsi';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/provinsi', $data);
		$this->load->view('admin/footer');
	}
	public function form_provinsi($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'provinsi';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('t_provinsi')->row_array();
		}
		$data['list_negara'] = $this->db->get('data_negara')->result_array();
		$this->load->view('admin/header', $page);
		$this->load->view('admin/provinsi_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_provinsi()
	{
		$post = $this->input->post();
		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('t_provinsi', $post);

			$update_kota['id_negara'] = $post['id_negara'];

			$this->db->where('id_prov', $post['id']);
			$this->db->update('t_kabupaten', $update_kota);
		} else {
			$this->db->insert('t_provinsi', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}




	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER KOTA
	 *
	 * Description
	 * -
	 */

	public function kota()
	{
		$this->db->select('tk.id');
		$this->db->select('tk.nama');
		$this->db->select('tp.nama as nama_provinsi');
		$this->db->select('dn.nama_negara');

		$this->db->join('data_negara dn', 'dn.id = tk.id_negara', 'left');
		$this->db->join('t_provinsi tp', 'tp.id = tk.id_prov', 'left');
		$list = $this->db->get('t_kabupaten tk')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'kota';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/kota', $data);
		$this->load->view('admin/footer', $page);
	}
	public function form_kota($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'kota';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('t_kabupaten')->row_array();
		}
		$data['list_negara'] = $this->db->get('data_negara')->result_array();
		$data['list_prov'] = $this->db->get('t_provinsi')->result_array();
		$this->load->view('admin/header', $page);
		$this->load->view('admin/kota_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_kota()
	{
		$post = $this->input->post();
		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('t_kabupaten', $post);
		} else {
			$this->db->insert('t_kabupaten', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}

	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER KECAMATAN
	 *
	 * Description
	 * -
	 */

	public function kecamatan()
	{
		$this->db->select('tke.id');
		$this->db->select('tke.nama');
		$this->db->select('tk.nama as nama_kabupaten');
		$this->db->select('tp.nama as nama_provinsi');
		$this->db->select('dn.nama_negara');

		$this->db->join('data_negara dn', 'dn.id = tke.id_negara', 'left');
		$this->db->join('t_provinsi tp', 'tp.id = tke.id_prov', 'left');
		$this->db->join('t_kabupaten tk', 'tk.id = tke.id_kabupaten', 'left');
		$list = $this->db->get('t_kecamatan tke')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'kecamatan';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/kecamatan', $data);
		$this->load->view('admin/footer');
	}
	public function form_kecamatan($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'kecamatan';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('t_kecamatan')->row_array();
		}
		$data['list_negara'] = $this->db->get('data_negara')->result_array();
		$data['list_prov'] = $this->db->get('t_provinsi')->result_array();
		$data['list_kab'] = $this->db->get('t_kabupaten')->result_array();
		$this->load->view('admin/header', $page);
		$this->load->view('admin/kecamatan_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_kecamatan()
	{
		$post = $this->input->post();
		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('t_kecamatan', $post);
		} else {
			$this->db->insert('t_kecamatan', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}


	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER OUTLET
	 *
	 * Description
	 * -
	 */

	public function outlet()
	{
		$this->db->where('type_outlet', 'Outlet');
		$list = $this->db->get('data_outlet')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'outlet';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/outlet', $data);
		$this->load->view('admin/footer');
	}
	public function form_outlet($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'outlet';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_outlet')->row_array();
		}
		$data['list_negara'] = $this->db->get('data_negara')->result_array();
		$data['list_prov'] = $this->db->get('t_provinsi')->result_array();
		$data['list_kab'] = $this->db->get('t_kabupaten')->result_array();
		$this->load->view('admin/header', $page);
		$this->load->view('admin/outlet_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_outlet()
	{
		$post = $this->input->post();
		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('data_outlet', $post);
		} else {
			$post['created_at'] = date('Y-m-d H:i:s');
			$post['id_user'] = $this->session->userdata('userid');
			$post['type_outlet'] = 'Outlet';
			$this->db->insert('data_outlet', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}



	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER AGEN
	 *
	 * Description
	 * -
	 */

	public function agen()
	{
		$this->db->where('type_outlet', 'Agen');
		$list = $this->db->get('data_outlet')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'agen';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/agen', $data);
		$this->load->view('admin/footer');
	}
	public function form_agen($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'agen';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_outlet')->row_array();
		}
		$data['list_negara'] = $this->db->get('data_negara')->result_array();
		$data['list_prov'] = $this->db->get('t_provinsi')->result_array();
		$data['list_kab'] = $this->db->get('t_kabupaten')->result_array();
		$this->load->view('admin/header', $page);
		$this->load->view('admin/agen_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_agen()
	{
		$post = $this->input->post();

		if($_FILES['logo']['name']){
			$random_name = 'agen_' . date('YmdHis');
			$ext = '.' . pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
			$config['upload_path'] = 'assets/uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name'] = $random_name;
			$this->load->library('upload', $config);

			$this->upload->do_upload('logo');
			$post['logo'] = $random_name . $ext;
		}
		

		if (@$post['id']) {


			$this->db->where('id', $post['id']);
			$this->db->update('data_outlet', $post);
		} else {

			// insert data
			$post['created_at'] = date('Y-m-d H:i:s');
			$post['id_user'] = $this->session->userdata('userid');
			$post['type_outlet'] = 'Agen';
			$this->db->insert('data_outlet', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}



	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER MEMBER
	 *
	 * Description
	 * text
	 */
	public function member()
	{
// 		if ($this->session->userdata('level') > 1) {
// 			$this->db->where('id_agen', $this->session->userdata('id_agen'));
// 		}

		$list = $this->db->get('data_users')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'member';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/member', $data);
		$this->load->view('admin/footer');
	}
	public function form_member($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'member';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_users')->row_array();
		}
		$data['list_negara'] = $this->db->get('data_negara')->result_array();
		$data['list_prov'] = $this->db->get('t_provinsi')->result_array();
		$data['list_kab'] = $this->db->get('t_kabupaten')->result_array();
		$data['list_kec'] = $this->db->get('t_kecamatan')->result_array();

		//var_dump($data['list_kec']);exit;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/member_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_member()
	{
		$post = $this->input->post();
		$this->db->where('id', $post['id_negara']);
		$get_negara = $this->db->get('data_negara')->row_array();
		$post['kode_negara'] = $get_negara['kode_negara'];

		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('data_users', $post);
		} else {
			$post['created_at'] = date('Y-m-d H:i:s');
			$post['id_user'] = $this->session->userdata('userid');

			if ($this->session->userdata('id_agen')) {

				$post['id_agen'] = $this->session->userdata('id_agen');
			}

			$this->db->insert('data_users', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}



	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER RATES
	 *
	 * Description
	 * -
	 */
	public function rates()
	{
		$list = $this->db->get('data_tarif_ukuran')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'rates';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/rates', $data);
		$this->load->view('admin/footer');
	}
	public function form_rates($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'rates';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_tarif_ukuran')->row_array();
		}
		$this->load->view('admin/header', $page);
		$this->load->view('admin/rates_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_rates()
	{
		$post = $this->input->post();

		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('data_tarif_ukuran', $post);
		} else {
			$post['created_at'] = date('Y-m-d H:i:s');
			$this->db->insert('data_tarif_ukuran', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}



	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER ARMADA
	 *
	 * Description
	 * -
	 */
	public function armada()
	{
		$list = $this->db->get('data_armada')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'armada';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/armada', $data);
		$this->load->view('admin/footer');
	}
	public function form_armada($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'armada';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_armada')->row_array();
		}
		$this->load->view('admin/header', $page);
		$this->load->view('admin/armada_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_armada()
	{
		$post = $this->input->post();

		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('data_armada', $post);
		} else {
			$this->db->insert('data_armada', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}



	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER DELIVERY STATUS
	 *
	 * Description
	 * -
	 */

	public function delivery_status()
	{
		$list = $this->db->get('data_status_pengiriman')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'delivery_status';
		$data['list'] = $list;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/delivery_status', $data);
		$this->load->view('admin/footer');
	}
	public function form_delivery_status($id = '')
	{
		$page['menu'] = 'master';
		$page['sub_menu'] = 'delivery_status';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('data_status_pengiriman')->row_array();
		}
		$this->load->view('admin/header', $page);
		$this->load->view('admin/delivery_status_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_delivery_status()
	{
		$post = $this->input->post();

		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('data_status_pengiriman', $post);
		} else {
			$this->db->insert('data_status_pengiriman', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}



	/**
	 * # ------------------------------------------------------------------ 
	 * * MASTER EMPLOYES
	 *
	 * Description
	 * -
	 */
	public function employes()
	{
		$this->db->where('id', $this->session->userdata('userid'));
		$user_log = $this->db->get('admin')->row_array();

		if ($user_log['level'] > 1) {
			$this->db->where('id_agen', $user_log['id_agen']);
		}
		$this->db->select('ad.*, lv.nama_level, do.nama as outlet');
		$this->db->join('level lv', 'lv.id = ad.level', 'left');
		$this->db->join('data_outlet do', 'do.id = ad.id_agen', 'left');
		$list = $this->db->get('admin ad')->result_array();
		$page['menu'] = 'master';
		$page['sub_menu'] = 'employes';
		$data['list'] = $list;
		$data['user_log'] = $user_log;
		$this->load->view('admin/header', $page);
		$this->load->view('admin/employes', $data);
		$this->load->view('admin/footer');
	}
	public function form_employes($id = '')
	{
		$this->db->where('id', $this->session->userdata('userid'));
		$user = $this->db->get('admin')->row_array();

		$page['menu'] = 'master';
		$page['sub_menu'] = 'employes';
		if ($id == '') {
			$data = [];
		} else {
			$this->db->where('id', $id);
			$data = $this->db->get('admin')->row_array();
		}

		if ($user['level'] > 1) {
			$this->db->where('id !=', '1');
		}
		$data['list_level'] = $this->db->get('level')->result_array();
		if ($user['level'] > 1) {
			$this->db->where('id', $user['id_agen']);
		}
		$data['list_outlet'] = $this->db
		->select('id, nama')
		->get('data_outlet')
		->result_array();

		$this->load->view('admin/header', $page);
		$this->load->view('admin/employes_form', $data);
		$this->load->view('admin/footer');
	}
	public function save_employes()
	{
		$post = $this->input->post();

		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('admin', $post);
		} else {
			$post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
			$this->db->insert('admin', $post);
		}
		$json['res'] = 'success';
		echo json_encode($json);
	}
	public function hash()
	{
		echo password_hash('wahyu', PASSWORD_DEFAULT);
	}
}
