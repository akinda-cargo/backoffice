<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userid'])) {
			redirect(base_url() . 'masuk', 'refresh');
		} else {
			$this->load->model('kasirMdl');
			$this->load->library('pdf');
		}
	}

	public function index()
	{
		$id_user = $this->session->userdata('userid');
		$data['transaksi'] = $this->db->query("SELECT *, a.id as id_temp FROM transaksi_temp a LEFT JOIN data_tarif_ukuran b ON a.id_size = b.id WHERE a.id_user=" . $id_user)->result_array();
		$data['tarif'] = $this->db->get_where('data_tarif_ukuran', array('flag' => 1))->result_array();
		$data['armada'] = $this->db->get('data_armada')->result_array();

		$this->load->view('transaksi/index', $data);
	}

	public function list_data($id_transaksi = '')
	{
		if ($id_transaksi != '') {
			$data['id_transaksi'] = $id_transaksi;
		}

		$page['menu'] = 'transaksi';
		$this->db->select('a.*, d.mata_uang,a.isi, d.nama_negara as negara_asal , c.nama_negara as negara_tujuan , e.alamat_pengirim, e.alamat_penerima, e.nama_pengirim, e.nama_penerima, b.nama_status');
		$this->db->join('data_status_pengiriman b', 'a.status = b.id', 'left');
		$this->db->join('data_negara d', 'a.kode_negara = d.id', 'left');
		$this->db->join('data_negara c', 'b.id = a.kode_negara_tujuan', 'left');
		$this->db->join('transaksi_data_pelanggan e', 'e.id_transaksi = a.id', 'left');

		if ($id_agen = $this->session->userdata('id_agen')) {
			$this->db->where('a.id_agen', $id_agen);
		}
		
		if ($post = $this->input->post()) {
			if ($post['tgl_awal'] != '' && $post['tgl_akhir'] != '') {
				$tgl_awal = date('Y-m-d', strtotime($post['tgl_awal']));
				$tgl_akhir = date('Y-m-d', strtotime($post['tgl_akhir']));
				$this->db->where('DATE(a.created_at) >= ', $tgl_awal);
				$this->db->where('DATE(a.created_at) <= ', $tgl_akhir);
				$data['filter']['tgl_awal'] = $tgl_awal;
				$data['filter']['tgl_akhir'] = $tgl_akhir;
			} else {
				$data['error'] = "Tanggal awal dan akhir harus diisi!";
				if ($post['tgl_awal'] < $post['tgl_akhir']) {
					$data['error'] = "Tanggal awal harus lebih kecil dari tanggal akhir!";
				}
			}
			
		}else{
			$now = date('Y-m-d');
			$this->db->where('DATE(a.created_at)', $now);
		}

		$this->db->group_by('a.id');

		$get = $this->db->get('transaksi a');



		$data['transaksi'] = $get->result_array();



		$this->load->view('admin/header', $page);
		$this->load->view('transaksi/list', $data);
		$this->load->view('admin/footer');
	}

	public function load_transaksi_status($id_transaksi = '')
	{
		$this->db->select('a.created_at, d.nama_status, b.no_resit, c.nama as nama_petugas, a.note');

		$this->db->join('transaksi b', 'a.id_transaksi = b.id', 'left');
		$this->db->join('admin c', 'a.id_user = c.id', 'left');
		$this->db->join('data_status_pengiriman d', 'a.status = d.id', 'left');

		$this->db->where('b.id', $id_transaksi);
		$this->db->group_by('a.status');
		$get = $this->db->get('transaksi_status a');

		$tr = array();
		foreach ($get->result_array() as $row) {
			$tr[] = array(
				'tgl_transaksi' => date('d-m-Y H:i', strtotime($row['created_at'])),
				'status'	=> $row['nama_status'],
				'no_resit'	=> $row['no_resit'],
				'nama_petugas'	=> $row['nama_petugas'],
				'note'		=> $row['note']
			);
		}


		$data['transaksi_status'] = $tr;
		$data['id_transaksi'] = $id_transaksi;
		$this->load->view('transaksi/load_transaksi_status', $data);
	}

	public function update_status()
	{
		if ($post = $this->input->post()) {

			//ambil id transaksi
			$get = $this->db->get_where('transaksi', array('id' => $post['id_transaksi']));

			if ($get->num_rows() > 0) {

				$get = $get->row_array();
				$id_user = $this->session->userdata('userid');

				$data['id_transaksi'] = $get['id'];
				$data['kode_barang'] = $post['no_resit'];
				$data['status'] = $post['status'];
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['id_user'] = $id_user;
				$data['note'] = $post['note'];

				if ($this->db->insert('transaksi_status', $data)) {

					$ptg = $this->db->get_where("admin",array('id' => $id_user))->row_array();

					$upp['status'] = $post['status'];

					if ($this->db->update('transaksi', $upp, array('id' => $post['id_transaksi']))) {

						if ($id_user == '11' || $id_user == '12' || $id_user == '13') {

							$this->db->select('c.wa_penerima, b.pesan_wa, c.wa_pengirim');
							$this->db->join('data_status_pengiriman b', 'a.status = b.id', 'LEFT');
							$this->db->join('transaksi_data_pelanggan c', 'a.id = c.id_transaksi', 'LEFT');
							$this->db->where('a.id', $get['id']);
							$trans = $this->db->get('transaksi a')->row_array();

							$no = $trans['wa_pengirim'];
							$pesan = $trans['pesan_wa'] . "\n Note: " . $post['note'] . "\n\nUpdated by : ".$ptg['nama']." ".date('d-m-Y H:i');
							
							$this->global->send_text_wa($no, $pesan);

						}
					}
				}
			}
		}
	}

	public function detail($id_transaksi = '')
	{

		$data['tr'] = $this->db->query("SELECT a.* , b.nama_negara , c.nama as provinsi, d.nama as kota, e.total, e.isi, f.ukuran, g.nama as kecamatan FROM transaksi_data_pelanggan a 
			LEFT JOIN data_negara b ON a.id_negara_penerima = b.id
			LEFT JOIN t_provinsi c ON a.id_provinsi_penerima = c.id
			LEFT JOIN t_kabupaten d ON a.id_kota_penerima = d.id 
			LEFT JOIN t_kecamatan g ON a.id_kecamatan_penerima = g.id 
			LEFT JOIN transaksi e ON a.id_transaksi = e.id
			LEFT JOIN data_tarif_ukuran f ON e.id_size = f.id WHERE a.id_transaksi=" . $id_transaksi)->row_array();


		$this->load->view('transaksi/load_detail_transaksi', $data);
	}

	// public function add_temp()
	// {
	// 	$id_user = $this->session->userdata('userid');

	// 	$this->db->where('adm.id', $id_user);
	// 	$this->db->select('do.kode');
	// 	$this->db->select('dn.kode_negara , adm.id_agen');
	// 	$this->db->join('data_outlet do', 'adm.id_agen = do.id', 'left');
	// 	$this->db->join('data_negara dn', 'do.id_negara = dn.id', 'left');
	// 	$get_outlet = $this->db->get('admin adm')->row_array();

	// 	if (empty($get_outlet['kode_negara'])) {
	// 		$get_outlet['kode_negara'] = "IDN";
	// 	}
	// 	if (empty($get_outlet['kode'])) {
	// 		$get_outlet['kode'] = "SPRADM";
	// 	}

	// 	if ($post = $this->input->post()) {
	// 		$data['kode_barang'] = $get_outlet['kode_negara'] . $post['container'] . $get_outlet['kode'];
	// 		$data['id_size'] = $post['id_size'];
	// 		$data['container'] = $post['container'];
	// 		$data['isi'] = $post['isi'];
	// 		$data['harga'] = $post['harga'];
	// 		$data['id_user'] = $this->session->userdata('userid');
	// 		$data['id_agen'] = $get_outlet['id_agen'];

	// 		if ($this->db->insert('transaksi_temp', $data)) {

	// 			$id_user = $this->session->userdata('userid');
	// 			$tr['transaksi'] =  $this->db->query("SELECT a.*, b.ukuran, a.id as id_temp FROM transaksi_temp a LEFT JOIN data_tarif_ukuran b ON a.id_size = b.id WHERE a.id_user=" . $id_user)->result_array();
	// 			$this->load->view('transaksi/load_barang', $tr);
	// 		}
	// 	}
	// }

	// public function del_temp()
	// {
	// 	$key = $this->input->post('key');
	// 	if ($this->db->delete('transaksi_temp', array('id' => $key))) {

	// 		$id_user = $this->session->userdata('userid');
	// 		$tr['transaksi'] =  $this->db->query("SELECT a.*, b.ukuran, a.id as id_temp FROM transaksi_temp a LEFT JOIN data_tarif_ukuran b ON a.id_size = b.id WHERE a.id_user=" . $id_user)->result_array();
	// 		$this->load->view('transaksi/load_barang', $tr);
	// 	}
	// }


	public function get_detail_struk()
	{
		if ($post = $this->input->post()) {
			$id_transaksi = $post['id_transaksi'];

			$data['tr'] = $this->db->query("SELECT a.*, b.nama as provinsi_penerima, d.no_resit, c.nama as kota_penerima, e.ukuran, d.isi, d.total FROM transaksi_data_pelanggan a 
				LEFT JOIN transaksi d ON a.id_transaksi = d.id
				LEFT JOIN t_provinsi b ON a.id_provinsi_penerima = b.id
				LEFT JOIN t_kabupaten c ON a.id_kota_penerima = c.id 
				LEFT JOIN data_tarif_ukuran e ON d.id_size = e.id
				WHERE a.id_transaksi=" . $id_transaksi)->row_array();


			$data['id_transaksi'] = $id_transaksi;

			$this->load->view('transaksi/load_struk', $data);
		}
	}

	public function add_pelanggan()
	{
		if ($post = $this->input->post()) {
			$field = $post['indentitas'];
			$value = $post['value'];

			if ($field == 'KTP') {
				$coloum = 'nik';
			} else {
				$coloum = 'no_pasport';
			}
			$get = $this->db->get_where('data_users', array($coloum => $value));

			if ($get->num_rows() > 0) {
				$json['status'] = false;
				$json['msg'] = 'Data sudah ada';

				echo json_encode($json);
			} else {

				if ($field == 'KTP') {
					$data['nik'] = $post['value'];
				} else {
					$data['no_pasport'] = $post['value'];
				}


				if ($post['kode_negara'] != '') {
					$get_neg = $this->db->get_where('data_negara', array('id' => $post['kode_negara']))->row_array();
					$negara = $get_neg['kode_negara'];
					$kode_tlp = $get_neg['kode_tlp'];
				} else {
					$negara = '';
					$kode_tlp = '62';
				}
				
				$nomer = intval($post['sender_wa']);
				$nomer_wa = substr($nomer, 0, 2) != $kode_tlp ? $kode_tlp . $nomer : "" . $nomer;

				$tlp = intval($post['sender_tlp']);
				$nomer_tlp = substr($nomer, 0, 2) != $tlp ? $kode_tlp . $tlp : "" . $tlp;


				$data['nama_lengkap'] = $post['sender_nama'];
				$data['no_tlp'] = $nomer_tlp;
				$data['no_wa'] = $nomer_wa;
				$data['alamat_lengkap'] = $post['sender_alamat'];
				$data['kode_negara'] = $negara;
				$data['id_user'] = $this->session->userdata('userid');
				$data['flag'] = 1;
				$data['created_at'] = date('Y-m-d H:i:s');

				if ($this->db->insert('data_users', $data)) {
					$json['status'] = true;
					$json['msg'] = 'Data Berhasil di simpan';

					echo json_encode($json);
				}
			}
		}
	}
	
	public function add_penerima()
	{
		if ($post = $this->input->post()) {
			$field = $post['indentitas'];
			$value = $post['value'];

			if ($field == 'KTP') {
				$coloum = 'nik';
			} else {
				$coloum = 'no_pasport';
			}
			$get = $this->db->get_where('data_users', array($coloum => $value));

			if ($get->num_rows() > 0) {
				$json['status'] = false;
				$json['msg'] = 'Data sudah ada';

				echo json_encode($json);
			} else {

				if ($field == 'KTP') {
					$data['nik'] = $post['value'];
				} else {
					$data['no_pasport'] = $post['value'];
				}

				if ($post['kode_negara'] != '') {
					$get_neg = $this->db->get_where('data_negara', array('id' => $post['kode_negara']))->row_array();
					$negara = $get_neg['kode_negara'];
					$kode_tlp = $get_neg['kode_tlp'];
				} else {
					$negara = '';
					$kode_tlp = '62';
				}
				
				$nomer = intval($post['wa']);
				$nomer_wa = substr($nomer, 0, 2) != $kode_tlp ? $kode_tlp . $nomer : "" . $nomer;

				$tlp = intval($post['tlp']);
				$nomer_tlp = substr($nomer, 0, 2) != $tlp ? $kode_tlp . $tlp : "" . $tlp;


				$data['nama_lengkap'] = $post['nama'];
				$data['no_tlp'] = $nomer_wa;
				$data['no_wa'] = $nomer_tlp;
				$data['alamat_lengkap'] = $post['alamat'];
				$data['kode_negara'] = $negara;
				$data['id_negara'] = $post['kode_negara'];
				$data['id_provinsi'] = $post['id_prov'];
				$data['id_kabupaten'] = $post['id_kota'];
				$data['id_kecamatan'] = $post['id_kecamatan'];
				$data['rt'] = $post['rt'];
				$data['rw'] = $post['rw'];
				$data['desa'] = $post['desa'];
				$data['dusun'] = $post['dusun'];
				$data['kode_pos'] = $post['pos'];
				$data['id_user'] = $this->session->userdata('userid');
				$data['flag'] = 1;
				$data['created_at'] = date('Y-m-d H:i:s');

				if ($this->db->insert('data_users', $data)) {
					$json['status'] = true;
					$json['msg'] = 'Data Berhasil di simpan';

					echo json_encode($json);
				}
			}
		}
	}

	public function add_transaksi()
	{
		if ($post = $this->input->post()) {

			$id_user = $this->session->userdata('userid');

			$this->db->where('adm.id', $id_user);
			$this->db->select('do.kode');
			$this->db->select('dn.kode_negara , adm.id_agen');
			$this->db->join('data_outlet do', 'adm.id_agen = do.id', 'left');
			$this->db->join('data_negara dn', 'do.id_negara = dn.id', 'left');
			$get_outlet = $this->db->get('admin adm')->row_array();

			if (empty($get_outlet['kode_negara'])) {
				$get_outlet['kode_negara'] = "IDN";
			}
			if (empty($get_outlet['kode'])) {
				$get_outlet['kode'] = "SPRADM";
			}

			$this->db->select('MAX(id) as box');
			$box_number = $this->db->get('transaksi')->row_array();
			$box = $box_number['box'] + 1;

			$data['kode_transaksi'] = 'TR' . date('YmdHis');
			$data['no_resit'] = $get_outlet['kode_negara'] . $post['container'] . $get_outlet['kode'] . $box;
			$data['container'] = $post['container'];
			$data['kode_negara'] = $post['id_negara_pengirim'];
			$data['kode_negara_tujuan'] = $post['id_negara_penerima'];
			$data['bayar'] = $post['bayar'];
			$data['total'] = $post['harga'];
			$data['kembali'] = $post['bayar'] - $post['harga'];
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['status'] = 1;
			$data['id_user'] = $this->session->userdata('userid');
			$data['id_penerima'] = $post['id_penerima'];
			$data['id_pengirim'] = $post['id_pengirim'];
			$data['platform'] = 'web';
			$data['isi'] = $post['isi'];
			$data['note'] = $post['note'];
			$data['id_size']	= $post['id_size'];
			$data['no_resit_lama'] = $post['no_resit_lama'];

			if ($id_agen = $this->session->userdata('id_agen')) {
				$data['id_agen'] = $id_agen;
			}

			if ($this->db->insert('transaksi', $data)) {
				$id_transaksi = $this->db->insert_id();

				$this->load->library('ciqrcode');
				$code = $data['no_resit'];

				$qr_image = $code . '.png';
				$params['data'] = $code;
				$params['level'] = 'H';
				$params['size'] = 8;
				$params['savename'] = FCPATH . "barcode/" . $qr_image;
				if ($this->ciqrcode->generate($params)) {
				}


				//insert data pelanggan
				$pel['id_transaksi'] = $id_transaksi;
				$pel['id_pengirim'] = $post['id_pengirim'];
				$pel['identity_type'] = $post['identity_type'];
				$pel['no_pasport'] = $post['pasport_pengirim'];
				$pel['nama_pengirim'] = $post['nama_pengirim'];
				$pel['hp_pengirim'] = $post['tlp_pengirim'];
				$pel['wa_pengirim'] = $post['wa_pengirim'];
				$pel['alamat_pengirim'] = $post['alamat_pengirim'];
				$pel['id_penerima'] = $post['id_penerima'];
				$pel['identity_type_penerima'] = $post['identity_type_penerima'];
				$pel['no_pasport_penerima'] = $post['pasport_penerima'];
				$pel['nama_penerima'] = $post['nama_penerima'];
				$pel['hp_penerima'] = $post['tlp_penerima'];
				$pel['wa_penerima'] = $post['wa_penerima'];
				$pel['id_negara_penerima'] = $post['id_negara_penerima'];


				$pel['desa_penerima'] = $post['desa_penerima'];
				$pel['dusun_penerima'] = $post['dusun_penerima'];
				$pel['rw_penerima'] = $post['rw_penerima'];
				$pel['rt_penerima'] = $post['rt_penerima'];
				$pel['kode_pos_penerima'] = $post['pos_penerima'];
				$pel['alamat_penerima'] = $post['alamat_penerima'];

				if (isset($post['id_kecamatan_penerima'])) {
					$pel['id_kecamatan_penerima'] = $post['id_kecamatan_penerima'];
				}

				if (isset($post['id_kota_penerima'])) {
					$pel['id_kota_penerima'] = $post['id_kota_penerima'];
				}

				if (isset($post['id_provinsi_penerima'])) {
					$pel['id_provinsi_penerima'] = $post['id_provinsi_penerima'];
				}

				$this->db->insert('transaksi_data_pelanggan', $pel);

				//insert log status
				$log['id_transaksi'] = $id_transaksi;
				$log['created_at'] = date('Y-m-d H:i:s');
				$log['status'] = 1;
				$log['id_user'] = $id_user;
				$log['note'] = $post['note'];

				$this->db->insert('transaksi_status', $log);

				$json['status'] = true;
				$json['msg'] = 'Berhasil melakukan transaksi';
				$json['id_transaksi'] = $id_transaksi;
				$json['wa_pengirim'] = $post['wa_pengirim'];
				$json['no_resit'] = $data['no_resit'];

				if ($this->session->userdata('id_agen')) {
					$json['agen'] = true;
				}

				$id_user = $this->session->userdata('userid');
				$print = 'invoice';
				$this->global->create_log_print($id_transaksi, $id_user, $print);

				$this->gen_pdf($id_transaksi);




				echo json_encode($json);
			}
		}





		// 	$detail = $this->db->get_where('transaksi_temp', array('id_user' => $id_user))->result_array();


		// 	$total = 0;
		// 	foreach ($detail as $row) {
		// 		$total += $row['harga'];

		// 		$add['id_transaksi'] = $id_transaksi;
		// 		$add['container'] = $row['container'];
		// 		$add['kode_barang'] = rand();
		// 		$add['id_size'] = $row['id_size'];
		// 		$add['tarif'] = $row['harga'];
		// 		$add['isi'] = $row['isi'];
		// 		$add['status'] = '1';

		// 		$this->db->insert('transaksi_detail', $add);

		// 	}


		// 	$upp['total'] = $total;
		// 	$upp['kembali'] = $post['bayar'] - $total;

		// 	if ($this->db->update('transaksi', $upp, array('id' => $id_transaksi))) {
		// 		$this->db->delete('transaksi_temp', array('id_user' => $id_user));


		// }
		//}

	}

	public function gen_pdf($id_transaksi)
	{

		$this->db->select('b.*,transaksi.no_resit, transaksi.total, transaksi.bayar, transaksi.kembali, c.nama as provinsi, d.nama as kota, transaksi.created_at, transaksi.id_agen, transaksi.total, transaksi.isi, transaksi.container, tu.ukuran, do.kode ');

		$this->db->join('transaksi_data_pelanggan b', 'b.id_transaksi = transaksi.id', 'left');
		$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
		$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');
		$this->db->join('data_outlet do', 'transaksi.id_agen = do.id', 'left');
		$this->db->join('data_tarif_ukuran tu', 'transaksi.id_size = tu.id', 'left');
		$this->db->where('transaksi.id', $id_transaksi);
		$get = $this->db->get('transaksi');
		//echo $this->db->last_query();exit;
		$get = $get->row_array();



		$setting = $this->global->getSetting("semua");

		$data['tr'] = $get;
		$data['setting'] = $setting;
		$html = $this->load->view('print/invoice_pdf', $data, true);
		$filename = 'invoce_' . $get['no_resit'];

		$this->load->library('pdf');
		$this->pdf->download($html, $filename, true, 'A4', 'portrait');
	}

	public function send_wa_transaksi($id_transaksi = '')
	{



		$this->db->select('a.total, a.no_resit, a.isi , b.*, a.created_at, c.nama as provinsi, d.nama as kota, tu.ukuran');
		$this->db->join('transaksi_data_pelanggan b', 'a.id = b.id_transaksi', 'LEFT');
		$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
		$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');
		$this->db->join('data_tarif_ukuran tu', 'a.id_size = tu.id', 'left');
		$this->db->where('a.id', $id_transaksi);
		$row = $this->db->get('transaksi a')->row_array();

		$no = $row['wa_pengirim'];
		$pesan =  "Terimakasih atas kepercayaan kepada syarikat AKINDA, kami akan info secara berkala status barang yg dikirim \n No Resi : " . $row['no_resit'] . " dengan Total : " . $row['total'] . " , dan di terima pada " . date('d-m-Y H:i', strtotime($row['created_at']));

		$userid = $this->session->userdata('userid');
		if ($userid == '11' || $userid == '12' || $userid == '13') {
			$this->global->send_text_wa($no, $pesan);
		}

		$filename = 'invoce_' . $row['no_resit'];
		$url_document = base_url() . 'pdf/' . $filename . '.pdf';
		$text = "No Resit: " . $row['no_resit'] . " \n Tarif: " . $row['total'] . "\n Ukuran " . $row['ukuran'] . ": " . $row['isi'];
		$text .= "*Pengirim* \n Nama Penerima: " . $row['nama_penerima'] . "\n No Telp/Whatsapp: " . $row['hp_penerima'] . "/" . $row['wa_penerima'] . "\n Provinsi/Kota: " . $row['provinsi'] . "/" . $row['kota'] . "\n";
		$text .= "*Pengirim* \n Nama Pengirim: " . $row['nama_pengirim'] . "\n No Telp/Whatsapp" . $row['hp_pengirim'] . "/" . $row['wa_pengirim'] . "\n Alamat Lengkap:" . $row['alamat_pengirim'];


		$userid = $this->session->userdata('userid');
		if ($userid == '11' || $userid == '12' || $userid == '13') {
			$this->send_dokumen_wa($no, $url_document, $text);
		}
	}

	public function send_dokumen_wa($no, $url_dokumen, $pesan)
	{

		return $this->global->send_file_wa($no, $pesan, $url_dokumen);
	}

	public function fetch()
	{
		$this->kasirMdl->customer_fetch($this->uri->segment(3));
	}

	public function cari_pelanggan()
	{
		$post = $this->input->post();
		$get = $this->db->get_where('data_users', array('id' => $post['code']))->row_array();

		echo json_encode($get);
	}
	public function load_edit_trans($id)
	{
		$this->db->select('a.*, b.*, a.id as id_trans');
		$this->db->join('transaksi_data_pelanggan b', 'a.id = b.id_transaksi', 'left');
		$this->db->where('a.id', $id);
		$data = $this->db->get('transaksi a')->row_array();
		$this->load->view('transaksi/load_edit_transaksi', $data);
	}
	public function load_edit_box($id)
	{
		$this->db->where('id', $id);
		$data['transaksi'] = $this->db->get('transaksi')->row_array();

		$this->db->where('id_transaksi', $id);
		$data['transaksi_detail'] = $this->db->get('transaksi_detail')->result_array();
		$this->load->view('transaksi/load_edit_box', $data);
	}
	public function act_edit_trans()
	{
		$post = $this->input->post();
		
		if($post['no_resit_before'] == $post['no_resit_lama']){

		}else
		{
			$cek = $this->db->get_where('transaksi',array('no_resit_lama' => $post['no_resit_lama']));
			if($cek->num_rows() > 0){
				echo 'No Resit Lama sudah terdaftar';
				die;
			}
		}

		$dTrans['kode_negara'] = $post['id_pengirim'];
		$dTrans['kode_negara_tujuan'] = $post['id_negara_penerima'];
		$dTrans['note'] = $post['note'];
		$dTrans['no_resit_lama'] = $post['no_resit_lama'];

		$this->db->where('id', $post['id']);
		$this->db->update('transaksi', $dTrans);
		
		if(isset($post['identity_type_penerima'])){
			$dDetailTrans['identity_type_penerima'] = $post['identity_type_penerima'];
		}
		
		if(isset($post['id_kota_penerima'])){
			$dDetailTrans['id_kota_penerima'] = $post['id_kota_penerima'];
		}

		$dDetailTrans['identity_type'] = $post['identity_type'];
		$dDetailTrans['no_pasport'] = $post['no_pasport'];
		$dDetailTrans['nama_pengirim'] = $post['nama_pengirim'];
		$dDetailTrans['hp_pengirim'] = $post['hp_pengirim'];
		$dDetailTrans['wa_pengirim'] = $post['wa_pengirim'];
		$dDetailTrans['alamat_pengirim'] = $post['alamat_pengirim'];

		$dDetailTrans['no_pasport_penerima'] = $post['no_pasport_penerima'];
		$dDetailTrans['nama_penerima'] = $post['nama_penerima'];
		$dDetailTrans['hp_penerima'] = $post['hp_penerima'];
		$dDetailTrans['wa_penerima'] = $post['wa_penerima'];
		$dDetailTrans['id_negara_penerima'] = $post['id_negara_penerima'];
		$dDetailTrans['id_provinsi_penerima'] = $post['id_provinsi_penerima'];

		$dDetailTrans['kode_pos_penerima'] = $post['kode_pos_penerima'];
		$dDetailTrans['alamat_penerima'] = $post['alamat_penerima'];



		$this->db->where('id_transaksi', $post['id']);
		$this->db->update('transaksi_data_pelanggan', $dDetailTrans);
		
		echo 1;
		
	}
	public function act_edit_box()
	{
		$post = $this->input->post();
		foreach ($post['note'] as $key => $value) {
			$dUpdate['note'] = $value;

			$this->db->where('id', $key);
			$this->db->update('transaksi_detail', $dUpdate);
		}
	}
}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */
