<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userid'])) {
			redirect(base_url() . 'masuk', 'refresh');
		} else {
		}
	}

	public function index()
	{
		redirect(base_url('laporan/pengiriman'), 'refresh');
	}

	public function manifest()
	{
		$data['error'] = '';
		$page['menu'] = 'laporan';
		$page['sub_menu'] = 'manifest';
		$page['datatables'] = true;

		$this->db->select('a.no_resit');
		$this->db->select('a.no_resit_lama');
		$this->db->select('a.note');
		$this->db->select('b.nama_status');
		$this->db->select('a.container');
		$this->db->select('e.nama as kota_tujuan');
		$this->db->select('f.ukuran');
		$this->db->select('a.created_at');
		$this->db->select('c.nama_pengirim');
		$this->db->select('c.no_pasport');
		$this->db->select('c.alamat_pengirim');
		$this->db->select('c.hp_pengirim');
		$this->db->select('c.nama_penerima');
		$this->db->select('c.alamat_penerima');
		$this->db->select('c.alamat_penerima');
		$this->db->select('c.hp_penerima');
		$this->db->select('a.isi');
		$this->db->select('g.nama as nama_provinsi, h.nama as nama_kecamatan, c.desa_penerima, c.rt_penerima, c.rw_penerima, c.dusun_penerima');

		$this->db->join('data_status_pengiriman b', 'a.status = b.id', 'left');
		$this->db->join('transaksi_data_pelanggan c', 'a.id = c.id_transaksi', 'left');
		$this->db->join('data_negara d', 'a.kode_negara = d.id', 'left');
		$this->db->join('t_kabupaten e', 'c.id_kota_penerima = e.id', 'left');
		$this->db->join('data_tarif_ukuran f', 'a.id_size = f.id', 'left');
		$this->db->join('t_provinsi g', 'c.id_provinsi_penerima = g.id', 'left');
		$this->db->join('t_kecamatan h', 'c.id_kecamatan_penerima = h.id', 'left');

		if ($post = $this->input->post()) {
			//  var_dump($post);
			if (($post['tgl_awal'] != '' && $post['tgl_akhir'] != '') && ($post['tgl_awal'] < $post['tgl_akhir'])) {
				$tgl_awal = date('Y-m-d', strtotime($post['tgl_awal']));
				$tgl_akhir = date('Y-m-d', strtotime($post['tgl_akhir']));
				$this->db->where('DATE(a.created_at) >= ', $tgl_awal);
				$this->db->where('DATE(a.created_at) <= ', $tgl_akhir);
				$data['filter']['tgl_awal'] = $tgl_awal;
				$data['filter']['tgl_akhir'] = $tgl_akhir;
			} else {
				$data['error'] = "Tanggal awal dan akhir harus diisi!";
				if ($post['tgl_awal'] < $tgl_akhir) {
					$data['error'] = "Tanggal awal harus lebih kecil dari tanggal akhir!";
				}
			}
			if ($post['status'] != '') {
				$this->db->where('a.status', $post['status']);
				$data['filter']['status'] = $post['status'];
			}
			if ($post['negara'] != '') {
				$this->db->where('a.kode_negara', $post['negara']);
				$data['filter']['negara'] = $post['negara'];
			}
		} else {
			$this->db->where('DATE(a.created_at)', date('Y-m-d'));
		}
		if ($id_agen = $this->session->userdata('id_agen')) {
			$this->db->where('a.id_agen', $id_agen);
		}

		$this->db->order_by('a.created_at', 'asc');
		$get = $this->db->get('transaksi a');
		$data['data'] = $get->result_array();
        
    
		$this->load->view('admin/header', $page);
		$this->load->view('laporan/manifest', $data);
		$this->load->view('admin/footer');
	}

	public function pengiriman()
	{
		$this->db->select('transaksi.kode_transaksi, transaksi.no_resit, transaksi.no_resit_lama, transaksi.created_at,total, id_agen, a.nama_negara , b.nama_negara as negara_tujuan, c.nama as nama_agen, d.alamat_pengirim, d.alamat_penerima, e.nama_status');
		$this->db->select('d.nama_pengirim, d.nama_penerima');
		$this->db->join('data_negara a', 'a.id = transaksi.kode_negara', 'left');
		$this->db->join('data_negara b', 'b.id = transaksi.kode_negara_tujuan', 'left');
		$this->db->join('data_outlet c', 'c.id = transaksi.id_agen', 'left');
		$this->db->join('transaksi_data_pelanggan d', 'd.id_transaksi = transaksi.id', 'left');
		$this->db->join('data_status_pengiriman e', 'transaksi.status = e.id', 'left');

		if ($id_agen = $this->session->userdata('id_agen')) {
			$this->db->where('transaksi.id_agen', $id_agen);
		}

		if ($post = $this->input->post()) {
			if ($post['tgl_awal'] != '') {
				$this->db->where('DATE(transaksi.created_at) >= ', $post['tgl_awal']);
				$data['tgl_awal'] = $post['tgl_awal'];
			}

			if ($post['tgl_akhir'] != '') {
				$this->db->where('DATE(transaksi.created_at) <= ', $post['tgl_akhir']);
				$data['tgl_akhir'] = $post['tgl_akhir'];
			}

			if ($post['status'] != '') {
				$this->db->where('transaksi.status', $post['status']);
				$data['status'] = $post['status'];
			}

			if ($post['id_agen'] != '') {
				$this->db->where('transaksi.id_agen', $post['id_agen']);
				$data['tgl_awal'] = $post['tgl_awal'];
			}


			if ($post['kode_negara'] != '') {
				$this->db->where('transaksi.kode_negara', $post['kode_negara']);
				$data['kode_negara'] = $post['kode_negara'];
			}

			if ($post['kode_negara_tujuan'] != '') {
				$this->db->where('transaksi.kode_negara_tujuan', $post['kode_negara_tujuan']);
				$data['kode_negara_tujuan'] = $post['kode_negara_tujuan'];
			}
			if ($post['no_resit'] != '') {
				$this->db->like('transaksi.no_resit', $post['no_resit'], 'both');
				$data['no_resit'] = $post['no_resit'];
			}
		}
		$list = $this->db->get('transaksi');
		// echo $this->db->last_query();
		//exit;
		$list = $list->result_array();


		$page['menu'] = 'laporan';
		$page['sub_menu'] = 'pengiriman';
		$page['title'] = 'Devlivery Report - Akinda Cargo';
		$page['datatables'] = true;
		$data['list'] = $list;
		$data['agen'] = $this->db->get_where('data_outlet', array('type_outlet' => 'Agen'))->result_array();
		$data['outlet'] = $this->db->get_where('data_outlet', array('type_outlet' => 'Outlet'))->result_array();
		$data['negara'] = $this->db->get('data_negara')->result_array();
		$this->load->view('admin/header', $page);
		$this->load->view('laporan/pengiriman', $data);
		$this->load->view('admin/footer', $page);
	}

	public function pendapatan()
	{

		$this->db->select('a.total , a.created_at,  b.nama_negara , c.nama_negara as negara_tujuan, d.nama as nama_agen, a.id_agen, e.alamat_pengirim, e.alamat_penerima');
		$this->db->join('data_negara b', 'b.id = a.kode_negara', 'left');
		$this->db->join('data_negara c', 'c.id = a.kode_negara_tujuan', 'left');
		$this->db->join('data_outlet d', 'd.id = a.id_agen', 'left');
		$this->db->join('transaksi_data_pelanggan e', 'e.id_transaksi = a.id', 'left');

		if ($id_agen = $this->session->userdata('id_agen')) {
			$this->db->where('a.id_agen', $id_agen);
		}

		$get  = $this->db->get('transaksi a');
		//echo $this->db->last_query();exit;
		$data['pendapatan'] = $get->result_array();
		$page['menu'] = 'laporan';
		$page['sub_menu'] = 'pendapatan';
		$page['title'] = 'Income Report - Akinda Cargo';
		$page['datatables'] = true;

		$data['agen'] = $this->db->get_where('data_outlet', array('type_outlet' => 'Agen'))->result_array();
		$data['outlet'] = $this->db->get_where('data_outlet', array('type_outlet' => 'Outlet'))->result_array();
		$data['negara'] = $this->db->get('data_negara')->result_array();

		$this->load->view('admin/header', $page);
		$this->load->view('laporan/pendapatan', $data);
		$this->load->view('admin/footer');
	}
}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */
