<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Export extends CI_Controller
{
    public function index($kode_negara='')
    {

        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
        $excelreader = new PHPExcel_Reader_Excel2007();
        
        
        $config['upload_path'] = realpath('excel');
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
         echo 'Gagal Import Data!';


     }else{


         //start transactional database
        $this->db->trans_start();
        $id_negara = $this->input->post('kode_negara');

        $data_upload = $this->upload->data();
        $neg = $this->db->get_where('data_negara',array('id' => $id_negara))->row_array();

        $loadexcel = $excelreader->load('excel/' . $data_upload['file_name']);
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
        unset($sheet[1]);
        
        foreach ($sheet as $key => $value) {
            if ($value['A'] != null && $value['A'] != 'NO_RESIT') {


                $this->db->select('b.id');
                $this->db->join('admin b', 'b.id_agen = a.id', 'LEFT');
                $petugas = $this->db->get_where('data_outlet a', array('a.kode' => $value['F']));
                if($petugas->num_rows() > 0){
                    $ptg = $petugas->row_array();
                    $id_petugas = $ptg['id'];
                }else{
                    $id_petugas = 1;
                }



                $tlp = str_replace(" ","",$value['L']);
                $data = explode("/" , $tlp);

                if(count($data) > 1){
                    $whatsapp = $data[0];
                    $telepon = $tlp;
                }else{
                    if($tlp === '')
                    {
                        $whatsapp = '0';
                        $telepon = '0';
                    }else{
                        $whatsapp = $data[0];
                        $telepon = $tlp;
                    }
                }

                $this->db->like('nama', $value['E'], 'both');
                $tujuan = $this->db->get('t_kabupaten')->row_array();

                $kode_tarif = str_replace(" ","",$value['G']);

                $ukuran = $this->db->get_where('data_tarif_ukuran', array('kode_tarif' => $kode_tarif))->row_array();

                $trans['kode_transaksi'] = 'TR' . date('YmdHis') . +1;
                
                $trans['no_resit_lama'] = $value['A'].'-'.$value['C'];
                $trans['container'] = $value['B'];
                $trans['kode_negara'] = '2';
                $trans['kode_negara_tujuan'] = '1';
                $trans['status'] = '1';
                $trans['id_user'] = $id_petugas;
                $trans['isi'] = $value['P'];
                $trans['note'] = 'Barang total '.$value['D']. ' Koli';
                $trans['platform'] = 'web';
                $trans['id_size'] = $ukuran['id'];
                $trans['created_at'] = date('Y-m-d H:i:s');
                $trans['mata_uang'] = $neg['mata_uang'];

                $this->db->insert('transaksi', $trans);

                $id_trans = $this->db->insert_id();


                $upp_trans['no_resit'] = $neg['kode_negara'] . $value['B'] . $value['F'] . $id_trans;
                $this->db->update('transaksi', $upp_trans, array('id' => $id_trans));

                $pelanggan['id_transaksi'] = $id_trans;
                $pelanggan['identity_type'] = 'Paspor';
                $pelanggan['no_pasport'] = $value['J'];
                $pelanggan['nama_pengirim'] = $value['I'];
                $pelanggan['hp_pengirim'] = $telepon;
                $pelanggan['wa_pengirim'] = $whatsapp;
                $pelanggan['alamat_pengirim'] = $value['K'];
                $pelanggan['identity_type_penerima'] = null;
                $pelanggan['no_pasport_penerima'] = null;
                $pelanggan['nama_penerima'] = $value['M'];
                $pelanggan['hp_penerima'] = $value['O'];
                $pelanggan['wa_penerima'] = $value['O'];
                $pelanggan['id_negara_penerima'] = '1';
                $pelanggan['id_provinsi_penerima'] = @$tujuan['id_prov'];
                $pelanggan['id_kota_penerima'] = @$tujuan['id'];
                $pelanggan['kode_pos_penerima'] = null;
                $pelanggan['alamat_penerima'] = $value['N'];

                $this->db->insert('transaksi_data_pelanggan', $pelanggan);
                

                $status['id_transaksi'] = $id_trans;
                $status['status'] = '1';
                $status['id_user'] = $id_petugas;
                $status['created_at'] = date('Y-m-d H:i:s');
                $this->db->insert('transaksi_status', $status);

                
            }
        }
        //end transactional database
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
         echo "Export data excel failed";
     }else{
        echo "Success upload data excel";
    }
}


}


}

/* End of file Controllername.php */
