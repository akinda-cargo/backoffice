<?php
header('Access-Control-Allow-Origin: *');

class Api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function tes_pdf()
	{
		$pdf = base_url() . 'DOKUMEN_DEVELOP/barcode.pdf';
		$b64Doc = chunk_split(base64_encode(file_get_contents($pdf)));

		echo "<iframe src='" . $pdf . "' style='width:100%;' />";
	}


	public function dologin()
	{
		if ($data = $this->input->post()) {
			$pass = $data['password'];
			$email = $data['email'];

			$get = $this->db->get_where('admin', array('email' => $email));

			if ($hasil = $get->row_array()) {

				if (password_verify($pass, $hasil['password'])) {

					$sess['id_user'] = $hasil['id'];
					$sess['nama'] = $hasil['nama'];
					$sess['level'] = $hasil['level'];
					$sess['status'] = true;
					$sess['id_outlet'] = $hasil['id_agen'];
					$sess['no_tlp'] = $hasil['tlp'];
					$sess['email'] = $hasil['email'];
					$sess['msg'] = 'Berhasil masuk';

					if ($hasil['id_agen'] != 0) {
						$sess['is_agen'] = true;
						$sess['id_agen'] = $hasil['id_agen'];
					} else {
						$sess['is_agen'] = false;
						$sess['id_agen'] = 0;
					}

					echo json_encode($sess);
				} else {
					echo json_encode(array('status' => false, 'msg' => 'Email atau password salah'));
				}
			} else {
				echo json_encode(array('status' => false, 'msg' => 'Data tidak di temukan'));
			}
		}
	}
	
	public function get_pelanggan($id_pelanggan=''){
		
		if($id_pelanggan != ''){
			$query = $this->db->get_where('data_users', array('id' => $id_pelanggan));

			if ($query->num_rows() > 0) {
				$output = $query->row_array();
				echo json_encode($output);
			} 
		}
		
	}
	
	public function add_pelanggan()
	{
		if ($post = $this->input->post()) {
			$field = $post['indentitas'];
			$value = $post['value'];

			if ($field == 'KTP') {
				$coloum = 'nik';
			} else {
				$coloum = 'no_pasport';
			}
			$get = $this->db->get_where('data_users', array($coloum => $value));

			if ($get->num_rows() > 0) {
				$json['status'] = false;
				$json['msg'] = 'Data sudah ada';

				echo json_encode($json);
			} else {

				if ($field == 'KTP') {
					$data['nik'] = $post['value'];
				} else {
					$data['no_pasport'] = $post['value'];
				}

				if ($post['kode_negara'] != '') {
					$get_neg = $this->db->get_where('data_negara', array('id' => $post['kode_negara']))->row_array();
					$negara = $get_neg['kode_negara'];
					$kode_tlp = $get_neg['kode_tlp'];
				} else {
					$negara = '';
					$kode_tlp = '62';
				}
				
				$nomer = intval($post['sender_wa']);
				$nomer_wa = substr($nomer, 0, 2) != $kode_tlp ? $kode_tlp . $nomer : "" . $nomer;
				
				$tlp = intval($post['sender_tlp']);
				$nomer_tlp = substr($nomer, 0, 2) != $tlp ? $kode_tlp . $tlp : "" . $tlp;


				$data['nama_lengkap'] = $post['sender_nama'];
				$data['no_tlp'] = $nomer_wa;
				$data['no_wa'] = $nomer_tlp;
				$data['alamat_lengkap'] = $post['sender_alamat'];
				$data['kode_negara'] = $negara;
				$data['id_negara'] = $post['kode_negara'];
				$data['id_user'] = $post['id_user'];
				$data['flag'] = 1;
				$data['created_at'] = date('Y-m-d H:i:s');

				if ($this->db->insert('data_users', $data)) {
					$json['status'] = true;
					$json['msg'] = 'Data Berhasil di simpan';

					echo json_encode($json);
				}
			}
		}
	}
	
	public function add_penerima()
	{
		if ($post = $this->input->post()) {
			$field = $post['indentitas'];
			$value = $post['value'];

			if ($field == 'KTP') {
				$coloum = 'nik';
			} else {
				$coloum = 'no_pasport';
			}
			$get = $this->db->get_where('data_users', array($coloum => $value));

			if ($get->num_rows() > 0) {
				$json['status'] = false;
				$json['msg'] = 'Data sudah ada';

				echo json_encode($json);
			} else {

				if ($field == 'KTP') {
					$data['nik'] = $post['value'];
				} else {
					$data['no_pasport'] = $post['value'];
				}

				if ($post['kode_negara'] != '') {
					$get_neg = $this->db->get_where('data_negara', array('id' => $post['kode_negara']))->row_array();
					$negara = $get_neg['kode_negara'];
					$kode_tlp = $get_neg['kode_tlp'];
				} else {
					$negara = '';
					$kode_tlp = '62';
				}
				
				$nomer = intval($post['wa']);
				$nomer_wa = substr($nomer, 0, 2) != $kode_tlp ? $kode_tlp . $nomer : "" . $nomer;
				
				$tlp = intval($post['tlp']);
				$nomer_tlp = substr($nomer, 0, 2) != $tlp ? $kode_tlp . $tlp : "" . $tlp;


				$data['nama_lengkap'] = $post['nama'];
				$data['no_tlp'] = $nomer_wa;
				$data['no_wa'] = $nomer_tlp;
				$data['alamat_lengkap'] = $post['alamat'];
				$data['kode_negara'] = $negara;
				$data['id_negara'] = $post['kode_negara'];
				$data['id_provinsi'] = $post['id_prov'];
				$data['id_kabupaten'] = $post['id_kota'];
				$data['id_kecamatan'] = $post['id_kecamatan'];
				$data['rt'] = $post['rt'];
				$data['rw'] = $post['rw'];
				$data['desa'] = $post['desa'];
				$data['dusun'] = $post['dusun'];
				$data['kode_pos'] = $post['pos'];
				$data['id_user'] = $post['id_user'];
				$data['flag'] = 1;
				$data['created_at'] = date('Y-m-d H:i:s');

				if ($this->db->insert('data_users', $data)) {
					$json['status'] = true;
					$json['msg'] = 'Data Berhasil di simpan';

					echo json_encode($json);
				}
			}
		}
	}

	public function detail_transaksi($no_resit)
	{
		$barang = $this->db->get_where('transaksi', array('no_resit' => $no_resit));

		if ($barang->num_rows() > 0) {

			$barang = $barang->row_array();
			$id_transaksi = $barang['id'];

			$tr = $this->db->query("SELECT a.* , b.nama_negara , c.nama as provinsi, d.nama as kota, e.ukuran, f.isi, f.total, f.no_resit, f.container FROM transaksi_data_pelanggan a
				LEFT JOIN transaksi f ON a.id_transaksi = f.id
				LEFT JOIN data_negara b ON a.id_negara_penerima = b.id
				LEFT JOIN t_provinsi c ON a.id_provinsi_penerima = c.id
				LEFT JOIN t_kabupaten d ON a.id_kota_penerima = d.id
				LEFT JOIN data_tarif_ukuran e ON f.id_size = e.id
				WHERE a.id_transaksi=" . $id_transaksi);

			//echo $this->db->last_query();exit;
			$tr = $tr->row_array();




			$transaksi = array(
				'id_transaksi' => $tr['id_transaksi'],
				'no_pasport' => $tr['no_pasport'],
				'nama_pengirim'	=> $tr['nama_pengirim'],
				'hp_pengirim'	=> $tr['hp_pengirim'],
				'alamat_pengirim' => $tr['alamat_pengirim'],
				'no_pasport_penerima' => $tr['no_pasport_penerima'],
				'nama_penerima'	=> $tr['nama_penerima'],
				'hp_penerima'	=> $tr['hp_penerima'],
				'wa_penerima'	=> $tr['wa_penerima'],
				'wa_pengirim'	=> $tr['wa_pengirim'],
				'kode_pos_penerima'	=> $tr['kode_pos_penerima'],
				'alamat_penerima'	=> $tr['alamat_penerima'],
				'nama_negara'	=> $tr['nama_negara'],
				'provinsi'	=> $tr['provinsi'],
				'kota'	=> $tr['kota'],
				'ukuran'	=> $tr['ukuran'],
				'isi'	=> $tr['isi'],
				'total'	=> $tr['total'],
				'total_rupiah'	=> decimals($tr['total']),
				'no_resit'	=> $tr['no_resit'],
				'container'	=> $tr['container']
			);

			echo json_encode(array('transaksi' => $transaksi));
		}
	}

	public function cek_resi($no_resit = '')
	{
		$this->db->select('a.created_at, b.nama_status, c.no_resit');
		$this->db->join('data_status_pengiriman b', 'a.status = b.id', 'left');
		$this->db->join('transaksi c', 'a.id_transaksi = c.id', 'left');
		$this->db->like('c.no_resit', $no_resit, 'both');
		$this->db->or_like('c.no_resit_lama', $no_resit, 'both');
		$this->db->order_by('b.id', 'DESC');
		$get = $this->db->get('transaksi_status a');

		$row = $get->row_array();
		$data = array(
			'tgl_transaksi' => $row['created_at'],
			'status'	=> $row['nama_status'],
			'no_resit'	=> $row['no_resit']
		);


		echo json_encode($data);
	}
	
	public function update_tgl_status(){
		$date1 = date('Y-m-d');
		$updatetgl = date('Y-m-d H:i:s',strtotime($date1 . "-1 days"));
		$up['created_at'] = date('Y-m-d H:i:s',strtotime('2021-03-20 19:03:15'));
		$this->db->update('transaksi_status',$up);
	}

	public function cek_status_resi($no_resit = '')
	{
		$this->db->select('a.created_at, d.nama_status, b.no_resit, c.nama as nama_petugas, a.note');

		$this->db->join('transaksi b', 'a.id_transaksi = b.id', 'left');
		$this->db->join('admin c', 'a.id_user = c.id', 'left');
		$this->db->join('data_status_pengiriman d', 'a.status = d.id', 'left');

		$this->db->where('b.no_resit', $no_resit);
		$this->db->group_by('a.status');
		$get = $this->db->get('transaksi_status a');

		$data = array();
		foreach ($get->result_array() as $row) {
			$data[] = array(
				'tgl_transaksi' => date('d-m-Y H:i', strtotime($row['created_at'])),
				'status'	=> $row['nama_status'],
				'no_resit'	=> $row['no_resit'],
				'nama_petugas'	=> $row['nama_petugas'],
				'note'		=> $row['note']
			);
		}



		echo json_encode($data);
	}


	public function slider()
	{
		$get = $this->db->get_where('data_slider', array('flag' => 1))->result_array();

		echo json_encode($get);
	}

	public function tarif()
	{
		$get = $this->db->get_where('data_tarif_ukuran', array('flag' => 1))->result_array();

		$data = array();

		foreach ($get as $row) {
			$data[] = array(
				'id'	=> $row['id'],
				'kode_tarif' => $row['kode_tarif'],
				'ukuran' => $row['ukuran'],
				'harga'	=> $row['harga'],
				'harga_rupiah' => decimals($row['harga'])
			);
		}

		echo json_encode($data);
	}

	public function armada()
	{
		$get = $this->db->get_where('data_armada')->result_array();

		echo json_encode($get);
	}

	public function outlet($type = '')
	{

		$this->db->select(' a.*, b.nama_negara, c.nama as provinsi');
		$this->db->join('data_negara b', 'a.id_negara = b.id', 'left');
		$this->db->join('t_provinsi c', 'a.id_provinsi = c.id', 'left');
		$this->db->where(array('a.flag' => 1, 'a.type_outlet' => $type));
		$get = $this->db->get('data_outlet a');
		//echo $this->db->last_query();exit;
		$get = $get->result_array();

		$data = array();
		foreach ($get as $row) {
			$data[] = array(
				'nama' => $row['nama'],
				'kode' => $row['kode'],
				'negara' => $row['nama_negara'],
				'provinsi' => $row['provinsi']
			);
		}

		echo json_encode($data);
	}

	public function history($id_user = '')
	{

		if ($id_user != '') {
			$this->db->select('a.no_resit, a.status, a.created_at, c.nama_status, a.id');
			$this->db->join('transaksi_status b', 'a.id = b.id_transaksi', 'LEFT');
			$this->db->join('data_status_pengiriman c', 'a.status = c.id', 'LEFT');
			$this->db->where('b.id_user', $id_user);
			$this->db->group_by('a.no_resit');
			$get = $this->db->get('transaksi a');

			//echo $this->db->last_query();
			$get = $get->result_array();
			$data = array();

			foreach ($get as $row) {
				$data[] = array(
					'id_transaksi' => $row['id'],
					'no_resit' => $row['no_resit'],
					'nama_status' => $row['nama_status'],
					'tgl_transaksi' => date('d/m/Y H:i', strtotime($row['created_at'])),
				);
			}

			echo json_encode($data, JSON_PRETTY_PRINT);
		}
	}


	public function history_daily($id_user = '')
	{

		if ($id_user != '') {
			$this->db->select('a.no_resit, a.status, a.created_at, c.nama_status, a.id');
			$this->db->join('transaksi_status b', 'b.id_transaksi = a.id', 'left');
			$this->db->join('data_status_pengiriman c', 'a.status = c.id', 'left');
			$this->db->where('b.id_user', $id_user);
			$this->db->where('DATE(b.created_at)', date('Y-m-d'));
			$this->db->group_by('a.no_resit');
			$get = $this->db->get('transaksi a')->result_array();
			//echo $this->db->last_query();

			$data = array();

			foreach ($get as $row) {
				$data[] = array(
					'id_transaksi' => $row['id'],
					'no_resit' => $row['no_resit'],
					'nama_status' => $row['nama_status'],
					'tgl_transaksi' => date('d/m/Y H:i', strtotime($row['created_at']))
				);
			}

			echo json_encode($data);
		}
	}

	public function tes_wa()
	{

		$no = '085933008404';
		$pesan = 'Tes ruang wa dokumen';
		// $url_dokumen = 'http://localhost/expedisi/pdf/invoce_IDN203SPRADM13.pdf';

		// $tes =  $this->global->send_file_wa($no, $pesan, $url_dokumen);
		$tes = 	$this->global->send_text_wa($no, $pesan);

		echo "<pre>";
		print_r($tes);
	}

	public function update_status()
	{
		if ($post = $this->input->post()) {

			//ambil id transaksi
			$get = $this->db->get_where('transaksi', array('no_resit' => $post['no_resit']));

			if ($get->num_rows() > 0) {

				$get = $get->row_array();


				$data['id_transaksi'] = $get['id'];
				$data['kode_barang'] = $post['no_resit'];
				$data['status'] = $post['status'];
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['id_user'] = $post['id_user'];
				$data['note'] = $post['note'];

				if ($this->db->insert('transaksi_status', $data)) {
					
					$ptg = $this->db->get_where("admin",array('id' => $post['id_user']))->row_array();

					$upp['status'] = $post['status'];

					if ($this->db->update('transaksi', $upp, array('no_resit' => $post['no_resit']))) {

						if ($post['id_user'] == '11' || $post['id_user'] == '12' || $post['id_user'] == '13') {

							$this->db->select('c.wa_penerima, b.pesan_wa, c.wa_pengirim');
							$this->db->join('data_status_pengiriman b', 'a.status = b.id', 'LEFT');
							$this->db->join('transaksi_data_pelanggan c', 'a.id = c.id_transaksi', 'LEFT');
							$this->db->where('a.id', $get['id']);
							$get = $this->db->get('transaksi a')->row_array();

							$no = $get['wa_pengirim'];
							$pesan = $get['pesan_wa'] . "\n Note: " . $post['note'] . "\n\nUpdated by : ".$ptg['nama']." ".date('d-m-Y H:i');

							$this->send_wa($no, $pesan);
						}

						$json['status'] = true;
						$json['msg'] = "Data Pengiriman berhasil di update. <br> Note: " . $post['note'];

						echo json_encode($json);
					}
				}
			}
		}
	}


	public function detail_scan($no_resit = '')
	{

		$this->db->select('b.*,transaksi.no_resit, transaksi.total,  c.nama as provinsi, d.nama as kota, e.nama_status, transaksi.id, transaksi.container');
		$this->db->join('transaksi_data_pelanggan b', 'b.id_transaksi = transaksi.id', 'left');
		$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
		$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');

		$this->db->join('data_status_pengiriman e', 'transaksi.status = e.id', 'left');
		$this->db->where('transaksi.no_resit', $no_resit);
		$get = $this->db->get('transaksi');

		if ($get->num_rows() > 0) {

			$get = $get->row_array();

			$data = array();
			$barang = array();


			//$data[] = array($get);
			$data[] = array(
				'total' => decimals($get['total']),
				'no_resit' => $get['no_resit'],
				'provinsi'	=> $get['provinsi'],
				'nama_pengirim' => $get['nama_pengirim'],
				'hp_pengirim'	=> $get['hp_pengirim'],
				'alamat_pengirim'	=> $get['alamat_pengirim'],
				'nama_penerima'	=> $get['nama_penerima'],
				'hp_penerima'	=> $get['hp_penerima'],
				'wa_penerima'	=> $get['wa_penerima'],
				'alamat_penerima'	=> $get['alamat_penerima'],
				'nama_status'	=> $get['nama_status'],
				'id_transaksi'	=> $get['id'],
				'container'		=> $get['container'],
			);

			$detail = $this->db->query("SELECT a.*, b.ukuran  FROM transaksi_detail a 
				LEFT JOIN data_tarif_ukuran b ON a.id_size = b.id
				WHERE a.id_transaksi ='" . $get['id'] . "' ")->result_array();

			foreach ($detail as $row) {
				$barang[] = array('ukuran' => $row['ukuran'], 'tarif' => decimals($row['tarif']));
			}

			$status = $this->db->get_where('data_status_pengiriman', array('flag' => 1))->result_array();


			echo json_encode(array('data' => $data, 'barang' => $barang, 'status' => $status), JSON_PRETTY_PRINT);
		} else {
			$data['status'] = false;
			$data['msg'] = 'Tidak ada data';
			echo json_encode($data);
		}
	}

	public function get_negara()
	{
		$get = $this->db->get('data_negara')->result_array();
		
		$query = $this->db->get('data_users');
		$output = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				$output[] = array(
					'nama'  => $row["nama_lengkap"],
					'id' => $row['id']
				);
			}
			
		}

		echo json_encode(array('negara' => $get, 'pelanggan' => $output));
	}

	public function get_prov($id = '')
	{

		$get = $this->db->get_where('t_provinsi', array('id_negara' => $id))->result_array();

		echo json_encode($get);
	}

	public function get_kota($id = '')
	{

		$get = $this->db->get_where('t_kabupaten', array('id_prov' => $id))->result_array();

		echo json_encode($get);
	}

	public function get_kecamatan($id = '')
	{
		$get = $this->db->get_where('t_kecamatan', array('id_kabupaten' => $id))->result_array();

		echo json_encode($get);
	}

	public function send_wa($no, $pesan)
	{

		//$this->global->sendWAOK($no,$pesan);

		$this->global->send_text_wa($no, $pesan);

		//$this->global->send_wablas($no,$pesan);
	}

	public function add_temp()
	{
		if ($post = $this->input->post()) {

			$id_user = $post['id_user'];

			$this->db->where('adm.id', $id_user);
			$this->db->select('do.kode');
			$this->db->select('dn.kode_negara');
			$this->db->join('data_outlet do', 'adm.id_agen = do.id', 'left');
			$this->db->join('data_negara dn', 'do.id_negara = dn.id', 'left');
			$get_outlet = $this->db->get('admin adm')->row_array();

			if (empty($get_outlet['kode_negara'])) {
				$get_outlet['kode_negara'] = "IDN";
			}
			if (empty($get_outlet['kode'])) {
				$get_outlet['kode'] = "SPRADM";
			}

			if ($post = $this->input->post()) {
				
				if($this->db->delete('transaksi_temp', array('id_user' => $id_user))){
					
					
					
					$data['kode_barang'] = $get_outlet['kode_negara'] . $post['no_kontena']  . $get_outlet['kode'];
					$data['id_size'] = $post['id_size'];
					$data['isi'] = $post['isi'];
					$data['note'] = $post['note'];
					$data['harga'] = $post['harga'];
					$data['id_user'] = $id_user;
					$data['container'] = $post['no_kontena'];
					$data['mata_uang'] = $post['mata_uang'];
					$data['no_resit_lama'] = $post['no_resit_lama'];

					if ($this->db->insert('transaksi_temp', $data)) {

						$json['msg'] = 'Berhasil input data barang';
						$json['status'] = true;
					} else {
						$json['msg'] = 'Harap cek kembali';
						$json['status'] = false;
					}

					echo json_encode($json);
					
				}
			}
		} else {
			$json['msg'] = 'Harap cek kembali';
			$json['status'] = false;

			echo json_encode($json);
		}
	}

	public function add_transaksi()
	{
		if ($post = $this->input->post()) {

			$id_user = $post['id_user'];

			$detail = $this->db->get_where('transaksi_temp', array('id_user' => $id_user))->row_array();


			$this->db->select('MAX(id) as box');
			$box_number = $this->db->get('transaksi')->row_array();
			$box = $box_number['box'] + 1;

			$data['kode_transaksi'] = 'TR' . date('YmdHis');
			$data['no_resit'] = $detail['kode_barang'] . $box;
			$data['kode_negara'] = $post['id_negara_pengirim'];
			$data['kode_negara_tujuan'] = $post['id_negara_penerima'];
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['status'] = 1;
			$data['id_user'] = $post['id_user'];
			$data['id_agen'] = $post['id_agen'];
			$data['platform'] = 'mobile';
			$data['isi'] = $detail['isi'];
			$data['id_size']	= $detail['id_size'];
			$data['total'] = $detail['harga'];
			$data['container'] = $detail['container'];
			$data['mata_uang'] = $detail['mata_uang'];
			$data['no_resit_lama'] = $detail['no_resit_lama'];
			$data['note']	= $detail['note'];


			if ($this->db->insert('transaksi', $data)) {
				$id_transaksi = $this->db->insert_id();

				//insert log status
				$log['id_transaksi'] = $id_transaksi;
				$log['created_at'] = date('Y-m-d H:i:s');
				$log['status'] = 1;
				$log['id_user'] = $id_user;

				$this->db->insert('transaksi_status', $log);


				//insert data pelanggan
				$pel['id_transaksi'] = $id_transaksi;
				$pel['no_pasport'] = $post['pasport_pengirim'];
				$pel['nama_pengirim'] = $post['nama_pengirim'];
				$pel['hp_pengirim'] = $post['tlp_pengirim'];
				$pel['wa_pengirim'] = $post['wa_pengirim'];
				$pel['alamat_pengirim'] = $post['alamat_pengirim'];
				$pel['no_pasport_penerima'] = $post['no_pasport_penerima'];
				$pel['nama_penerima'] = $post['nama_penerima'];
				$pel['hp_penerima'] = $post['tlp_penerima'];
				$pel['wa_penerima'] = $post['wa_penerima'];
				$pel['id_negara_penerima'] = $post['id_negara_penerima'];
				$pel['id_provinsi_penerima'] = $post['id_provinsi_penerima'];
				$pel['id_kota_penerima'] = $post['id_kota_penerima'];
				$pel['kode_pos_penerima'] = $post['pos_penerima'];
				$pel['alamat_penerima'] = $post['alamat_penerima'];
				$pel['id_kecamatan_penerima'] = $post['id_kecamatan_penerima'];
				$pel['desa_penerima'] = $post['desa_penerima'];
				$pel['dusun_penerima'] = $post['dusun_penerima'];
				$pel['rt_penerima'] = $post['rt_penerima'];
				$pel['rw_penerima'] = $post['rw_penerima'];

				$this->db->insert('transaksi_data_pelanggan', $pel);

				//insert log status
				$log['id_transaksi'] = $id_transaksi;
				$log['created_at'] = date('Y-m-d H:i:s');
				$log['status'] = 1;
				$log['id_user'] = $id_user;
				$this->db->insert('transaksi_status', $log);

				//qrcode genrate
				$this->load->library('ciqrcode');
				$code = $data['no_resit'];

				$qr_image = $code . '.png';
				$params['data'] = $code;
				$params['level'] = 'H';
				$params['size'] = 8;
				$params['savename'] = FCPATH . "barcode/" . $qr_image;
				if ($this->ciqrcode->generate($params)) {
				}


				//send wa notif
				$no_wa = $post['wa_pengirim'];
				$pesan = "Terimakasih atas kepercayaan kepada syarikat AKINDA, kami akan info secara berkala status barang yg dikirim \n No Resi : " . $data['no_resit'] . " dengan Total : " . $data['total'] . " , dan di terima pada " . $data['created_at'];

				$userid = $this->session->userdata('userid');
				if ($userid == '11' || $userid == '12' || $userid == '13') {
					$this->send_wa($no_wa, $pesan);
				}

				$this->db->delete('transaksi_temp', array('id_user' => $id_user));

				$json['status'] = true;
				$json['msg'] = 'Berhasil melakukan transaksi';
				$json['id_transaksi'] = $id_transaksi;
				$json['wa_penerima'] = $post['wa_penerima'];

				if ($post['id_agen']) {
					$json['agen'] = true;
				}
				$print = 'invoice';
				$this->global->create_log_print($id_transaksi, $id_user, $print);

				$this->gen_pdf($id_transaksi);

				echo json_encode($json);
			}
		}
	}

	public function gen_pdf($id_transaksi)
	{

		$this->db->select('b.*,transaksi.no_resit, transaksi.no_resit_lama, transaksi.note, transaksi.total, transaksi.bayar, transaksi.kembali, c.nama as provinsi, d.nama as kota, transaksi.created_at, transaksi.id_agen, transaksi.total, transaksi.isi, transaksi.container, transaksi.id_user, tu.ukuran, do.kode ');
		$this->db->join('transaksi_data_pelanggan b', 'b.id_transaksi = transaksi.id', 'left');
		$this->db->join('t_provinsi c', 'b.id_provinsi_penerima = c.id', 'left');
		$this->db->join('t_kabupaten d', 'b.id_kota_penerima = d.id', 'left');
		$this->db->join('data_outlet do', 'transaksi.id_agen = do.id', 'left');
		$this->db->join('data_tarif_ukuran tu', 'transaksi.id_size = tu.id', 'left');
		$this->db->where('transaksi.id', $id_transaksi);
		$get = $this->db->get('transaksi');
		//echo $this->db->last_query();exit;
		$get = $get->row_array();
		


		$setting = $this->global->getSetting("semua");

		$data['tr'] = $get;
		$data['setting'] = $setting;
		$html = $this->load->view('print/invoice_pdf', $data, true);
		$filename = 'invoce_' . $get['no_resit'];

		$this->load->library('pdf');
		$this->pdf->download($html, $filename, true, 'A4', 'portrait');

		$filename = 'invoce_' . $get['no_resit'];
		$url_document = base_url() . 'pdf/' . $filename . '.pdf';
		$no = $get['wa_pengirim'];
		//$no = '6285933008404';
		$text = "No Resit: " . $get['no_resit'] . " \nTarif: " . $get['total'] . "\nUkuran " . $get['ukuran'] . ": " . $get['isi'] . "\n \n";
		$text .= "*Penerima* \n \nNama Penerima: " . $get['nama_penerima'] . "\nNo Telp/Whatsapp: " . $get['hp_penerima'] . "/" . $get['wa_penerima'] . "\nProvinsi/Kota: " . $get['provinsi'] . "/" . $get['kota'] . "\n \n";
		$text .= "*Pengirim* \n \nNama Pengirim: " . $get['nama_pengirim'] . "\nNo Telp/Whatsapp" . $get['hp_pengirim'] . "/" . $get['wa_pengirim'] . "\nAlamat Lengkap:" . $get['alamat_pengirim'];
		$pesan = '';
		$userid = $get['id_user'];
		if ($userid == '11' || $userid == '12' || $userid == '13') {
			$this->send_wa($no, $text);
			$this->send_dokumen_wa($no, $url_document, $pesan);
			
		}
	}

	public function send_dokumen_wa($no, $url_dokumen, $pesan)
	{

		return $this->global->send_file_wa($no, $pesan, $url_dokumen);
	}
}
