<!DOCTYPE html>
<html>
<head>
	<title>Barcode Transaksi Akinda</title>
</head>

<style type="text/css">
.font30 { font-family:Arial, Helvetica, sans-serif; font-size:30px; color:#000000; }

.font20 { font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#000000; }
.font16 { font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#000000; }
.font14 { font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000; }
.font12 { font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; }
.font11 { font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; }
.font10 { font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#000000; }
.box {
 width : 100mm;
 height: 70mm;
	
}
 table {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body 
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
    margin-top:2px;
    margin-left: 2px;
	margin-right: 2px;
    margin-bottom: 2px;
}	

.pagebreak {page-break-before: always}

    @media print {
		html, body {
		    width : 100mm;
           height: 75mm;
            font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
     margin-top:2px;
    margin-left: 2px;
	margin-right: 2px;
    margin-bottom: 2px;
		}
        
     
</style>
<body  OnLoad="window.print();">
    	
	<div class="box">
	
		  <table  class="font11" border="2px" cellpadding="1" cellspacing="0" style="width: 100%;border-collapse: collapse;">
              <tr>
					<td colspan="2" style="text-align: left;">
						<span class="font10">Akinda-cargo.com</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;" class="font20">
						<strong><?= $tr['no_resit']; ?></strong>
					</td>
				</tr>
				<tr>
					<td width="60%" align="justify">
						<div class="font10">Dengan menyantumkan paket ini , maka penerima telah mengkonfirmasi kebenarnan informasi resi dan isi paket ini ,
						serta telah memahami dan menyetujui syarat dan ketentuan umum Akinda Cargo</div>
					</td>
					<td width="40%" rowspan="2" align="center">TGL: <?=$tr['created_at']?>
						<img src="<?= base_url('barcode/'.$tr['no_resit'].'.png'); ?>" style="width: 50%;"></td>
				</tr>
				
			</table>

			<table  class="font11" border="2px" cellpadding="1" cellspacing="0" style="width: 100%;border-collapse: collapse;">
                <tr>
					<td style="text-align: left;">
					<strong>Penerima : </strong>
					<?= $tr['nama_penerima'].' , ' .$tr['hp_penerima'].' , '.$tr['kota'].' , '.$tr['alamat_penerima']; ?>					<br>
					</td>
				</tr>
                <tr>
					<td style="text-align: left;">
					<strong>Pengirim : </strong><?= $tr['nama_pengirim'].' , ' .$tr['hp_pengirim'].' , '.$tr['alamat_pengirim']; ?> <br>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">
						

						Jumlah : <?= 'Box '.$tr['ukuran'].' Barang '.$tr['isi'];  ?>

					</td>
				</tr>
                <tr>
					<td style="text-align: left;">
					Catatan : 

				    <?=$tr['note']?></td>
				</tr>
				<tr>
					<td  style="color:#000;text-align: center;">Lembar Drop Poin Incoming</td>
				</tr>
			</table>
        <em class="font10">Dicetak oleh : <?=$this->session->userdata('nama')?>, pada tgl <?=date('Y-m-d H:i:s')?></em>
        <div>
           
        <div style="page-break-before:always;"></div>
            	
	<div class="box">
	
		  <table  class="font11" border="2px" cellpadding="1" cellspacing="0" style="width: 100%;border-collapse: collapse;">
              <tr>
					<td colspan="2" style="text-align: left;">
						<span class="font10">Akinda-cargo.com</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;" class="font20">
						<strong><?= $tr['no_resit']; ?></strong>
					</td>
				</tr>
				<tr>
					<td width="60%" align="justify">
						<div class="font10">Dengan menyantumkan paket ini , maka penerima telah mengkonfirmasi kebenarnan informasi resi dan isi paket ini ,
						serta telah memahami dan menyetujui syarat dan ketentuan umum Akinda Cargo</div>
					</td>
					<td width="40%" rowspan="2" align="center">TGL: <?=$tr['created_at']?>
						<img src="<?= base_url('barcode/'.$tr['no_resit'].'.png'); ?>" style="width: 50%;"></td>
				</tr>
				
			</table>

			<table  class="font11" border="2px" cellpadding="1" cellspacing="0" style="width: 100%;border-collapse: collapse;">
                <tr>
					<td style="text-align: left;">
					<strong>Penerima : </strong>
					<?= $tr['nama_penerima'].' , ' .$tr['hp_penerima'].' , '.$tr['kota'].' , '.$tr['alamat_penerima']; ?>					<br>
					</td>
				</tr>
                <tr>
					<td style="text-align: left;">
					<strong>Pengirim : </strong>
					<?= $tr['nama_pengirim'].' , ' .$tr['hp_pengirim'].' , '.$tr['alamat_pengirim']; ?>					<br>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">
						

						Jumlah : <?= 'Box '.$tr['ukuran'].' Barang '.$tr['isi'];  ?>

					</td>
				</tr>
                <tr>
					<td style="text-align: left;">
					Catatan : <?=$tr['note']?>

					</td>
				</tr>
				<tr>
					<td  style="color:#000;text-align: center;">Lembar Pengirim</td>
				</tr>
			</table>
         <em class="font10">Dicetak oleh : <?=$this->session->userdata('nama')?>, pada tgl <?=date('Y-m-d H:i:s')?></em>
        <div>
</body>
</html>