<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>receipt</title>
  <!-- <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@400&display=swap" rel="stylesheet"> -->
  <link href="https://fonts.googleapis.com/css2?family=Nanum+Gothic+Coding&family=Roboto+Mono:wght@500&display=swap" rel="stylesheet">
  <style>

    @page { margin: 0 }
body { margin: 0 }
.sheet {
  margin: 0;
  overflow: hidden;
  position: relative;
  box-sizing: border-box;
  page-break-after: always;
}

/** Paper sizes **/
body.A3               .sheet { width: 297mm; height: 419mm }
body.A3.landscape     .sheet { width: 420mm; height: 296mm }
body.A4               .sheet { width: 210mm; height: 296mm }
body.A4.landscape     .sheet { width: 297mm; height: 209mm }
body.A5               .sheet { width: 148mm; height: 209mm }
body.A5.landscape     .sheet { width: 210mm; height: 147mm }
body.letter           .sheet { width: 216mm; height: 279mm }
body.letter.landscape .sheet { width: 280mm; height: 215mm }
body.legal            .sheet { width: 216mm; height: 356mm }
body.legal.landscape  .sheet { width: 357mm; height: 215mm }

/** Padding area **/
.sheet.padding-10mm { padding: 5mm;padding-left: 0mm;margin-bottom: 55mm; }

/** For screen preview **/
@media screen {
  body { background: #e0e0e0 }
  .sheet {
    background: white;
    box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
    margin: 5mm auto;
  }
}

/** Fix for Chrome issue #273306 **/
@media print {
           body.A3.landscape { width: 420mm }
  body.A3, body.A4.landscape { width: 297mm }
  body.A4, body.A5.landscape { width: 210mm }
  body.A5                    { width: 148mm }
  body.letter, body.legal    { width: 216mm }
  body.letter.landscape      { width: 280mm }
  body.legal.landscape       { width: 357mm }
}

/*@font-face {
  font-family: 'Arial_Monospaced_MT';
  src: url('<?= base_url().'assets/fonts/Arial_Monospaced_MT.ttf'; ?>') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
} */



* {
    font-family: 'Arial';
   font-size: 12px;
   font-weight: normal;
    word-spacing: 0px;
    letter-spacing: -1px;
    line-height: 1.2em;
}

body.receipt .sheet { width: 58mm;height: auto; } /* change height as you like */
@media print {
 body.receipt { width: 58mm } 
 .page-break { display: block; page-break-before: always; }
 * {
   font-family: 'Arial';
   font-size: 12px;
    word-spacing: 0px;
    word-break: break-all;
    letter-spacing: 1px;
    line-height: 1.2em;
}
 } /* this line is needed for fixing Chrome's bug */




  </style>
</head>
<?php 
if($this->session->userdata('id_agen')){
  $id_agen = $this->session->userdata('id_agen');
  $get = $this->db->get_where('data_outlet', array('id' => $id_agen))->row_array();
  $nama = $get['nama'];
  $alamat = $get['alamat'];
  $tlp = $get['tlp'];
}else{
  $nama = $setting->nama;
  $alamat = $setting->alamat;
  $tlp = $setting->notelp;
}
 ?>


<body class="receipt">
  <section class="sheet padding-10mm">
    <center><span style="font-weight: bold;font-size: 15px;"><?= $nama; ?></span> <br> <?= $alamat; ?> <br><?= $tlp; ?><br><br></center>
    <div style="width: 100%;">
      <div style="width: 50%;float: left;">Kasir</div>
      <div style="width: 50%;float: right;text-align: right;"><?= $this->session->userdata('nama'); ?></div>
    </div>
     <div style="width: 100%;">
      <div style="width: 30%;float: left;">Tanggal</div>
      <div style="width: 70%;float: right;text-align: right;">29-09-2020 16:45</div>
    </div>
    <table style="width: 100%;">
      <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
      <tr>
        <td colspan="2">Pengirim</td>
      </tr>
      <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
     

      <tr>
        <td colspan="2">Nama</td>
      </tr>
      <tr>
        <td colspan="2"><strong><?= $tr['nama_pengirim']; ?></strong></td>
      </tr>
      <tr>
        <td colspan="2">Alamat</td>
      </tr>
      <tr>
        <td colspan="2"><strong><?= $tr['alamat_pengirim']; ?></strong></td>
      </tr>
      <tr>
        <td colspan="2">No Telephone</td>
      </tr>
      <tr>
        <td colspan="2"><strong><?= $tr['hp_pengirim']; ?></strong></td>
      </tr>


      <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
      <tr>
        <td colspan="2">Penerima</td>
      </tr>
      <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>

      <tr>
        <td colspan="2">Nama</td>
      </tr>
      <tr>
        <td colspan="2"><strong><?= $tr['nama_penerima']; ?></strong></td>
      </tr>
      <tr>
        <td colspan="2">Alamat</td>
      </tr>
      <tr>
        <td colspan="2"><strong><?= $tr['alamat_penerima']; ?><?= $tr['kode_pos_penerima']; ?></strong></td>
      </tr>
      <tr>
        <td colspan="2">Provinsi/Kota</td>
      </tr>
      <tr>
        <td colspan="2"><strong><?= $tr['provinsi']; ?>/<?= $tr['kota']; ?></strong></td>
      </tr>
      <tr>
        <td colspan="2">No Telephone</td>
      </tr>
      <tr>
        <td colspan="2"><strong>Hp : <?= $tr['hp_penerima']; ?> <br> WA : <?= $tr['wa_penerima']; ?></strong></td>
      </tr>


       <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>

        <tr>
        <td>BOX <?= $tr['ukuran']; ?></td>
        <td style="text-align: right;"><?= decimals($tr['total']); ?></td>
      </tr>

       <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
      <tr>
        <td>Sub Total</td>
        <td style="text-align: right;"><?= decimals($tr['total']); ?></td>
      </tr>
       <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
      <tr>
        <td>Total Pembayaran</td>
        <td style="text-align: right;"><?= decimals($tr['bayar']); ?></td>
      </tr>
      <tr>
        <td>Kembalian</td>
        <td style="text-align: right;"><?= decimals($tr['kembali']); ?></td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center;">
          <img src="<?= base_url('barcode/'.$img_url); ?>" style="width: 50%;">
        </td>
      </tr>
    </table>
    
  </section>
  <script type="text/javascript">
      setTimeout(function () { window.print(); }, 500);
         window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
  </script>
</body>
</html>