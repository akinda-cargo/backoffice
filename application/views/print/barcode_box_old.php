<!DOCTYPE html>
<html>
<head>
	<title>Barcode produk</title>
</head>

<style type="text/css">
	body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #FAFAFA;
		font: 12px "Tahoma";
	}
	* {
		box-sizing: border-box;
		-moz-box-sizing: border-box;
	}
	tr td {
		padding-top: 5px;
		padding-bottom: 5px;
		padding-left: 5px;
		padding-right: 5px;
	}

	tr th {
		padding-top:5px;
		padding-bottom: 5px;
	}
	.page {
		width: 100mm;
		/* padding: 5mm; */
		margin: 1mm auto;
		border: 1px #D3D3D3 solid;
		border-radius: 5px;
		background: white;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	}
	.subpage {
		padding: 1cm;
		border: 5px red solid;
		height: 257mm;
		outline: 2cm #FFEAEA solid;
	}
	.page .fh5co-project {
		float: left;
		margin-bottom: -15px;
		width: 100px;
		height: 130px;
		-webkit-transform: rotate(90deg);
		-moz-transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		-o-transform: rotate(90deg);
		transform: rotate(90deg);
		/* border: 2px solid; */
	}
	.page .fh5co-project > a {
	text-align: center;
	text-decoration:none;
	}
	.page .fh5co-project > a img {
	width:100px;
	margin-bottom: -10px;
	font-size:10px;
	text-decoration:none;
	}
	.page .fh5co-project > a h2 {
	text-decoration:none;
	margin-bottom: 0px;
	font-size: 10px;
	color: #999999;
	-webkit-transition: 0.3s;
	-o-transition: 0.3s;
	transition: 0.3s;
	}
	.page .fh5co-project > a:hover h2, .page .fh5co-project > a:active h2, .page .fh5co-project > a:focus h2 {
	color: #000;
	}


	.text-right{
		text-align: right;
	}

	@media screen {
        div.footer {
            display: none;
        }
    }
    @media print {
        div.footer {
            position: fixed;
            right: 0;
            bottom: 0;
        }
    }

	@page {
		margin: 0;
	}
	@media print {
		html, body {
			width: 90;
			height: 40mm;        
		}
		.page {
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: always;
			padding-bottom: 30px;
		}
		 #foot {
    display: block;
    position: fixed;
    bottom: 0pt;
  }
	}

	.p9{
		font-size: 9pt;
	}

	.py8 tr td{
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<body>
	<div class="book">
		<div class="page">
			
			<table style="width: 100%;border-collapse: collapse;" border="1px">
				<tr>
					<th>
						Nomor Box
					</th>
					<th>
						Non COD
					</th>
					<th rowspan="3">
						TGL: 2021-02-03
						<img src="<?= base_url('barcode/'.$tr['no_resit'].'.png'); ?>" style="width: 50%;">
					</th>
				</tr>
				<tr>
					<td>
						No Box: <?= $tr['no_resit']; ?> <br>
						Kirim :<?= $tr['total']; ?> <br>
						Asuransi: 0
					</td>
					<td><strong>Total Biaya <br> <?= decimals($tr['total']); ?>(<?= $tr['mata_uang']; ?>)</strong></td>
				</tr>
				<tr>
					<td colspan="2" style="color:#FFF;background: #000;text-align: center;">Lembar Pengirim</td>
				</tr>
			</table>

			<table style="width: 100%;border-collapse: collapse;" border="1px">
				<tr>
					<td colspan="2" style="text-align: center;">
						<h1><?= $tr['no_resit']; ?></h1>
					</td>
				</tr>
				<tr>
					<td style="font-size: 12px;">
						Dengan menyantumkan paket ini , maka penerima telah mengkonfirmasi kebenarnan informasi resi dan isi paket ini ,
						serta telah memahami dan menyetujui syarat dan ketentuan umum Akinda Cargo
					</td>
					<td rowspan="2"><img src="<?= base_url('barcode/'.$tr['no_resit'].'.png'); ?>" style="width: 100%;"></td>
				</tr>
				<tr>
					<td  style="color:#FFF;background: #000;text-align: center;">Lembar Drop Poin Incoming</td>
				</tr>
			</table>

			<table style="width: 100%;border-collapse: collapse;" border="1px">
				<tr>
					<td colspan="3" style="text-align: center;">
						Penerima : <?= $tr['nama_penerima'].' , ' .$tr['hp_penerima'].' , '.$tr['kota'].' , '.$tr['alamat_penerima']; ?> <br>

						Pengirim : <?= $tr['nama_pengirim'].' , ' .$tr['hp_pengirim'].' , '.$tr['alamat_pengirim']; ?> <br>

						Jumlah : <?= 'Box '.$tr['ukuran'].' Barang '.$tr['isi'];  ?>

					</td>
				</tr>
			
				<tr>
					<td>
						No Box: <?= $tr['no_resit']; ?> <br>
						Kirim :<?= $tr['total']; ?> <br>
						Asuransi: 0
					</td>
					<td><strong>Total Biaya <br> <?= decimals($tr['total']); ?>(<?= $tr['mata_uang']; ?>)</strong></td>
					<td>Informasi Tambahan</td>
				</tr>
				<tr>
					<td  style="color:#FFF;background: #000;text-align: center;" colspan="3">Lembar Drop Poin Incoming</td>
				</tr>
			</table>


		</div>
			
	</div>
</body>
</html>