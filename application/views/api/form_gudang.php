
<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Gudang</h3> </div>

        </div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">

                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <form method="POST" id="formdata">
                                    <div class="row">
                                        <div class="col-md-6">                                                               
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control input-default" name="id_cabang">
                                                <option>-- Pilih Cabang --</option>
                                                <?php foreach($cabang as $row){ ?>
                                                    <option value="<?php echo $row['id_cabang']; ?>" <?php if(isset($id)){ if($gudang['id_cabang'] == $row['id_cabang']){ echo "selected"; } } ?>><?php echo $row['nama_cabang']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label>Nama gudang</label>
                                            <input name="nama_gudang" type="text" class="form-control input-default " placeholder="Nama Gudang" value="<?php if(isset($id)){ echo $gudang['nama_gudang']; } ?>">

                                             <?php if(isset($id)){ ?>
                                                <input type="hidden" name="id_gudang" value="<?php if(isset($id)){ echo $gudang['id_gudang']; } ?>">
                                            <?php } ?>
                                        </div>

                                    </div>

                                

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info col-md-2">Save</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $('#formdata').submit(function(event){
        event.preventDefault();
        var formdata = $(this).serialize();

            $.ajax({
                type: "POST",
                 url: "<?php echo base_url().'admin/simpan_gudang'; ?>",
                data: formdata,
                success: function(data){
                  
                    if(data == 1)
                    {
                         swal("Success!","Berhasil simpan gudang.", "success")
                        .then((value) => {
                          window.location = "<?php echo base_url().'admin/gudang'; ?>";
                      });
                    }

                }
            });

    });
</script>