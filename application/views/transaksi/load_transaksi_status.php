
<div class="col-md-12" style="padding-left: 15px;padding-right: 15px;">

    <table class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Petugas</th>
                <th>Note</th>
            </tr>
        </thead>
        <?php foreach ($transaksi_status as $i) : ?>
            <tbody>
                <tr>
                    <td><?= $i['tgl_transaksi'] ?></td>
                    <td><strong><?= $i['status'] ?></strong></td>
                    <td><?= $i['nama_petugas'] ?></td>
                    <td><?= $i['note'] ?></td>
                </tr>
            </tbody>
        <?php endforeach; ?>
    </table>

    <div class="row">
        <div class="col-md-4">
            <label>Update Status</label>
            <select class="form-control" name="status" id="status">
                <option value="">Pilih Status</option>
                <?php
                $get = $this->db->get_where('data_status_pengiriman', array('flag' => 1))->result_array();
                foreach ($get as $row) {
                    ?>
                    <option value="<?= $row['id']; ?>"><?= $row['nama_status']; ?></option>
                    <?php
                } ?>
            </select>
            <div class="invalid-feedback">
                Status harus di isi
            </div>
        </div>
        <div class="col-md-4">
            <label>Note</label>
            <textarea name="note" class="form-control"></textarea>
        </div>
        <div class="col-md-4" style="padding-top: 30px;">
            <input type="hidden" name="id_transaksi" value="<?= $id_transaksi; ?>">
            <button type="button" class="btn btn-success btn-status">Update Status</button>
        </div>
    </div>

</div>

<script type="text/javascript">
    $('.btn-status').click(function(){

        var status = $('#status').val();
        var id_transaksi = $('input[name=id_transaksi]').val();
        var note = $('textarea[name=note]').val();

        if(status == ''){
            $('#status').addClass('is-invalid');
            return false;
        }

        $(this).removeClass('btn-success').addClass('btn-secondary').text('loading..');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'transaksi/update_status/'; ?>",
            data: {id_transaksi:id_transaksi, status:status, note:note},
            success: function(data) {
                location.reload();                       

            }
        });

    });
</script>

