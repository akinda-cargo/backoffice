
  <div class="col-md-8" style="background-color: #000000b5;">

  </div>
  <div class="col-md-4" style="background-color: #f3f3f3;">
    <div style="padding-top: 15px;"><div style="text-align: right;font-size: 16px;cursor: pointer;" class="close-bill">X</div></div>
    <div class="bill-body">
    <table class="table">
      <tr>
        <td colspan="2"><strong>Data Pengirim</strong></td>
      </tr>
      <tr>
        <td>Nama Lengkap</td>
        <td><strong><?= $tr['nama_pengirim']; ?></strong></td>
      </tr>
      <tr>
        <td>Alamat Lengkap</td>
        <td><strong><?= $tr['alamat_pengirim']; ?></strong></td>
      </tr>
     
      <tr>
        <td>No Telephone</td>
        <td><strong><?= $tr['hp_pengirim']; ?></strong></td>
      </tr>
      <tr>
        
      </tr>
      <tr>
        <td colspan="2"><strong>Data Penerima</strong></td>
      </tr>
  
      <tr>
        <td>Nama Lengkap</td>
        <td><strong><?= $tr['nama_penerima']; ?></strong></td>
      </tr>
      <tr>
        <td>Alamat Lengkap</td>
        <td><strong><?= $tr['alamat_penerima']; ?><?= $tr['kode_pos_penerima']; ?></strong></td>
      </tr>
      <tr>
        <td>Provinsi/Kota</td>
        <td><strong><?= $tr['provinsi_penerima']; ?>/<?= $tr['kota_penerima']; ?></strong></td>
      </tr>
      <tr>
        <td>No Telephone</td>
        <td><strong>Hp : <?= $tr['hp_penerima']; ?> <br> WA : <?= $tr['wa_penerima']; ?></strong></td>
      </tr>
      <tr>
       
      </tr>
    </table>

      <div style="border-bottom: 2px dashed #aaa;
      margin: 20px 0;"></div>

      <table class="table table-bordered">
        <thead>
            <tr>
                <th>Barang</th>
                <th>Isi</th>
                <th>Tarif</th>
            </tr>
        </thead>
        <tbody>
           

                <tr>
                    <td>Box <?= $tr['ukuran']; ?></td>
                    <td><?= $tr['isi']; ?></td>
                    <td class="text-right"><?= decimals($tr['total']); ?></td>
                </tr>

              

        </tbody>
        <tfoot>
            <tr>
                <td colspan="2"><strong>Total</strong></td>
                <td class="text-right"><strong class="total" key="<?= $tr['total']; ?>"><?= decimals($tr['total']); ?></strong></td>
            </tr>
            <tr>
                <td colspan="2"><strong>Kembalian</strong></td>
                <td class="text-right txtkembalian" style="font-weight: bold;"></td>
            </tr>
        </tfoot>
    </table>

      <div style="border-bottom: 2px dashed #aaa;
      margin: 20px 0;"></div>

        <center>
      <img src="<?= base_url('barcode/'.$tr['no_resit'].'.png'); ?>">
</center>


    </div>
    <br>
    <div class="row" style="margin-bottom: 30px;">
      <div class="col-lg-12" style="text-align: right;">
       <a class="btn btn-info btn-sm print-struk" style="color: #FFF;" key="<?= $id_transaksi; ?>"><i class="fas fa-print"></i> Print</a>
     </div>
   </div>
   

 </div>