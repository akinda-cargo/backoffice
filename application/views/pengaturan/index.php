
<?php if($this->session->flashdata('success')){ ?>
  <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" >
    <div class="toast-body alert-success">
     <?= $this->session->flashdata('success'); ?>
   </div><!--toast-body-->
 </div><!--toast-->
<?php } ?>

<style type="text/css">
  .text-content{
    margin:0;
  }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Pengaturan</h1>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-body">
       <form class="form-update">
      <div class="row">
        <div class="col-lg-6">

        </div>
        <div class="col-lg-6" style="text-align: right;">
         <button type="submit" class="btn btn-success btn-fw">Simpan</button>
       </div>
     </div>

     <div class="row">
      <div class="col-lg-3">
        <p>Informasi Akun</p>
      </div>
      <div class="col-lg-9">
        <div class="row">
           <div class="col-lg-3">
            <div style="width: 100%;background: #FFF;height: 150px;background-image: url(<?= base_url('assets/uploads/' . $logo); ?>);background-size: contain;background-position: center;border: 1px solid #DDD; border-radius:5px;background-repeat: no-repeat;" class="div-image">

            </div>
            <input type="file" name="logo" id="logo" value="<?= @$logo; ?>" style="display: none;">
            <button class="btn btn-secondary btn-block mt-2" type="button" id="btn-logo">Ubah Logo</button>
          </div>
          <div class="col-lg-9">
           
              <?php 
              $settings = $this->db->get_where('setting', array('flag' => 1))->result_array();

              foreach ($settings as $row) {


               ?>
               <div class="form-group">
                <label><?= $row['field']; ?></label>
                <input type="text" name="<?= $row['field']; ?>" class="form-control input-sm" value="<?= $row['value']; ?>">
              </div>

            <?php } ?>
         
        </div>
      </div>

    </div>
  </div>
   </form>
</div>
</div>

</div>



<!-- /.container-fluid -->
<script type="text/javascript">
  $('.toast').toast({
    'animation':true,
    'autohide':false
  });
  $('.toast').toast('show');

  setTimeout(function(){
    $('.toast').toast('hide');
  },5000);

  $('.form-update').submit(function(){
    event.preventDefault();

    var formData = new FormData(this);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'pengaturan/perbarui'; ?>",
      data:formData,
      processData:false,
      contentType:false,
      cache:false,
      async:false,
      beforeSend: function() {
        $('.btn-submit').prop('disabled', true);
        $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
      },
      success: function(data) {

        location.reload();

     
      }
    });
  });



</script>

<script type="text/javascript">
  $('#btn-logo').click(function(){
    $('#logo').trigger('click');
  });

  $("#logo").change(function(){
    ShowImage(this);
});

 function ShowImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          
            $('.div-image').css('background-image', 'url('+e.target.result+')').css('background-position', 'center').css('background-size', 'contain').css('background-repeat', 'no-repeat');
        }

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
