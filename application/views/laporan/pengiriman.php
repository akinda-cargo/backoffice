<?php if ($this->session->flashdata('success')) { ?>
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body alert-success">
            <?= $this->session->flashdata('success'); ?>
        </div>
        <!--toast-body-->
    </div>
    <!--toast-->
<?php } ?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Laporan Pengiriman</h1>


    <div class="card shadow mb-3">
        <div class="card-body">
            <form action="" method="POST">
                <div class="row">

                    <div class="col-md-4" style="display: none;">
                        <label>Tanggal Awal</label>
                        <input type="date" name="tgl_awal" class="form-control" placeholder="Firt Date" value="<?php if (isset($tgl_awal)) {
                                                                                                                    echo $tgl_awal;
                                                                                                                } ?>">
                    </div>
                    <div class="col-md-4" style="display: none;">
                        <label>Tanggal Akhir</label>
                        <input type="date" name="tgl_akhir" class="form-control" placeholder="End Date" value="<?php if (isset($tgl_akhir)) {
                                                                                                                    echo $tgl_akhir;
                                                                                                                } ?>">
                    </div>
                    <div class="col-md-8">
                        <label>Nomor Resit</label>
                        <input type="text" name="no_resit" placeholder="Nomor Resit" class="form-control" value="<?= (isset($no_resit) ? $no_resit : false) ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="filter_status">Status Pengiriman</label>
                        <select name="status" id="filter_status" class="form-control">
                            <option value="">-- PILIH --</option>
                            <?php
                            $get_status = $this->db->get('data_status_pengiriman')->result_array();
                            ?>
                            <?php foreach ($get_status as $i) : ?>
                                <?php if (isset($status)) : ?>
                                    <?php if ($status == $i['id']) : ?>
                                        <option value="<?= $i['id'] ?>" selected><?= $i['nama_status'] ?></option>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <option value="<?= $i['id'] ?>"><?= $i['nama_status'] ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <?php if ($this->session->userdata('level') == 1) { ?>
                        <div class="col-md-2 mt-2">
                            <label>Agen</label>
                            <select class="form-control" name="id_agen">
                                <option value="">Pilih Agen</option>
                                <?php foreach ($agen as $row) {
                                ?>
                                    <option value="<?= $row['id']; ?>" <?php if (isset($id_agen)) {
                                                                            if ($row['id'] == $id_agen) {
                                                                                echo "selected";
                                                                            }
                                                                        } ?>><?= $row['nama']; ?></option>
                                <?php
                                } ?>
                            </select>
                        </div>
                        <div class="col-md-2 mt-2">
                            <label>Otlet</label>
                            <select class="form-control" name="id_outlet">
                                <option value="">Pilih Otlet</option>
                                <?php foreach ($outlet as $row) {
                                ?>
                                    <option value="<?= $row['id']; ?>" <?php if (isset($id_outlet)) {
                                                                            if ($row['id'] == $id_outlet) {
                                                                                echo "selected";
                                                                            }
                                                                        } ?>><?= $row['nama']; ?></option>
                                <?php
                                } ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="col-md-2 mt-2">
                        <label>Negara</label>
                        <select class="form-control" name="kode_negara">
                            <option value="">Pilih Negara</option>
                            <?php foreach ($negara as $row) {
                            ?>
                                <option value="<?= $row['id']; ?>" <?php if (isset($kode_negara)) {
                                                                        if ($row['id'] == $kode_negara) {
                                                                            echo "selected";
                                                                        }
                                                                    } ?>><?= $row['nama_negara']; ?></option>
                            <?php
                            } ?>
                        </select>
                    </div>
                    <div class="col-md-2 mt-2">
                        <label>Negara Tujuan</label>
                        <select class="form-control" name="kode_negara_tujuan">
                            <option value="">Pilih Negara</option>
                            <?php foreach ($negara as $row) {
                            ?>
                                <option value="<?= $row['id']; ?>" <?php if (isset($kode_negara_tujuan)) {
                                                                        if ($row['id'] == $kode_negara_tujuan) {
                                                                            echo "selected";
                                                                        }
                                                                    } ?>><?= $row['nama_negara']; ?></option>
                            <?php
                            } ?>
                        </select>
                    </div>
                    <div class="col-md-12 text-right mt-2">
                        <button type="submit" class="btn btn-info"><i class="fas fa-search"></i> Cari</button>
                        <a href="" class="btn btn-secondary">Reset</a>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary title-file">Laporan Pengiriman</h6>
            <div id="tableActions"></div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered text-left" id="dataexcel" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Tanggal Transaksi</th>
                            <th>No. Resit</th>
                            <th>Resit Lama</th>
                            <th>Nama Pengirim</th>
                            <th>Nama Penerima</th>
                            <th class="text-right">Total</th>
                            <th>Alamat Pengirim</th>
                            <th>Alamat Tujuan</th>
                            <th>Status</th>
                            <th>Agen</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($list as $row) {

                            $agent = '-';
                            if ($row['id_agen'] != '') {
                                $agent = $row['nama_agen'];
                            }
                        ?>

                            <tr>
                                <td><?= date('Y/m/d H:i',  strtotime($row['created_at'])); ?></td>
                                <td><?= $row['no_resit'] ?></td>
                                <td><?= $row['no_resit_lama'] ?></td>
                                <td><?= $row['nama_pengirim'] ?></td>
                                <td><?= $row['nama_penerima'] ?></td>
                                <td class="text-right"><?= decimals_excel($row['total']); ?></td>
                                <td><?= $row['alamat_pengirim'] . ', <br>' . strtoupper($row['nama_negara']); ?></td>
                                <td><?= $row['alamat_penerima'] . ', <br>' . strtoupper($row['negara_tujuan']); ?></td>
                                <td><?= $row['nama_status'] ?></td>
                                <td><?= $agent ?></td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
    $('.toast').toast({
        'animation': true,
        'autohide': false
    });
    $('.toast').toast('show');

    setTimeout(function() {
        $('.toast').toast('hide');
    }, 5000);


    var tables = $('#dataexcel').DataTable({
        "columnDefs": [{
            "targets": [0, 4, 5, 6, 7, 8],
            "searchable": false
        }],
        "order": [[ 0, "desc" ]]
    });
    new $.fn.dataTable.Buttons(tables, {
        buttons: [{
                extend: 'excel',
                text: '<i class="fa fa-file-excel"></i> Excel',
                title: $('.title-file').text().trim(),
                className: 'btn btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i> Print',
                title: $('.title-file').text().trim(),
                className: 'btn btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
    });
    tables.buttons().container().appendTo('#tableActions');
</script>