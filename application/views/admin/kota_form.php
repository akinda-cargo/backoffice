<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Kota</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Kota</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax" data-uri="<?= base_url('admin/save_kota'); ?>" data-redirect="<?= base_url('admin/kota'); ?>">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (@$id) {
                            echo "<input type='hidden' name='id' value='$id'>";
                        }
                        ?>
                        <div class="form-group">
                            <label>Negara</label>
                            <select class="select2 form-control" name="id_negara" id="selectNegara">
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_negara as $item) {
                                ?>
                                    <option value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_negara) ? 'selected' : ''; ?>><?= $item['nama_negara'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Provinsi</label>
                            <select class="select2 form-control related-select" name="id_prov" data-target="#selectNegara">
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_prov as $item) {
                                ?>
                                    <option class="rs<?= $item['id_negara'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_prov) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nama Kota</label>
                            <input type="text" name="nama" class="form-control" value="<?= @$nama ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Kota</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>