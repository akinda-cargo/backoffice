<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Pegawai / Pengguna</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Pegawai</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax" data-uri="<?= base_url('admin/save_employes'); ?>" data-redirect="<?= base_url('admin/employes'); ?>">
                <?php
                if (@$id) {
                    echo "<input type='hidden' name='id' value='$id'>";
                }
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="<?= @$email ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" value="<?= @$nama ?>" required>
                        </div>
                    </div>
                    <?php
                    if (!isset($id)) {
                    ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" required>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Telephone</label>
                            <input type="number" name="tlp" class="form-control" value="<?= @$tlp ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Level</label>
                            <select class="form-control" name="level" required>
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_level as $item) {
                                ?>
                                    <option value="<?= $item['id'] ?>" <?= ($item['id'] == @$level) ? 'selected' : ''; ?>><?= $item['nama_level'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Otlet / Agen</label>
                            <select class="form-control" name="id_agen">
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_outlet as $item) {
                                ?>
                                    <option value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_agen) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="flag" class="form-control" required>
                                <option value="">-- PILIH --</option>
                                <option <?= (@$flag == '1') ? 'selected' : '' ?> value="1">Aktif</option>
                                <option <?= (@$flag == '0') ? 'selected' : '' ?> value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Pegawai</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>