    <!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Tarif</h1>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Tarif</h6>
    </div>
    <div class="card-body">
      <form class="form-ajax" data-uri="<?= base_url('admin/save_rates'); ?>" data-redirect="<?= base_url('admin/rates'); ?>">
        <?php
        if (@$id) {
          echo "<input type='hidden' name='id' value='$id'>";
        }
        ?>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Kode Tarif</label>
              <input type="text" name="kode_tarif" class="form-control" value="<?= @$kode_tarif ?>" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Ukuran</label>
              <input type="text" name="ukuran" class="form-control" value="<?= @$ukuran ?>" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Harga</label>
              <input type="number" name="harga" class="form-control" value="<?= @$harga ?>" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Status</label>
              <select name="flag" class="form-control" required>
                <option value="">-- Choose --</option>
                <option <?= (@$flag == '1') ? 'selected' : '' ?> value="1">Aktif</option>
                <option <?= (@$flag == '0') ? 'selected' : '' ?> value="0">Tidak Aktif</option>
              </select>
            </div>
          </div>
          <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-success btn-submit">Simpan Tarif</button>
          </div>
        </div>
      </form>
    </div>
  </div>

</div>