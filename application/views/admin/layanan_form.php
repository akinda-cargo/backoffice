

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Layanan</h1>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Form Layanan</h6>
                        </div>
                        <div class="card-body">
                            <form class="form-submit">
                          <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-success btn-submit">Simpan Layanan</button>
                            </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                      <label>Kelompok Layanan</label>
                                      <select class="form-control" name="id_kelompok" required="">
                                          <option>Pilih Kelompok</option>
                                          <?php foreach ($kelompok as $k) { ?>
                                              <option value="<?= $k['id'];?>" <?php if(isset($id)){ if($row['id_kelompok'] == $k['id']){ echo "selected"; } } ?>><?= $k['nama'];?></option>
                                         <?php } ?>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <label>Cabang</label>
                                      <select class="form-control" name="id_cabang" required="">
                                          <option>Pilih Cabang</option>
                                          <?php foreach ($cabang as $c) { ?>
                                              <option value="<?= $c['id'];?>" <?php if(isset($id)){ if($row['id_cabang'] == $c['id']){ echo "selected"; } } ?>><?= $c['nama'];?></option>
                                         <?php } ?>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <label>Nama Layanan</label>
                                      <input type="text" name="nama" class="form-control" required="" value="<?php if(isset($id)){ echo $row['nama']; } ?>">
                                      <?php if(isset($id)){  ?>
                                        <input type="hidden" name="id_layanan" value="<?=$id;?>">
                                      <?php } ?>
                                  </div>
                                 
                              </div>

                              <div class="col-md-6">
                                   <div class="form-group">
                                      <label>No Whatapps</label>
                                      <input type="text" name="no_wa" class="form-control" required="" value="<?php if(isset($id)){ echo $row['wa']; } ?>">
                                  </div>
                                  <div class="form-group">
                                      <label>Keterangan</label>
                                      <textarea class="form-control" name="keterangan"><?php if(isset($id)){ echo $row['ket']; } ?></textarea>
                                  </div>
                              </div>
                          </div>
                          </form>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->
<script type="text/javascript">
     $(".form-submit").submit(function(event){
  event.preventDefault();

  var formData = new FormData(this);

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/simpan_layanan'; ?>",
    data:formData,
    processData:false,
    contentType:false,
    cache:false,
    async:false,
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {

      if(data == 1)
      {
        window.location = '<?php echo base_url().'admin/layanan'; ?>';
      }
    }
  });

});
</script>