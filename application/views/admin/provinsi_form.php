<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Provinsi</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Provinsi</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax" data-uri="<?= base_url('admin/save_provinsi'); ?>" data-redirect="<?= base_url('admin/provinsi'); ?>">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (@$id) {
                            echo "<input type='hidden' name='id' value='$id'>";
                        }
                        ?>
                        <div class="form-group">
                            <label>Country</label>
                            <select class="select2 form-control" name="id_negara">
                                <option value="">-- Pilih --</option>
                                <?php
                                foreach ($list_negara as $item) {
                                ?>
                                    <option value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_negara) ? 'selected' : ''; ?>><?= $item['nama_negara'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nama Provinsi</label>
                            <input type="text" name="nama" class="form-control" value="<?= @$nama ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Provinsi</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>