<?php if ($this->session->flashdata('success')) { ?>
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body alert-success">
            <?= $this->session->flashdata('success'); ?>
        </div>
        <!--toast-body-->
    </div>
    <!--toast-->
<?php } ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Slider</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Data Slider</h6>

            <div><a href="<?= base_url('admin/form_slider'); ?>" class="btn btn-info btn-sm">Tambah Baru</a></div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Gambar</th>
                            <th width="30%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr>
                                <td><img class="img-thumbnail w-100" src="<?= base_url('assets/banner/' . $row['gambar']); ?>" alt="<?= $row['gambar'] ?>"></td>
                                <td>
                                    <a href="<?= base_url('admin/form_slider/' . $row['id']); ?>" class="btn btn-primary btn-sm ">Ubah</a>
                                    <a href="<?= base_url('admin/delete_slider/' . $row['id']); ?>" class="btn btn-danger btn-sm d-inline">Hapus</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>