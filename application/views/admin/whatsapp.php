<?php if ($flash) { ?>
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body alert-success">
            <?php echo $flash; ?>
        </div>
        <!--toast-body-->
    </div>
    <!--toast-->
<?php } ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Device Whatsapp</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Data Whatsapp</h6>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Device</th>
                            <th>Nama Alias</th>
                            <th>Balance</th>
                            <th>Device</th>
                            <th>Tanggal Expied</th>
                            <th>Status Device</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($devices as $row) { ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row['id_device']; ?></td>
                                <td><?= $row['nama_alias']; ?></td>
                                <td><?= $row['balance']; ?></td>
                                <td><?= $row['sender']; ?></td>
                                <td><?= $row['expired']; ?></td>
                                <td><?= $row['device_status']; ?></td>
                                <td><a href="<?= base_url('whatsapp/cek_all_device'); ?>" class="btn btn-primary btn-sm">Cek Device</a>
                                    <a href="#" class="btn btn-info btn-sm btn-cek">Lihat Qr</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="modal fade bd-example-modal-lg" id="modal-action" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title-action"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row align-items-center loading" style="display: flex;justify-content: center;min-height: 400px;width: 100%;">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" role="status" style="width: 3rem;height: 3rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            <div class="row" style="padding:10px;text-algin:center;" id="load_action">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

 $('.toast').toast({
        'animation': true,
        'autohide': false
    });
    $('.toast').toast('show');

    setTimeout(function() {
        $('.toast').toast('hide');
    }, 5000);
    
  $('.btn-cek').click(function() {
        id = $(this).attr('key');
        $('#load_action').html('');
        $('#modal-title-action').text('Show Device');
        $('#modal-action').modal('show');
        $('.loading').show();
        $('#load_action').load('<?= base_url('whatsapp/show_qr') ?>/', function() {
            $('.loading').hide();
        });
    })
</script>