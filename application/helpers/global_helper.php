<?php 

function getSetting($data){
		if($data != "semua"){
		$this->db->where("field",$data);
		}
		$res = $this->db->get("setting");
		$result = null;
		if($data == "semua"){
			$result = array(null);
			foreach($res->result() as $re){
				$result[$re->field] = $re->value;
			}
			$result = (object)$result;
		}else{
			$result = "";
			foreach($res->result() as $re){
				$result = $re->value;
			}
		}
		return $result;
	}

function rupiah($angka){
	if(is_numeric($angka)){

		if($angka == ''){
		return 'Rp 0';
		exit;
		}

		if($angka == 0){
			return 'Rp 0';
			exit;
		}

		$angka = floor($angka);
		$hasil_rupiah = "Rp " . number_format($angka);
		return $hasil_rupiah;
	}else{
		return 'Rp 0';
			exit;
	}
	
 
}

function datetime_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);





$splitTimeStamp = explode(" ",$tanggal);
$date = $splitTimeStamp[0];
$time = date('h:i' , strtotime($splitTimeStamp[1]));


	$pecahkan = explode('-', $date);
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0]. ' ' . $time;
}

 function digit_random(){
        $digits = 3;
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

function date_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);

	$hari = array ( 1 =>    'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			'Jumat',
			'Sabtu',
			'Minggu'
		); 


	$pecahkan = explode('-', $tanggal);
	$num = date('N', strtotime($tanggal)); 
 
	//return $hari[$num] . ' ' . $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}


if (!function_exists('dateIndo')) {
	function dateIndo($data, $format='d-m-Y') {
		$bulanIndo = array(
					'01' => 'Januari',
					'02' => 'Februari',
					'03' => 'Maret',
					'04' => 'April',
					'05' => 'Mei',
					'06' => 'Juni',
					'07' => 'Juli',
					'08' => 'Agustus',
					'09' => 'September',
					'10' => 'Oktober',
					'11' => 'November',
					'12' => 'Desember',
				);
		if($format=='d-m-Y'){
			$date = explode('-', date($format, strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			return $tanggal.' '.$bulan.' '.$tahun;
		}elseif($format=='d-m-Y H:i'){
			$date = explode('-', date('d-m-Y-H-i', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			$jam = $date[3];
			$menit = $date[4];
			return $tanggal.'-'.$bulan.'-'.$tahun.' '.$jam.':'.$menit;
		}elseif($format=='d-m-Y H:i:s'){
			$date = explode('-', date('d-m-Y-H-i-s', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			$jam = $date[3];
			$menit = $date[4];
			$detik = $date[5];
			return $tanggal.'-'.$bulan.'-'.$tahun.' '.$jam.':'.$menit.':'.$detik;
		}elseif($format=='d m Y, H.i'){
			$date = explode('-', date('d-m-Y-H-i', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			$jam = $date[3];
			$menit = $date[4];
			return $tanggal.' '.$bulan.' '.$tahun.', '.$jam.'.'.$menit;
		}elseif($format=='d m Y H.i'){
			$date = explode('-', date('d-m-Y-H-i', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			$jam = $date[3];
			$menit = $date[4];
			return $tanggal.' '.$bulan.' '.$tahun.' '.$jam.'.'.$menit;
		}elseif($format=='d m Y, H:i'){
			$date = explode('-', date('d-m-Y-H-i', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			$jam = $date[3];
			$menit = $date[4];
			return $tanggal.' '.$bulan.' '.$tahun.', '.$jam.':'.$menit;
		}elseif($format=='d m Y'){
			$date = explode('-', date('d-m-Y', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			return $tanggal.' '.$bulan.' '.$tahun;
		}elseif($format=='H.i'){
			$date = explode('-', date('H-i', strtotime($data)));
			$jam = $date[0];
			$menit = $date[1];
			return $jam.'.'.$menit;

		}elseif($format=='H:i'){
			$date = explode('-', date('H-i', strtotime($data)));
			$jam = $date[0];
			$menit = $date[1];
			return $jam.':'.$menit;
		}elseif($format=='m Y'){
			$date = explode('-', date('d-m-Y', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			return $bulan.' '.$tahun;
		}elseif($format=='m'){
			$date = explode('-', date('d-m-Y', strtotime($data)));
			$tanggal = $date[0];
			$bulan = $bulanIndo[$date[1]];
			$tahun = $date[2];
			return $bulan;
		}
	}
}


if (!function_exists('format_decimals')) {
	function format_decimals($value){
		if($value != 0 || $value != '')
		{
			if(isset($value))
			{
				return "Rp. ".number_format($value,0,",",".");
			}

		}else{
			return 0;
		}

	}
}

if (!function_exists('decimals')) {
	function decimals($value){
		if($value != 0 || $value != '')
		{
			if(isset($value))
			{
				return number_format($value,0,",",".");
			}

		}else{
			return 0;
		}

	}
}

if (!function_exists('decimals_excel')) {
	function decimals_excel($value){
		if($value != 0 || $value != '')
		{
			if(isset($value))
			{
				return number_format($value,0,",",",");
			}

		}else{
			return 0;
		}

	}
}


    function sendMessage($title,$text) {
    $content      = array(
        "en" => $text
    );
    $heading = array(
       "en" => $title
    );

    $fields = array(
        'app_id' => "000622ac-d803-4e1f-b7db-6b4e1451b94e",
        'included_segments' => array(
            'All'
        ),
        'data' => array(
            "foo" => "bar"
        ),
        'contents' => $content,
        'headings' => $heading

    );

    $fields = json_encode($fields);
    print("\nJSON sent:\n");
    print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic Yzk4ZTU2YmYtMWM1NS00ZTg5LTk1ZGYtNDE2M2EzODNmOTY0'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}

 function push($title,$text)
{
  $response = sendMessage($title,$text);
  $return["allresponses"] = $response;
  $return = json_encode($return);

  $data = json_decode($response, true);
  print_r($data);
  $id = $data['id'];
  print_r($id);

  print("\n\nJSON received:\n");
  print($return);
  print("\n");
}



 ?>