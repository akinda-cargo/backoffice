<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masuk extends CI_Controller {

	public function index()
	{
		$this->load->view('masuk/masuk');
	}

	public function dologin()
	{
		if($data = $this->input->post())
		{
			$pass = $data['password'];
			$email = $data['email'];

			// $pass = '12312312';
			// $email = '34534534534';
			
			$get = $this->db->get_where('admin',array('email' => $email));
			
			if($hasil = $get->row_array())
			{

				if(password_verify($pass, $hasil['password'])){
				
					$sess['userid'] = $hasil['id'];
					$sess['nama'] = $hasil['nama'];
					$sess['level'] = $hasil['level'];
					if($hasil['id_agen'] != ''){
						$sess['id_agen'] = $hasil['id_agen'];
					}
					$this->session->set_userdata($sess);

					echo json_encode(array('status' => true, 'msg' => 'Berhasil masuk'));
				}else{
					echo json_encode(array('status' => false, 'msg' => 'Email atau password salah'));
				}				

				

			}else{
					echo json_encode(array('status' => false, 'msg' => 'Data tidak di temukan'));
			}

		}


	}

	public function show_sess(){
		var_dump($this->session->userdata());
	}

	public function keluar()
	{
		$this->session->sess_destroy();
		redirect(base_url().'masuk','refresh');
	}

}

/* End of file Masuk.php */
/* Location: ./application/controllers/Masuk.php */ ?>