<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Export extends CI_Controller
{
    public function index()
    {
        $size = [
            'K' => 1,
            'S' => 2,
            'B' => 3,
            'XL' => 4,
            'L3' => 4,
        ];

        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
        $excelreader = new PHPExcel_Reader_Excel2007();

        $dir    = 'excel';
        $excel = scandir($dir);
        foreach ($excel as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                $loadexcel = $excelreader->load('excel/' . $value);
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
                unset($sheet[1]);

                foreach ($sheet as $key => $value) {
                    if ($value['A'] != null && $value['A'] != 'NO_RESIT') {
                        $this->db->like('nama', $value['E'], 'both');
                        $tujuan = $this->db->get('t_kabupaten')->row_array();

                        $trans['kode_transaksi'] = 'TR' . date('YmdHis');
                        $trans['no_resit_lama'] = $value['A'];
                        $trans['container'] = $value['B'];
                        $trans['kode_negara'] = '2';
                        $trans['kode_negara_tujuan'] = '1';
                        $trans['status'] = '1';
                        $trans['id_user'] = '1';
                        $trans['isi'] = $value['P'];
                        $trans['platform'] = 'web';
                        $trans['id_size'] = @$size[$value['G']];
                        $trans['created_at'] = date('Y-m-d H:i:s');

                        $this->db->insert('transaksi', $trans);

                        $id_trans = $this->db->insert_id();

                        $pelanggan['id_transaksi'] = $id_trans;
                        $pelanggan['identity_type'] = 'Paspor';
                        $pelanggan['no_pasport'] = $value['J'];
                        $pelanggan['nama_pengirim'] = $value['I'];
                        $pelanggan['hp_pengirim'] = $value['L'];
                        $pelanggan['wa_pengirim'] = $value['L'];
                        $pelanggan['alamat_pengirim'] = $value['K'];
                        $pelanggan['identity_type_penerima'] = null;
                        $pelanggan['no_pasport_penerima'] = null;
                        $pelanggan['nama_penerima'] = $value['M'];
                        $pelanggan['hp_penerima'] = $value['O'];
                        $pelanggan['wa_penerima'] = $value['O'];
                        $pelanggan['id_negara_penerima'] = '1';
                        $pelanggan['id_provinsi_penerima'] = @$tujuan['id_prov'];
                        $pelanggan['id_kota_penerima'] = @$tujuan['id'];
                        $pelanggan['kode_pos_penerima'] = null;
                        $pelanggan['alamat_penerima'] = $value['N'];

                        $this->db->insert('transaksi_data_pelanggan', $pelanggan);
                        echo $this->db->last_query();
                        echo '<br>';

                        $status['id_transaksi'] = $id_trans;
                        $status['status'] = '1';
                        $status['id_user'] = '1';
                        $this->db->insert('transaksi_status', $status);
                    }
                }
            }
        }
    }
}

/* End of file Controllername.php */
