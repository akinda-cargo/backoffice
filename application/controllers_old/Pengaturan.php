<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userid'])) {
			redirect(base_url() . 'masuk', 'refresh');
		} else {
		}
	}

	public function index()
	{
		$data['logo'] =  $this->global->getSetting("logo");

		$this->load->view('admin/header');
		$this->load->view('pengaturan/index',$data);
		$this->load->view('admin/footer');
	}

	public function outlet(){
		

		$id_agen = $this->session->userdata('id_agen');
		$this->db->where('id', $id_agen);
		$data = $this->db->get('data_outlet')->row_array();
		$data['list_negara'] = $this->db->get('data_negara')->result_array();
		$data['list_prov'] = $this->db->get('t_provinsi')->result_array();
		$data['list_kab'] = $this->db->get('t_kabupaten')->result_array();

		$this->load->view('admin/header');
		$this->load->view('pengaturan/outlet', $data);
		$this->load->view('admin/footer');
	}

	public function perbarui(){
		if($post = $this->input->post()){
			$settings = $this->db->get_where('setting', array('flag' => 1, 'field != ' => 'logo'))->result_array();

			$data = array();
			foreach ($settings as $row) {

				


				$field = $row['field'];

				$data['value'] = $post[$field];

				$this->db->update('setting', $data , array('field' => $field));

				
				
			}

			if ($_FILES['logo']['name'] != '') {

					$random_name = 'logo-' . date('YmdHis');
					$ext = '.' . pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
					$config['upload_path'] = 'assets/uploads/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['file_name'] = $random_name;
					$this->load->library('upload', $config);


				// upload file
					$this->upload->do_upload('logo');
					$up['value'] = $random_name . $ext;

					$this->db->update('setting', $up , array('field' => 'logo'));
					echo $this->db->last_query();
				}




		}
	}

	public function perbarui_outlet()
	{
		$post = $this->input->post();

		if ($_FILES['logo']['name'] != '') {
			
			$random_name = 'agen_' . date('YmdHis');
			$ext = '.' . pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
			$config['upload_path'] = 'assets/uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name'] = $random_name;
			$this->load->library('upload', $config);
			

				// upload file
			$this->upload->do_upload('logo');
			$post['logo'] = $random_name . $ext;
		}

		if (@$post['id']) {
			$this->db->where('id', $post['id']);
			$this->db->update('data_outlet', $post);
		} 
		$json['res'] = 'success';
		echo json_encode($json);
	}

}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */ ?>