<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Whatsapp extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userid'])) {
			redirect(base_url() . 'masuk', 'refresh');
		} else {
		}
	}

	public function index()
	{
		$list = $this->db->get('data_whatsapp')->result_array();
		$page['menu'] = 'whatsapp';
		$page['sub_menu'] = 'whatsapp';
		$data['devices'] = $list;
		$data['flash'] = $this->session->flashdata('success');
		
		$this->load->view('admin/header', $page);
		$this->load->view('admin/whatsapp', $data);
		$this->load->view('admin/footer');
	}
	
	public function cek_all_device(){
	    $res = $this->global->info_devices();
	    $stat = $this->global->get_device_wa();
	    
	    $obj = json_decode($res, true);
	    $row = json_decode($stat, true);
	       
	   
	    $data['nama_alias'] =  $obj['nama'];
	    $data['balance'] =  $obj['balance'];
	    $data['sender'] = $obj['token']['sender'];
	    $data['expired'] = $obj['token']['expired'];
	    $data['device_status'] = $row['device_status'];
	    
	    $id_device = $obj['token']['id'];
	    
	    if($this->db->update('data_whatsapp', $data, array('id_device' => $id_device))){
	        $this->session->set_flashdata('success', 'Berhasil update device status Whatsapp');
	       redirect(base_url('whatsapp'), 'refresh');
	    }
	}
	
	public function logout_wa(){
		$res = $this->global->logout_wa();
		var_dump($res);
	}

	public function get_status_device(){
		$res = $this->global->get_device_wa();
			echo "<pre>";
		print_r($res);
	}

	public function show_qr(){
		$res = $this->global->show_qr();
		echo $res;
	}

	public function info_devices(){
		$res = $this->global->info_devices();
		echo "<pre>";
		print_r($res);
	}


}