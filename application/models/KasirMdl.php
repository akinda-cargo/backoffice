<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KasirMdl extends CI_Model
{

    function customer_fetch($q)
    {
        $this->db->like('nama_lengkap', $q, 'both');

        // if ($id_agen = $this->session->userdata('id_agen')) {
        //     $this->db->where('id_agen', $id_agen);
        // }
        $query = $this->db->get('data_users');

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $output[] = array(
                    'nama'  => $row["nama_lengkap"],
                    'id' => $row['id']
                );
            }
            echo json_encode($output);
        }
    }
}

/* End of file KasirMdl.php */
/* Location: ./application/models/KasirMdl.php */
