<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GlobalModel extends CI_Model
{

	function getSetting($data)
	{
		if ($data != "semua") {
			$this->db->where("field", $data);
		}
		$res = $this->db->get("setting");
		$result = null;
		if ($data == "semua") {
			$result = array(null);
			foreach ($res->result() as $re) {
				$result[$re->field] = $re->value;
			}
			$result = (object)$result;
		} else {
			$result = "";
			foreach ($res->result() as $re) {
				$result = $re->value;
			}
		}
		return $result;
	}

	function getRandomWasap()
	{
		$this->db->order_by("tgl", "ASC");
		$this->db->limit(1);
		$res = $this->db->get("wasap");

		$result = 0;
		foreach ($res->result() as $r) {
			if (substr($r->wasap, 0, 1) == 0) {
				$result = "+62" . substr($r->wasap, 1);
			} elseif (substr($r->wasap, 0, 2) == "62") {
				$result = "+" . $r->wasap;
			} elseif (substr($r->wasap, 0, 1) == "+") {
				$result = $r->wasap;
			}
		}
		return $result;
	}

	public function sendWAOK($nomer, $pesan)
	{
		$key = $this->getSetting("woowa");
		$nomer = intval($nomer);
		$nomer = substr($nomer, 0, 2) != "62" ? "+62" . $nomer : "+" . $nomer;
		$url = 'http://116.203.92.59/api/send_message';
		$data = array(
			"phone_no"	=> $nomer,
			"key"		=> $key,
			"message"	=> $pesan

		);
		$data_string = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		$res = curl_exec($ch);
		curl_close($ch);


		if ($res == "Success") {
			return 'bisa';
		} else {
			return 'gabisa';
		}
	}

	public function send_wablas($nomer, $pesan)
	{
		$key = $this->getSetting("wablas_api");
		$nomer = intval($nomer);
		$nomer = substr($nomer, 0, 2) != "62" ? "+62" . $nomer : "+" . $nomer;

		$curl = curl_init();
		$token = "";
		$data = [
			'phone' => $nomer,
			'message' => $pesan,
		];

		curl_setopt(
			$curl,
			CURLOPT_HTTPHEADER,
			array(
				"Authorization: $key",
			)
		);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($curl, CURLOPT_URL, "https://teras.wablas.com/api/send-message");
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		$result = curl_exec($curl);
		curl_close($curl);

		echo "<pre>";
		print_r($result);
	}


	public function send_text_wa($nomer,$pesan){

		$key = $this->getSetting("ruangwa_api");
		//$nomer = intval($nomer);
		//$nomer = substr($nomer,0,2) != "62" ? "+62".$nomer : "+".$nomer;

		$token = $key;
		$phone = $nomer;
		$message = $pesan;
		$url = 'http://ruangwa.com/api/send-message.php';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_TIMEOUT,30);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'token'    => $token,
			'phone'     => $phone,
			'message'   => $message,
		));
		$result = curl_exec($curl);
		curl_close($curl); 

		return $result;
	}
	
	public function logout_wa(){
		$key = $this->getSetting("ruangwa_api");

		$token = $key;	
		$url = 'http://ruangwa.com/v2/api/del-session.php';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_TIMEOUT,30);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'token'    => $token
		));
		$result = curl_exec($curl);
		curl_close($curl); 

		return $result;
	}

	public function get_device_wa(){
		$key = $this->getSetting("ruangwa_api");

		$token = $key;	
		$url = 'http://ruangwa.com/v2/api/device.php';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_TIMEOUT,30);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'token'    => $token
		));
		$result = curl_exec($curl);
		curl_close($curl); 

		return $result;
		
	}

	public function show_qr(){
		$key = $this->getSetting("ruangwa_api");

		$token = $key;	
		$url = 'http://ruangwa.com/v2/api/qrcode-image.php';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_TIMEOUT,30);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'token'    => $token
		));
		$result = curl_exec($curl);
		curl_close($curl); 

		return $result;
		
	}

	public function info_devices(){
		$key = $this->getSetting("ruangwa_api");

		$token = $key;	
		$url = 'http://ruangwa.com/v2/api/info.php';
		$username = 'akinda';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_TIMEOUT,30);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'token'    => $token,
			'username'	=> $username
		));
		$result = curl_exec($curl);
		curl_close($curl); 

		return $result;
	}


	public function send_file_wa($nomer,$pesan,$url_dokumen){

		$key = $this->getSetting("ruangwa_api");
		//$nomer = intval($nomer);
		//$nomer = substr($nomer,0,2) != "62" ? "+62".$nomer : "+".$nomer;

		$token = $key;
		$phone = $nomer;
		$message = $pesan;
		$url = 'http://ruangwa.com/v2/api/send-document.php';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_TIMEOUT,30);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'token'    => $token,
			'phone'     => $phone,
			'document'	=> $url_dokumen,
			'caption'   => $message,
			
		));
		$result = curl_exec($curl);
		curl_close($curl); 

		return true;
	}



	function outlet()
	{
		$this->db->select('COUNT(id) as total, type_outlet ');
		$this->db->group_by('type_outlet');
		$get = $this->db->get('data_outlet');

		return $get->result_array();
	}

	function create_log_print($id_transaksi,$id_user,$print){

		$get = $this->db->get_Where('log_print', array('id_transaksi' => $id_transaksi , 'print' => $print, 'id_user' => $id_user));

		if($get->num_rows() > 0){

			$rowp = $get->row_array();
			
			$log = array(
				'counter'		=> $rowp['counter']+1,
				'updated_at'	=> date('Y-m-d H:i:s')
			);

			$this->db->update('log_print', $log, array('id_transaksi' => $id_transaksi , 'print' => $print, 'id_user' => $id_user));

		}else{

			$log = array(
				'id_transaksi'	=> $id_transaksi,
				'id_user'		=> $id_user,
				'counter'		=> 1,
				'print'			=> $print,
				'created_at'	=> date('Y-m-d H:i:s'),
				'updated_at'	=> date('Y-m-d H:i:s')
			);

			$this->db->insert('log_print', $log);

		}

		
	}


	function income_daily()
	{

		$where = '';
		if ($id_agen = $this->session->userdata('id_agen')) {
			$where = ' WHERE id_agen =' . $id_agen;
		}
		$bulan = $this->db->query("SELECT DATE(created_at) as tanggal,SUM(total) as total_pendapatan FROM transaksi " . $where . " GROUP BY tanggal")->result_array();


		$tanggale = array();
		$month = date('m');
		$year = date('Y');

		for ($d = 1; $d <= 31; $d++) {
			$time = mktime(12, 0, 0, $month, $d, $year);
			if (date('m', $time) == $month) {
				$tanggale[] = date('Y-m-d', $time);
			}
		}


		$hari = array();
		$total = array();

		foreach ($tanggale as $tgl) {
			$hari[] = date('d', strtotime($tgl));
			$total_in = 0;
			foreach ($bulan as $row) {
				if ($tgl == date('Y-m-d', strtotime($row['tanggal']))) {
					$total_in = (int)$row['total_pendapatan'];
				}
			}
			$total[] = $total_in;
		}

		return array('hari' => $hari, 'total' => $total);
	}

	function total_loading(){

		$query = $this->db->query("SELECT COUNT(a.id) as total, a.kode_negara , b.nama_negara FROM transaksi a LEFT JOIN data_negara b ON a.kode_negara = b.id WHERE a.kode_negara != 0 AND a.status = 2 GROUP BY a.kode_negara");

		return $query->result_array();
	}
}

/* End of file GlobalModel.php */
/* Location: ./application/models/GlobalModel.php */
