<?php if (@$error) : ?>
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body alert-danger">
            <?= $error ?>
        </div>
    </div>
<?php endif; ?>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Manifest</h1>
    <div class="card shadow mb-3">
        <div class="card-body">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-3">
                        <label for="tgl_awal">Tanggal Awal</label>
                        <input type="date" name="tgl_awal" id="tgl_awal" class="form-control" value="<?= (isset($filter['tgl_awal']) ? $filter['tgl_awal'] : null) ?>">
                    </div>
                    <div class="col-md-3">
                        <label for="tgl_akhir">Tanggal Akhir</label>
                        <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control" value="<?= (isset($filter['tgl_akhir']) ? $filter['tgl_akhir'] : null) ?>">
                    </div>
                    <div class="col-md-3">
                        <label for="filter_status">Status Pengiriman</label>
                        <select name="status" id="filter_status" class="form-control">
                            <option value="">-- PILIH --</option>
                            <?php
                            $get_status = $this->db->get_where('data_status_pengiriman', array('flag' => 1))->result_array();
                            ?>
                            <?php foreach ($get_status as $i) : ?>
                                <option value="<?= $i['id'] ?>" <?= (@$filter['status'] == $i['id'] ? 'selected' : null) ?>><?= $i['nama_status'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                     <div class="col-md-3">
                        <label for="negara">Negara</label>
                        <select name="negara" id="negara" class="form-control">
                            <option value="">-- PILIH --</option>
                            <?php
                            $get_status = $this->db->get('data_negara')->result_array();
                            ?>
                            <?php foreach ($get_status as $i) : ?>
                                <option value="<?= $i['id'] ?>" <?= (@$filter['negara'] == $i['id'] ? 'selected' : null) ?>><?= $i['nama_negara'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-12 text-right mt-2">
                        <button type="submit" class="btn btn-info"><i class="fas fa-search"></i> Cari</button>
                        <a href="" class="btn btn-secondary">Reset</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary title-file">
                <?php
                if (@$filter['tgl_awal']) {
                    echo "Manifest " . ($filter['tgl_awal'] ? date_indo($filter['tgl_awal']) . ' sd. ' . date_indo($filter['tgl_akhir']) : 'false');
                } else {
                    echo "Manifest " . date_indo(date('Y-m-d'));
                }
                ?>
            </h6>
            <div id="tableActions"></div>
        </div>
        <div class="card-body">
            <!-- <div class="table-responsive"> -->
            <table class="table table-bordered" id="data-table" cellspacing="0" style="table-layout: fixed;">
                <thead>
                    <tr>
                        <th>NO_RESIT</th>
                        <th>NO_RESIT_LAMA</th>
                        <th>STATUS</th>
                        <th>NO_KONTENA</th>
                        <th>INDEX_BUNGKUSAN</th>
                        <th>JUMLAH_BUNGKUSAN</th>
                        <th>PROVINSI_TUJUAN</th>
                        <th>KOTA_TUJUAN</th>
                        <th>KECAMATAN_TUJUAN</th>
                        <th>DESA_TUJUAN</th>
                        <th>DUSUN TUJUAN</th>
                        <th>RT/RW</th>
                        <th>KODE_KURIR</th>
                        <th>UKURAN</th>
                        <th>TANGGAL_RESIT</th>
                        <th>PENGIRIM</th>
                        <th>PASPOR_NIK_KTP</th>
                        <th>ALAMAT_PENGIRIM</th>
                        <th>TELEPON_PENGIRIM</th>
                        <th>PENERIMA</th>
                        <th>ALAMAT_PENERIMA</th>
                        <th>TELEPON_PENERIMA</th>
                        <th>ISI_BARANG</th>
                        <th>TELEPON</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $i) : ?>
                        <tr>
                            <td><?= $i['no_resit'] ?></td>
                             <td><?= $i['no_resit_lama'] ?></td>
                            <td><?= $i['nama_status'] ?></td>
                            <td><?= $i['container'] ?></td>
                            <td></td>
                            <td></td>
                            <td><?= $i['nama_provinsi']; ?></td>
                            <td><?= $i['kota_tujuan'] ?></td>
                            <td><?= $i['nama_kecamatan']; ?></td>
                            <td><?= $i['desa_penerima']; ?></td>
                            <td><?= $i['dusun_penerima']; ?></td>
                            <td><?= $i['rw_penerima']; ?>/<?= $i['rt_penerima']; ?></td>
                            <td></td>
                            <td><?= $i['ukuran'] ?></td>
                            <td><?= date('d/m/Y',  strtotime($i['created_at'])); ?></td>
                            <td><?= $i['nama_pengirim'] ?></td>
                            <td><?= $i['no_pasport'] ?></td>
                            <td><?= $i['alamat_pengirim'] ?></td>
                            <td><?= $i['hp_pengirim'] ?></td>
                            <td><?= $i['nama_penerima'] ?></td>
                            <td><?= $i['alamat_penerima'] ?></td>
                            <td><?= $i['hp_penerima'] ?></td>
                            <td><?= $i['isi'] ?></td>
                            <td> - </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <!-- </div> -->
        </div>
    </div>
</div>
<script type="text/javascript">
    var codeListTable = $('#data-table').DataTable({
        "scrollX": true,
    });
    new $.fn.dataTable.Buttons(codeListTable, {
        buttons: [{
                extend: 'excel',
                text: '<i class="fa fa-file-excel"></i> Excel',
                title: $('.title-file').text().trim(),
                className: 'btn btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                text: '<i class="fa fa-file-pdf"></i> PDF',
                title: $('.title-file').text().trim(),
                className: 'btn btn-info btn-sm',
                pageSize: 'LEGAL',
                orientation: 'landscape',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print"></i> Print',
                title: $('.title-file').text().trim(),
                className: 'btn btn-info btn-sm',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
    });
    codeListTable.buttons().container().appendTo('#tableActions');
    $('.toast').toast({
        'animation': true,
        'autohide': false
    });
    $('.toast').toast('show');

    setTimeout(function() {
        $('.toast').toast('hide');
    }, 5000);
</script>