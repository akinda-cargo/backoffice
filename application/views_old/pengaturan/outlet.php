
<?php if($this->session->flashdata('success')){ ?>
  <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" >
    <div class="toast-body alert-success">
     <?= $this->session->flashdata('success'); ?>
   </div><!--toast-body-->
 </div><!--toast-->
<?php } ?>

<style type="text/css">
  .text-content{
    margin:0;
  }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Setting Outlet</h1>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-body">
     <form class="form-ajax-file" data-uri="<?= base_url('pengaturan/perbarui_outlet'); ?>" data-redirect="<?= base_url('pengaturan/outlet'); ?>">
      <div class="row">
        <div class="col-lg-6">

        </div>
        <div class="col-lg-6" style="text-align: right;">
         <button type="submit" class="btn btn-success btn-fw">Update</button>
       </div>
     </div>

     <div class="row">
      <div class="col-lg-3">
        <p>Outlet Information</p>
      </div>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-3">
            <div style="width: 100%;background: #FFF;height: 150px;background-image: url(<?= base_url('assets/uploads/' . $logo); ?>);background-size: contain;background-position: center;border: 1px solid #DDD; border-radius:5px;background-repeat: no-repeat;" class="div-image">

            </div>
            <input type="file" name="logo" id="logo" value="<?= @$logo; ?>" style="display: none;">
            <button class="btn btn-secondary btn-block mt-2" type="button" id="btn-logo">Change Logo</button>
          </div>
          <div class="col-lg-9">
            <?php
            if (@$id) {
              echo "<input type='hidden' name='id' value='$id'>";
            }
            ?>
            <div class="form-group">
              <label>Outlet Code</label>
              <input type="text" name="kode" class="form-control" value="<?= @$kode ?>" required>
            </div>
            <div class="form-group">
              <label>Name</label>
              <input type="text" name="nama" class="form-control" placeholder="Name" value="<?= @$nama; ?>">
            </div>

            <div class="form-group">
              <label>Country</label>
              <select class="form-control" name="id_negara" id="selectNegara">
                <option value="">-- Choose Country --</option>
                <?php
                foreach ($list_negara as $item) {
                  ?>
                  <option value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_negara) ? 'selected' : ''; ?>><?= $item['nama_negara'] ?></option>
                  <?php
                }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>Province</label>
              <select class="form-control related-select" name="id_provinsi" id="selectProvinsi" data-target="#selectNegara">
                <option value="">-- Choose Province --</option>
                <?php
                foreach ($list_prov as $item) {
                  ?>
                  <option class="rs<?= $item['id_negara'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_provinsi) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                  <?php
                }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>City</label>
              <select class="form-control related-select" name="id_kabupaten" data-target="#selectProvinsi">
                <option value="">-- Choose City --</option>
                <?php
                foreach ($list_kab as $item) {
                  ?>
                  <option class="rs<?= $item['id_prov'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_kabupaten) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                  <?php
                }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>Phone Number</label>
              <input type="number" name="tlp" class="form-control" value="<?= @$tlp ?>" required>
            </div>

            <div class="form-group">
              <label>Address</label>
              <textarea name="alamat" class="form-control" rows="5"><?= @$alamat ?></textarea>
            </div>

          </div>
        </div>

      </div>
    </div>
  </form>
</div>
</div>

</div>


<script type="text/javascript">
  $('#btn-logo').click(function(){
    $('#logo').trigger('click');
  });

  $("#logo").change(function(){
    ShowImage(this);
});

 function ShowImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          
            $('.div-image').css('background-image', 'url('+e.target.result+')').css('background-position', 'center').css('background-size', 'contain').css('background-repeat', 'no-repeat');
        }

        reader.readAsDataURL(input.files[0]);
    }
}
</script>