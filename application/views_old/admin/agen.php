<?php if ($this->session->flashdata('success')) { ?>
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body alert-success">
            <?= $this->session->flashdata('success'); ?>
        </div>
        <!--toast-body-->
    </div>
    <!--toast-->
<?php } ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Agen</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Data Agen</h6>

            <div><a href="<?= base_url('admin/form_agen'); ?>" class="btn btn-info btn-sm">Tambah Baru</a></div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama Agen</th>
                            <th width="15%">Logo</th>
                            <th>Alamat</th>
                            <th>No Telephone</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr>
                                <td><?= $row['kode']; ?></td>
                                <td><?= $row['nama']; ?></td>
                                <td><img src="<?= base_url('assets/uploads/' . $row['logo']); ?>" alt="logo" class="img-thumbnail w-100"></td>
                                <td><?= $row['alamat']; ?></td>
                                <td><?= $row['tlp'] ?></td>
                                <td>
                                    <?= ($row['flag'] == '1') ?
                                        '<span class="badge badge-success p-2">Aktif</span>' :
                                        '<span class="badge badge-danger p-2">Tidak Aktif</span>' ?>
                                </td>
                                <td><a href="<?= base_url('admin/form_agen/' . $row['id']); ?>" class="btn btn-primary btn-sm col-md-12">Ubah</a></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>