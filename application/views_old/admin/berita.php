

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Berita</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Data Berita</h6>

            <div><a href="<?=base_url('admin/form_berita');?>" class="btn btn-info btn-sm">Tambah Baru</a></div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Judul</th>
                            <th>Konten</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($berita as $row) {

                            if($row['flag'] == 1){
                                $status = '<span class="badge badge-success">Aktif</span>';
                            }else{
                                $status = '<span class="badge badge-danger">Inaktif</span>';
                            }
                            ?>

                            <tr>
                                <td><?= $row['foto']; ?></td>
                                <td><?= $row['judul']; ?></td>
                                <td><?= substr(strip_tags($row['isi']), 0 , 150); ?></td>
                                <td><?= $row['tgl_input']; ?></td>
                                <td><?= $status; ?></td>
                                <td><a href="<?=base_url('admin/form_berita/'.$row['id']);?>" class="btn btn-primary btn-sm col-md-12">Edit</a></td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
