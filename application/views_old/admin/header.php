<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= base_url('favicon.ico'); ?>" type="image/x-icon">
    <?php if (isset($title)) { ?>
        <title><?= $title; ?></title>
    <?php } else { ?>
        <title>AKINDA - Admin Panel</title>
    <?php } ?>

    <!-- Custom fonts for this template -->
    <link href="<?= base_url('public/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= base_url('public/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/select2/css/select2.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/select2/css/select2-bootstrap4.min.css'); ?>" rel="stylesheet">

    <script src="<?= base_url('public/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('public/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

    <?php if (isset($datatables)) { ?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.bootstrap4.min.css">

        <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.colVis.min.js"></script>
    <?php } else { ?>
        <link href="<?= base_url('public/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">
    <?php } ?>

    <style type="text/css">
        .table {
            font-size: 13px;
        }

        .toast {
            float: right;
            clear: right;
            margin: 1rem;
            position: fixed;
            right: 0;
            z-index: 2;
        }
    </style>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">

                <div class="sidebar-brand-text mx-3">AKINDA CARGO</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url(); ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Beranda</span></a>
                </li>

                <?php if ($this->session->userdata('level') <= 2) { ?>
                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#reportscollapse" aria-expanded="true" aria-controls="reportscollapse">
                            <i class="fas fa-fw fa-file-alt"></i>
                            <span>Laporan</span>
                        </a>
                        <div id="reportscollapse" class="collapse <?= (@$menu == 'laporan') ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <a class="collapse-item <?= (@$sub_menu == 'manifest') ? 'active' : ''; ?>" href="<?= base_url('laporan/manifest'); ?>">Manifest</a>
                                <a class="collapse-item <?= (@$sub_menu == 'pengiriman') ? 'active' : ''; ?>" href="<?= base_url('laporan/pengiriman'); ?>">Pengiriman</a>
                                <a class="collapse-item <?= (@$sub_menu == 'pendapatan') ? 'active' : ''; ?>" href="<?= base_url('laporan/pendapatan'); ?>">Pendapatan</a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if ($this->session->userdata('level') == 1) { ?>

                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i class="fas fa-fw fa-database"></i>
                            <span>Data Master</span>
                        </a>
                        <div id="collapseTwo" class="collapse <?= (@$menu == 'master') ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <!-- <a class="collapse-item <?= (@$sub_menu == 'slider') ? 'active' : ''; ?>" href="<?= base_url('admin/slider'); ?>">Slider</a> -->
                                <a class="collapse-item <?= (@$sub_menu == 'negara') ? 'active' : ''; ?>" href="<?= base_url('admin/negara'); ?>">Negara</a>
                                <a class="collapse-item <?= (@$sub_menu == 'provinsi') ? 'active' : ''; ?>" href="<?= base_url('admin/provinsi'); ?>">Provinsi</a>
                                <a class="collapse-item <?= (@$sub_menu == 'kota') ? 'active' : ''; ?>" href="<?= base_url('admin/kota'); ?>">Kota</a>
                                <a class="collapse-item <?= (@$sub_menu == 'kecamatan') ? 'active' : ''; ?>" href="<?= base_url('admin/kecamatan'); ?>">Kecamatan</a>
                                <a class="collapse-item <?= (@$sub_menu == 'outlet') ? 'active' : ''; ?>" href="<?= base_url('admin/outlet'); ?>">Outlet</a>
                                <a class="collapse-item <?= (@$sub_menu == 'agen') ? 'active' : ''; ?>" href="<?= base_url('admin/agen'); ?>">Agen</a>
                                <a class="collapse-item <?= (@$sub_menu == 'rates') ? 'active' : ''; ?>" href="<?= base_url('admin/rates'); ?>">Tarif</a>
                                <a class="collapse-item <?= (@$sub_menu == 'armada') ? 'active' : ''; ?>" href="<?= base_url('admin/armada'); ?>">Armada</a>
                                <a class="collapse-item <?= (@$sub_menu == 'delivery_status') ? 'active' : ''; ?>" href="<?= base_url('admin/delivery_status'); ?>">Pengiriman Status</a>

                                <a class="collapse-item <?= (@$sub_menu == 'member') ? 'active' : ''; ?>" href="<?= base_url('admin/member'); ?>">Pelanggan</a>
                                <a class="collapse-item <?= (@$sub_menu == 'employes') ? 'active' : ''; ?>" href="<?= base_url('admin/employes'); ?>">User / Pegawai</a>
                            </div>
                        </div>
                    </li>
                <?php } ?>

                <?php if ($this->session->userdata('level') == 2) { ?>
                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i class="fas fa-fw fa-database"></i>
                            <span>Data Master</span>
                        </a>
                        <div id="collapseTwo" class="collapse <?= (@$menu == 'master') ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <a class="collapse-item <?= (@$sub_menu == 'member') ? 'active' : ''; ?>" href="<?= base_url('admin/member'); ?>">Pelanggan</a>
                                <a class="collapse-item <?= (@$sub_menu == 'employes') ? 'active' : ''; ?>" href="<?= base_url('admin/employes'); ?>">User / Pegawai</a>
                            </div>
                        </div>
                    </li>
                <?php } ?>

                <hr class="sidebar-divider">


                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('transaksi'); ?>">
                        <i class="fas fa-fw fa-store"></i>
                        <span>Transaksi Baru</span></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('transaksi/list_data'); ?>">
                            <i class="fas fa-fw fa-clipboard-list"></i>
                            <span>Data Transaksi</span></a>
                        </li>

                        <!-- Divider -->
                        <hr class="sidebar-divider d-none d-md-block">

                        <?php if ($this->session->userdata('level') == 3) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('admin/member'); ?>">
                                    <i class="fas fa-fw fa-clipboard-list"></i>
                                    <span>Pelanggan</span></a>
                                </li>
                            <?php } ?>

                            <?php if ($this->session->userdata('level') == 1) { ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url('pengaturan'); ?>">
                                        <i class="fas fa-fw fa-cog"></i>
                                        <span>Pengaturan</span></a>
                                    </li>
                                     <li class="nav-item <?= (@$sub_menu == 'whatsapp') ? 'active' : ''; ?>">
                                    <a class="nav-link" href="<?= base_url('whatsapp'); ?>">
                                        <i class="fas fa-fw fa-cog"></i>
                                        <span>Pengaturan Whatsapp</span></a>
                                    </li>
                                <?php } ?>


                                <?php if ($this->session->userdata('level') == 2) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= base_url('pengaturan/outlet'); ?>">
                                            <i class="fas fa-fw fa-cog"></i>
                                            <span>Pengaturan</span></a>
                                        </li>
                                    <?php } ?>


                                    <!-- Sidebar Toggler (Sidebar) -->
                                    <div class="text-center d-none d-md-inline">
                                        <button class="rounded-circle border-0" id="sidebarToggle"></button>
                                    </div>

                                </ul>
                                <!-- End of Sidebar -->

                                <!-- Content Wrapper -->
                                <div id="content-wrapper" class="d-flex flex-column">

                                    <!-- Main Content -->
                                    <div id="content">

                                        <!-- Topbar -->
                                        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                                            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                                                <i class="fa fa-bars"></i>
                                            </button>


                                            <!-- Topbar Navbar -->
                                            <ul class="navbar-nav ml-auto">
                                                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                                                <li class="nav-item dropdown no-arrow d-sm-none">
                                                    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-search fa-fw"></i>
                                                    </a>
                                                    <!-- Dropdown - Messages -->
                                                    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                                        <form class="form-inline mr-auto w-100 navbar-search">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-primary" type="button">
                                                                        <i class="fas fa-search fa-sm"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </li>

                                                <!-- Nav Item - Alerts -->
                                                <li class="nav-item dropdown no-arrow mx-1">
                                                    <?php
                                                    $total = 0;
                                                    $this->db->where('DATE(created_at)', date('Y-m-d'));
                                                    if ($id_agen = $this->session->userdata('id_agen')) {
                                                        $this->db->where('id_agen', $id_agen);
                                                    }
                                                    $get = $this->db->get('transaksi');

                                                    if($total = $get->num_rows() > 0){

                                                        $tr = $get->row_array();

                                                        if ($total > 9) {
                                                            $total = '9+';
                                                        } 

                                                    }else{
                                                        $total = 0;

                                                    }




                                                    ?>
                                                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-bell fa-fw"></i>
                                                        <!-- Counter - Alerts -->
                                                        <span class="badge badge-danger badge-counter"><?= $total; ?></span>
                                                    </a>
                                                    <!-- Dropdown - Alerts -->
                                                    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                                                        <h6 class="dropdown-header">
                                                            Transaksi Baru
                                                        </h6>
                                                        <?php if($get->num_rows() > 0){ ?>
                                                        <a class="dropdown-item d-flex align-items-center" href="<?= base_url('transaksi/list_data/'.$tr['id']); ?>">

                                                            <div>
                                                                <div class="small text-gray-500"><?= dateIndo($tr['created_at']); ?></div>
                                                                No Transaksi : <span><?= $tr['no_resit'];  ?></span> <br>
                                                                Total : <b><?= decimals($tr['total']); ?></b>
                                                            </div>
                                                        </a>
                                                        <a class="dropdown-item text-center small text-gray-500" href="<?= base_url('transaksi/list_data'); ?>">Lihat Semua</a>
                                                    <?php }else{ ?>
                                                        <a class="dropdown-item d-flex align-items-center" href="#">

                                                            <div>
                                                                <div class="small text-gray-500">Tidak ada transaksi</b></div>
                                                            </div>
                                                        </a>
                                                    <?php } ?>
                                                    </div>
                                                </li>


                                                <div class="topbar-divider d-none d-sm-block"></div>

                                                <!-- Nav Item - User Information -->
                                                <li class="nav-item dropdown no-arrow">
                                                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $this->session->userdata('nama'); ?></span>
                                                        <img class="img-profile rounded-circle" src="<?= base_url('public/img/undraw_profile.svg'); ?>">
                                                    </a>
                                                    <!-- Dropdown - User Information -->
                                                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                                        <a class="dropdown-item" href="#">
                                                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                                            Profil
                                                        </a>
                                                        <div class="dropdown-divider"></div>
                                <!-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
    Logout
</a> -->
<a href="<?= base_url('masuk/keluar'); ?>" class="dropdown-item">
    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
    Keluar
</a>
</div>
</li>

</ul>

</nav>
                <!-- End of Topbar -->