<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Status Pengiriman</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Status Pengiriman</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax" data-uri="<?= base_url('admin/save_delivery_status'); ?>" data-redirect="<?= base_url('admin/delivery_status'); ?>">
                <?php
                if (@$id) {
                    echo "<input type='hidden' name='id' value='$id'>";
                }
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kode Status</label>
                            <input type="text" name="kode_status" class="form-control" value="<?= @$kode_status ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama_status" class="form-control" value="<?= @$nama_status ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" rows="5"><?= @$keterangan ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Peesan WhatsApp</label>
                            <textarea name="pesan_wa" class="form-control" rows="5"><?= @$pesan_wa ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="flag" class="form-control" required>
                                <option value="">-- Choose --</option>
                                <option <?= (@$flag == '1') ? 'selected' : '' ?> value="1">Aktif</option>
                                <option <?= (@$flag == '0') ? 'selected' : '' ?> value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Status Pengiriman</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>