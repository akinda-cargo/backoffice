<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Pelanggan</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Pelanggan</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax" data-uri="<?= base_url('admin/save_member'); ?>" data-redirect="<?= base_url('admin/member'); ?>">
                <?php
                if (@$id) {
                    echo "<input type='hidden' name='id' value='$id'>";
                }
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nomor Kartu</label>
                            <input type="number" name="nik" class="form-control" value="<?= @$nik ?>" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nomor Pasport</label>
                            <input type="number" name="no_pasport" class="form-control" value="<?= @$no_pasport ?>" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nama Lenggkap</label>
                            <input type="text" name="nama_lengkap" class="form-control" value="<?= @$nama_lengkap ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Telephone</label>
                            <input type="number" name="no_tlp" class="form-control" value="<?= @$no_tlp ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No WhatsApp</label>
                            <input type="number" name="no_wa" class="form-control" value="<?= @$no_wa ?>" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Negara</label>
                            <select class="select2 form-control" name="id_negara" id="selectNegara">
                                <option value="">-- Pilih --</option>
                                <?php
                                foreach ($list_negara as $item) {
                                ?>
                                    <option value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_negara) ? 'selected' : ''; ?>><?= $item['nama_negara'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Provinsi</label>
                            <select class="select2 form-control related-select" name="id_provinsi" id="selectProvinsi" data-target="#selectNegara">
                                <option value="">-- Pilih --</option>
                                <?php
                                foreach ($list_prov as $item) {
                                ?>
                                    <option class="rs<?= $item['id_negara'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_provinsi) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kota</label>
                            <select class="select2 form-control related-select" name="id_kabupaten" id="selectKab" data-target="#selectProvinsi">
                                <option value="">-- Pilih --</option>
                                <?php
                                foreach ($list_kab as $item) {
                                ?>
                                    <option class="rs<?= $item['id_prov'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_kabupaten) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <select class="select2 form-control related-select" name="id_kecamatan" data-target="#selectKab">
                                <option value="">-- Pilih --</option>
                                <?php
                                foreach ($list_kec as $item) {
                                ?>
                                    <option class="rs<?= $item['id_kabupaten'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_kecamatan) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Desa</label>
                            <input type="text" name="desa" class="form-control" value="<?= @$desa ?>" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Dusun</label>
                            <input type="text" name="dusun" class="form-control" value="<?= @$dusun ?>" >
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>RW</label>
                            <input type="text" name="rw" class="form-control" value="<?= @$rw ?>" >
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>RT</label>
                            <input type="text" name="rt" class="form-control" value="<?= @$rt ?>" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kode Pos</label>
                            <input type="text" name="kode_pos" class="form-control" value="<?= @$kode_pos ?>" >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Alamat Lengkap</label>
                            <textarea name="alamat_lengkap" class="form-control" rows="5"><?= @$alamat_lengkap ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="flag" class="form-control" required>
                                <option value="">-- PILIH --</option>
                                <option <?= (@$flag == '1') ? 'selected' : '' ?> value="1">Aktif</option>
                                <option <?= (@$flag == '0') ? 'selected' : '' ?> value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Pelanggan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>