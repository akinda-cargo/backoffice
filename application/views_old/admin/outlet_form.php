<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Outlet</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Outlet</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax" data-uri="<?= base_url('admin/save_outlet'); ?>" data-redirect="<?= base_url('admin/outlet'); ?>">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Kode Outlet</label>
                            <input type="text" name="kode" class="form-control" value="<?= @$kode ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <?php
                        if (@$id) {
                            echo "<input type='hidden' name='id' value='$id'>";
                        }
                        ?>
                        <div class="form-group">
                            <label>Negara</label>
                            <select class="select2 form-control" name="id_negara" id="selectNegara">
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_negara as $item) {
                                ?>
                                    <option value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_negara) ? 'selected' : ''; ?>><?= $item['nama_negara'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Provinsi</label>
                            <select class="select2 form-control related-select" name="id_provinsi" id="selectProvinsi" data-target="#selectNegara">
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_prov as $item) {
                                ?>
                                    <option class="rs<?= $item['id_negara'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_provinsi) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Kota</label>
                            <select class="select2 form-control related-select" name="id_kabupaten" data-target="#selectProvinsi">
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_kab as $item) {
                                ?>
                                    <option class="rs<?= $item['id_prov'] ?>" value="<?= $item['id'] ?>" <?= ($item['id'] == @$id_kabupaten) ? 'selected' : ''; ?>><?= $item['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" value="<?= @$nama ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control" rows="5"><?= @$alamat ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Kode Pos</label>
                            <input type="text" name="kode_pos" class="form-control" value="<?= @$kode_pos ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>No Telephone</label>
                            <input type="number" name="tlp" class="form-control" value="<?= @$tlp ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="flag" class="form-control" required>
                                <option value="">-- PILIH --</option>
                                <option <?= (@$flag == '1') ? 'selected' : '' ?> value="1">Aktif</option>
                                <option <?= (@$flag == '0') ? 'selected' : '' ?> value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Outlet</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>