
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Berita</h1>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Form Berita</h6>
                        </div>
                        <div class="card-body">
                            <form class="form-submit">
                          <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-success btn-submit">Simpan Berita</button>
                            </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                      <label>Judul</label>
                                      <input type="text" name="judul" class="form-control" required="">
                                  </div>
                                  <div class="form-group">
                                      <label>Foto</label>
                                      <input type="file" name="userfile" class="form-control" />
                                  </div>
                                 
                              </div>

                              <div class="col-md-12">
                                  <label>Isi</label>
                                  <div id="summernote"></div>
                              </div>
                          </div>
                          </form>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->
<script type="text/javascript">
  jQuery(document).ready(function() {
  $('#summernote').summernote({
        placeholder: 'Tulis Isi Konten disini',
        tabsize: 2,
        height: 100
      });
});

  $(".form-submit").submit(function(event){
  event.preventDefault();

  var desc = $('#summernote').summernote('code');

  var formData = new FormData(this);

  formData.append('isi',desc);

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/simpan_berita'; ?>",
    data:formData,
    processData:false,
    contentType:false,
    cache:false,
    async:false,
    beforeSend: function() {
      // $('.btn-submit').prop('disabled', true);
      // $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {

      if(data == 1)
      {
        window.location = '<?php echo base_url().'admin/berita'; ?>';
      }
    }
  });

});
</script>