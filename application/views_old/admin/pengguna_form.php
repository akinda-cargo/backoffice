<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Pengguna</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Pengguna</h6>
        </div>
        <div class="card-body">
            <form class="form-submit">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Pengguna</button>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" required="" value="<?php if (isset($id)) {
                                                                                                        echo $row['nama'];
                                                                                                    } ?>">
                        </div>
                        <div class="form-group">
                            <label>Kelamin</label>
                            <select class="form-control" name="kelamin" required="">
                                <option value="">Pilih Kelamin</option>
                                <option value="L" <?php if (isset($id)) {
                                                        if ($row['kelamin'] == 'L') {
                                                            echo "selected";
                                                        }
                                                    } ?>>Laki-laki
                                </option>
                                <option value="P" <?php if (isset($id)) {
                                                        if ($row['kelamin'] == 'P') {
                                                            echo "selected";
                                                        }
                                                    } ?>>Perempuan
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" required="" value="<?php if (isset($id)) {
                                                                                                                echo $row['tempat_lahir'];
                                                                                                            } ?>">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" name="tgl_lahir" class="form-control" required="" value="<?php if (isset($id)) {
                                                                                                            echo $row['tgl_lahir'];
                                                                                                        } ?>">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control" placeholder="Alamat"><?php if (isset($id)) {
                                                                                                    echo $row['alamat'];
                                                                                                } ?></textarea>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cabang</label>
                            <select class="select2 form-control" name="id_cabang" required="">
                                <option>Pilih Cabang</option>
                                <?php foreach ($cabang as $c) { ?>
                                    <option value="<?= $c['id']; ?>" <?= (isset($id) ? ($row['id_cabang'] == $c['id'] ? "selected" : '') : '') ?>>
                                        <?= $c['nama']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Level</label>
                            <select class="form-control" name="id_level" required="">
                                <option>Pilih Level</option>
                                <?php foreach ($level as $l) { ?>
                                    <option value="<?= $l['id']; ?>" <?php if (isset($id)) {
                                                                            if ($row['level'] == $l['id']) {
                                                                                echo "selected";
                                                                            }
                                                                        } ?>>
                                        <?= $l['nama']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" required="" value="<?php if (isset($id)) {
                                                                                                            echo $row['login'];
                                                                                                        } ?>">
                        </div>
                        <?php if (isset($id)) { ?>
                            <input type="hidden" name="id_pengguna" value="<?= $id; ?>">
                        <?php } else { ?>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" required="">
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
    $(".form-submit").submit(function(event) {
        event.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'admin/simpan_pengguna'; ?>",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            beforeSend: function() {
                // $('.btn-submit').prop('disabled', true);
                // $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
            },
            success: function(data) {

                if (data == 1) {
                    window.location = '<?php echo base_url() . 'admin/pengguna'; ?>';
                }
            }
        });

    });
</script>