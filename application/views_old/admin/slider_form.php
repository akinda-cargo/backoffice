<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Slider</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Slider</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax-file" data-uri="<?= base_url('admin/save_slider'); ?>" data-redirect="<?= base_url('admin/slider'); ?>">
                <div class="row">
                    <div class="col-md-6">
                        <?php
                        if (@$id) {
                            echo "<input type='hidden' name='id' value='$id'>";
                        }
                        ?>
                        <div class="form-group">
                            <label>Gambar</label>
                            <input type="file" name="gambar" class="form-control" <?= (@$gambar) ? 'required' : '' ?>>
                            <?php if (@$gambar) : ?>
                                <img class="img-thumbnail w-100 mt-2" src="<?= base_url('assets/banner/' . @$gambar); ?>" alt="<?= @$gambar ?>">
                                <input type="hidden" name="old_gambar" value="<?= @$gambar ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="flag" class="form-control" required>
                                <option value="">-- PILIHs --</option>
                                <option <?= (@$flag == '1') ? 'selected' : '' ?> value="1">Aktif</option>
                                <option <?= (@$flag == '0') ? 'selected' : '' ?> value="0">Tidak AKtif</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Slider</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>