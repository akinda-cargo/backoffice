<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Negara</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Negara</h6>
        </div>
        <div class="card-body">
            <form class="form-ajax" data-uri="<?= base_url('admin/save_negara'); ?>" data-redirect="<?= base_url('admin/negara'); ?>">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (@$id) {
                            echo "<input type='hidden' name='id' value='$id'>";
                        }
                        ?>
                        <div class="form-group">
                            <label>Kode Negara</label>
                            <input type="text" name="kode_negara" class="form-control" value="<?= @$kode_negara ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Nama Negara</label>
                            <input type="text" name="nama_negara" class="form-control" value="<?= @$nama_negara ?>" required>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success btn-submit">Simpan Negara</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>