<!DOCTYPE html>
<html>

<head>
	<title>Invoice</title>
</head>

<style type="text/css">
	@page {
		margin-left: 2mm;
		margin-right: 2mm;
		margin-top: 2mm;
	}

	body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		background-color: #FAFAFA;
		font: 12px "Tahoma";
		margin-right: 2mm;
		margin-left: 2mm;
		margin-top: 2mm;
	}

	tr td {
		padding-top: 5px;
		padding-bottom: 5px;
		padding-left: 5px;
		padding-right: 5px;
	}

	tr th {
		padding-top: 5px;
		padding-bottom: 5px;
	}

	.text-right {
		text-align: right;
	}

	@media screen {
		div.footer {
			display: none;
		}
	}

	@media print {
		div.footer {
			position: fixed;
			right: 0;
			bottom: 0;
		}
	}

	@page {
		size: A4;
		margin-right: 2mm;
		margin-left: 2mm;
		margin-top: 2mm;
	}


	.p9 {
		font-size: 9pt;
	}

	.py8 tr td {
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>

<body>

	<?php
	if ($this->session->userdata('id_agen')) {
		$id_agen = $this->session->userdata('id_agen');
		$get = $this->db->get_where('data_outlet', array('id' => $id_agen))->row_array();
		$nama = $get['nama'];
		$alamat = $get['alamat'];
		$tlp = $get['tlp'];
	} else {
		$nama = $setting->nama;
		$alamat = $setting->alamat;
		$tlp = $setting->notelp;
	}
	?>
	<div class="book">
		<div class="page">

			<table style="width: 100%;margin-bottom: 40px;">
				<tr>
					<td style="width:70%;">
						<h2><?= $nama; ?></h2>
						<p style="margin: 0;"><?= $alamat; ?></p>
						<p style="margin: 0;">Tel: <?= $tlp; ?></p>
					</td>
					<td style="width: 30%;">
						KONTENA : <strong><?= $tr['container']; ?></strong> <br>
						PETUGAS : <strong><?php if ($tr['kode'] == '') {
												echo "ADM";
											} else {
												echo $tr['kode'];
											} ?></strong><br>
						NOMOR KOTAK RESIT <br>
						NO: <strong style="color:red;"><?= $tr['no_resit']; ?></strong> <br>
						NOMOR RESIT LAMA <br>
						NO: <strong style="color:red;"><?= $tr['no_resit_lama']; ?></strong>
					</td>
				</tr>
			</table>

			<table style="width: 100%;">
				<tr>
					<td style="width: 70%;border:1px solid #000;border-right: 0;vertical-align: top;">
						<table style="width:100%;">
							<tr>
								<td style="text-align: center;"><u><strong>RESIT RASMI</strong></u></td>
							</tr>
							<tr>
								<td style="border: 1px solid #000;">Data Pengirim</td>
							</tr>
							<tr>
								<td>
									<p>Nama : <strong><?= $tr['nama_pengirim']; ?></strong></p>
									<p>Alamat : <strong><?= $tr['alamat_pengirim']; ?></strong></p>
									<p>HP: <strong><?= $tr['hp_pengirim']; ?></strong></p>
								</td>
							</tr>
							<tr>
								<td style="border: 1px solid #000;">Data Penerima</td>
							</tr>
							<tr>
								<td>
									<p>Nama : <strong><?= $tr['nama_penerima']; ?></strong></p>
									<p>Alamat : <strong><?= $tr['alamat_penerima']; ?></strong></p>
									<p>Provinsi: <strong><?= $tr['provinsi']; ?></strong></p>
									<p>Kota : <strong><?= $tr['kota']; ?></strong></p>
									<p>Kode Pos : <strong><?= $tr['kode_pos_penerima']; ?></strong></p>
									<p>HP: <strong><?= $tr['hp_penerima']; ?></strong></p>
									<p>HP WA: <strong><?= $tr['wa_penerima']; ?></strong></p>
								</td>
							</tr>
							<tr>
								<td>Saya sebagai pengirim telah membaca dan bersetuju dengan terima dan syarat</td>
							</tr>
							<tr>
								<td>
									<table style="width: 100%;">
										<tr>
											<td style="width: 50%;text-align: left;padding-top: 70px;">Pengirim</td>
											<td style="width: 50%;text-align: right;padding-top: 70px;">Petugas</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style="border-top:1px solid #000;">
									<u><strong>TERIMA DAN SYARAT</strong></u><br><br>
									Dilarang memasukan barang yang di larang termasuk barang - barang berharga seperti telephone, uang , emas , berlian, barang mewah dan serta perhiasan dan barang - barang berbahasa seperti tong gas , senjata api cat dan barang mudah meledak. <b>Syarikat</b> tidak bertanggung jawab terhadap barang yang di samarkan di dalam resit <b>Syarikat</b> juga tidak bertanggung jawab atas sekiranya barang rusak akibat barang rusak terkena banjir
								</td>
							</tr>
						</table>
					</td>
					<td style="width: 30%;vertical-align: top;border:1px solid #000;">
						<p>Jumlah Koli: <strong> 1 BOX</strong></p>



						<p>Size Koli 1 : <strong><?= $tr['ukuran']; ?></strong></p>

						<p>ISI : <strong style="font-weight: 600;"><?= $tr['isi']; ?></strong></p>

						<p>KOLI 1 BND : <strong><?= decimals($tr['total']); ?></strong></p>
						<p>LEBIHAN SIZE BDN: </p>



						<hr>

						<p>JUMLAH BND : <strong><?= decimals($tr['total']); ?></strong></p>
						<p>TERAKHIR DI AMBIL <br> <strong><?= date('d-m-Y', strtotime($tr['created_at'])); ?></strong></p>

						<img src="<?= 'barcode/' . $tr['no_resit'] . '.png'; ?>">
					</td>
				</tr>
			</table>


		</div>
	</div>
</body>

</html>