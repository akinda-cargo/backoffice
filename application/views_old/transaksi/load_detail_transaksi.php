<div class="col-md-6">
    <h5>Pengirim</h5>
    <hr>
    <p class="text-content">Paspor : <?= $tr['no_pasport']; ?></p>
    <p class="text-content">Nama Lengkap : <?= $tr['nama_pengirim']; ?></p>
    <p class="text-content">No Telephone : <?= $tr['hp_pengirim']; ?></p>
    <p class="text-content">No WA : <?= $tr['wa_pengirim']; ?></p>
    <p class="text-content">Alamat : <?= $tr['alamat_pengirim']; ?></p>
</div>

<div class="col-md-6">
    <h5>Penerima</h5>
    <hr>
    <p class="text-content">Nama Lengkap : <?= $tr['nama_penerima']; ?></p>
    <p class="text-content">No Telephon : <?= $tr['hp_penerima']; ?></p>
    <p class="text-content">No Whatapps : <?= $tr['no_pasport']; ?></p>
    <p class="text-content">Negara : <?= $tr['nama_negara']; ?></p>
    <p class="text-content">Provinsi : <?= $tr['provinsi']; ?></p>
    <p class="text-content">Kota : <?= $tr['kota']; ?></p>
    <p class="text-content">Kecamatan : <?= $tr['kecamatan']; ?></p>
    <p class="text-content">Desa : <?= $tr['desa_penerima']; ?></p>
    <p class="text-content">Dusun : <?= $tr['dusun_penerima']; ?></p>
    <p class="text-content">RW / RT : <?= $tr['rw_penerima'] . ' / ' . $tr['rt_penerima']; ?></p>
    <p class="text-content">Kode Code : <?= $tr['kode_pos_penerima']; ?></p>
    <p class="text-content">Alamat : <?= $tr['alamat_penerima']; ?></p>
</div>

<div class="col-md-12 mt-2">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Barang</th>
                <th>Isi</th>
                <th>Tarif</th>
            </tr>
        </thead>
        <tbody>


            <tr>
                <td>Box <?= $tr['ukuran']; ?></td>
                <td><?= $tr['isi']; ?></td>
                <td class="text-right"><?= decimals($tr['total']); ?></td>

            </tr>



        </tbody>
        <tfoot>
            <tr>
                <td colspan="2"><strong>Total</strong></td>
                <td class="text-right"><strong class="total" key="<?= $tr['total']; ?>"><?= decimals($tr['total']); ?></strong></td>

            </tr>
            <tr>
                <td colspan="2"><strong>Kembalian</strong></td>
                <td class="text-right txtkembalian" style="font-weight: bold;"></td>

            </tr>
        </tfoot>
    </table>
</div>