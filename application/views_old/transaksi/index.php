<!DOCTYPE html>
<html>

<head>
	<title>Transaksi Akinda</title>
	<link href="<?= base_url('public/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link href="<?= base_url('assets/select2/css/select2.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/select2/css/select2-bootstrap4.min.css'); ?>" rel="stylesheet">
	<style type="text/css">
		.tt-menu {
			width: 422px;
			margin: 12px 0;
			padding: 8px 0;
			background-color: #fff;
			border: 1px solid #ccc;
			border: 1px solid rgba(0, 0, 0, 0.2);
			-webkit-border-radius: 8px;
			-moz-border-radius: 8px;
			border-radius: 8px;
			-webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
			-moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
			box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
		}

		.tt-menu,
		.gist {
			text-align: left;
		}

		.tt-suggestion {
			padding: 3px 20px;
			line-height: 24px;
		}

		.tt-suggestion:hover {
			cursor: pointer;
			color: #fff;
			background-color: #0097cf;
		}

		.tt-suggestion.tt-cursor {
			color: #fff;
			background-color: #0097cf;

		}

		.twitter-typeahead {
			width: 100%;
		}

		.col-left {
			overflow-y: scroll;
			min-height: 720px;
			height: 720px;
		}

		.col-right {
			height: 100%;
			position: absolute;
			top: 60px;
			bottom: 0;
			right: 0;
			width: 75%;
		}

		.bg-gradient-primary {
			background-color: #4e73df;
			background-image: linear-gradient(180deg, #4e73df 10%, #224abe 100%);
			background-size: cover;
		}
	</style>
	<script src="<?= base_url('public/vendor/jquery/jquery.min.js'); ?>"></script>
	<script src="<?= base_url('public/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>


	<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
	<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-gradient-primary" style="color:#FFF;">
		<a class="navbar-brand" href="<?= base_url(); ?>" style="color:#FFF;"><i class="fas fa-arrow-left"></i> KEMBALI</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav" style="margin: 0 auto;">
				<li class="nav-item active">
					<a class="nav-link" href="#" style="font-weight: bold;color: #FFF;">AKINDA CARGO <span class="sr-only">(current)</span></a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row mt-3">

			<div class="col-md-4 col-left">
				<form class="form-submit" style="width: 100%;">
					<h4>Pengirim</h4>
					<button type="button" class="btn btn-primary btn-block mb-2 cari-pelanggan" key="pengirim">Cari Pelanggan</button>
					<div class="col-md-12">
						<div class="row">
							<div class="custom-control custom-radio col-md-6">
								<input type="radio" id="identity_type1" name="identity_type" checked class="custom-control-input" value="KTP">
								<label class="custom-control-label" for="identity_type1">NIK</label>
							</div>
							<div class="custom-control custom-radio col-md-6">
								<input type="radio" id="identity_type2" name="identity_type" class="custom-control-input" value="Paspor">
								<label class="custom-control-label" for="identity_type2">Paspor</label>
							</div>
						</div>
					</div>
					<div class="form-group mt-2">
						<input type="text" name="pasport_pengirim" class="form-control sender_pasport" placeholder="NIK">
						<div class="invalid-feedback">
							NIK / Paspor harus diisi
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="nama_pengirim" class="form-control sender_nama" placeholder="Nama Lengkap">
						<div class="invalid-feedback">
							Nama harus diisi.
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="tlp_pengirim" class="form-control sender_tlp" placeholder="No Telephone">
						<div class="invalid-feedback">
							No Telephone harus diisi.
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="wa_pengirim" class="form-control sender_wa" placeholder="Nomor WhatsApp">
						<div class="invalid-feedback">
							Nomor WhatsApp harus diisi.
						</div>
					</div>
					<div class="form-group">
						<select class="select2 form-control send_negara" name="id_negara_pengirim" id="id_negara_pengirim">
							<option value="">Pilih Negara</option>
							<?php
							$country = $this->db->query('SELECT * FROM data_negara')->result_array();
							foreach ($country as $c) {
							?>
								<option value="<?= $c['id']; ?>"><?= $c['nama_negara']; ?></option>
							<?php
							} ?>
						</select>
						<div class="invalid-feedback">
							Negaara harus diisi.
						</div>
					</div>
					<div class="form-group">
						<textarea name="alamat_pengirim" class="form-control sender_alamat" placeholder="Alamat Lengkap"></textarea>
						<div class="invalid-feedback">
							Alamat Lengkap harus diisi.
						</div>
					</div>

					<hr>

					<h4>Penerima</h4>

					<button type="button" class="btn btn-primary btn-block mb-2 cari-pelanggan" key="penerima">Cari Pelanggan</button>
					<div class="col-md-12">
						<div class="row">
							<div class="custom-control custom-radio col-md-6">
								<input type="radio" id="identity_type_penerima1" name="identity_type_penerima" checked class="custom-control-input" value="KTP">
								<label class="custom-control-label" for="identity_type_penerima1">KTP</label>
							</div>
							<div class="custom-control custom-radio col-md-6">
								<input type="radio" id="identity_type_penerima2" name="identity_type_penerima" class="custom-control-input" value="Paspor">
								<label class="custom-control-label" for="identity_type_penerima2">Paspor</label>
							</div>
						</div>
					</div>
					<div class="form-group mt-2">
						<input type="text" name="pasport_penerima" class="form-control to_pasport" placeholder="KTP">
						<div class="invalid-feedback">
							KTP / Pasport is require.
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="nama_penerima" class="form-control to_nama" placeholder="Recipient's Name">
						<div class="invalid-feedback">
							Nama harus diisi.
						</div>
					</div>

					<div class="form-group">
						<input type="text" name="tlp_penerima" class="form-control to_tlp" placeholder="Phone Number">
						<div class="invalid-feedback">
							No Telephone harus diisi.
						</div>
					</div>

					<div class="form-group">
						<input type="text" name="wa_penerima" class="form-control to_wa" placeholder="WhatsApp Number">
						<div class="invalid-feedback">
							No Whatapps Number harus diisi.
						</div>
					</div>

					<div class="form-group">
						<select class="select2 form-control to_negara" name="id_negara_penerima" id="id_negara">
							<option value="">Pilih Negara</option>
							<?php
							$country = $this->db->query('SELECT * FROM data_negara')->result_array();
							foreach ($country as $c) {
							?>
								<option value="<?= $c['id']; ?>"><?= $c['nama_negara']; ?></option>
							<?php
							} ?>
						</select>
						<div class="invalid-feedback">
							Negara harus diisi.
						</div>
					</div>

					<div class="form-group">
						<select class="select2 form-control to_province" name="id_provinsi_penerima" id="id_provinsi">
							<option value="">Pilih Provinsi</option>
						</select>
						<div class="invalid-feedback">
							Provinsi harus diisi.
						</div>
					</div>

					<div class="form-group">
						<select class="select2 form-control to_kota" name="id_kota_penerima" id="id_kota">
							<option value="0">Pilih Kota</option>
						</select>
						<div class="invalid-feedback">
							Kota haarus diisi.
						</div>
					</div>
					<div class="form-group">
						<select class="select2 form-control to_kecamatan" name="id_kecamatan_penerima" id="id_kecamatan">
							<option value="0">Pilih Kecamatan</option>
						</select>
						<div class="invalid-feedback">
							Kecamatan haarus diisi.
						</div>
					</div>


					<div class="form-group">
						<input type="text" name="desa_penerima" class="form-control to_desa" placeholder="Desa">
						<div class="invalid-feedback">
							Desa harus diisi.
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="dusun_penerima" class="form-control to_dusun" placeholder="Dusun">
						<div class="invalid-feedback">
							Dusun harus diisi.
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="rw_penerima" class="form-control to_rw" placeholder="RW">
						<div class="invalid-feedback">
							RW harus diisi.
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="rt_penerima" class="form-control to_rt" placeholder="RT">
						<div class="invalid-feedback">
							RT harus diisi.
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="pos_penerima" class="form-control to_pos" placeholder="Kode Pos">
						<div class="invalid-feedback">
							Kode Pos harus diisi.
						</div>
					</div>

					<div class="form-group">
						<textarea class="form-control to_alamat" name="alamat_penerima" placeholder="Alamat Lengkap"></textarea>
						<div class="invalid-feedback">
							Alamat harus diisi.
						</div>
					</div>

					<input type="hidden" name="id_penerima" id="id_penerima">
					<input type="hidden" name="id_pengirim" id="id_pengirim">
				</form>
			</div>
			<div class="col-md-8">
				<h4>BOX</h4>



				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Ukuran Box</label><br>

							<?php foreach ($tarif as $row) {
							?>

								<div class="form-check">
									<input class="form-check-input size-box" type="radio" name="id_size" id="<?= $row['id']; ?>" value="<?= $row['id'] ?>" key="<?= $row['harga']; ?>">
									<label class="form-check-label" for="<?= $row['id']; ?>">
										<?= $row['ukuran']; ?>
									</label>
								</div>

							<?php
							} ?>



						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Isi</label>
							<textarea class="form-control" name="isi" placeholder="Isi dalam box" rows="2"></textarea>
						</div>
						<div class="form-group">
							<label>Note</label>
							<textarea class="form-control" name="note" placeholder="Note" rows="2"></textarea>
						</div>
					</div>
					<div class="col-md-4">

						<div class="form-group mb-2">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<!-- <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mata Uang</button>
									<div class="dropdown-menu">
										<?php foreach ($country as $row) { ?>
											<a class="dropdown-item" href="#"><?= $row['mata_uang']; ?></a>
										<?php } ?>
										
									</div> -->
									<select style="color: #495057;
									background-color: #fff;
									background-clip: padding-box;
									border: 1px solid #ced4da;" name="mata_uang" class="val_mata_uang" required="">
										<option>Mata Uang</option>
										<?php foreach ($country as $row) { ?>
											<option value="<?= $row['mata_uang']; ?>"><?= $row['mata_uang']; ?></option>
										<?php } ?>
									</select>
								</div>
								<input type="number" min="0" name="harga" class="form-control tarif" aria-label="Tarif" placeholder="Tarif">
							</div>
						</div>
						<div class="form-group">
							<label>No Resit Lama</label>
							<input type="text" name="no_resit_lama" class="form-control no_resit_lama">
						</div>
						<div class="form-group">
							<label>Container</label>
							<input type="number" name="container" class="form-control armada" required="">
							<div class="invalid-feedback">
								Container harus diisi.
							</div>
						</div>
					</div>

				</div>




				<div class="form-group">
					<input type="text" name="bayar" class="form-control bayar" placeholder="Total Bayar" style="text-align: right;">
					<div class="invalid-feedback">
						Total bayar harus diisi.
					</div>
				</div>

				<button type="button" class="btn btn-success btn-bayar btn-block">Bayar</button>


			</div>

		</div>
	</div>


	<div class="modal fade" id="cariModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Seach Customer</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="prefetch">
						<input type="text" id="search_box" name="nama_pelanggan" class="form-control nama_pelanggan typeahead" placeholder="Customer Name" autocomplete="false">
					</div>
				</div>
				<input type="hidden" name="inputjenis" id="inputjenis">
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>

	<script src="<?= base_url('assets/select2/js/select2.min.js'); ?>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.select2').select2({
				theme: 'bootstrap4',
			});
		});
		//cari provinsi
		$('input[name=identity_type]').change(function() {
			$('.sender_pasport').attr('placeholder', this.value + ' Number');
		});
		$('input[name=identity_type_penerima]').change(function() {
			$('.to_pasport').attr('placeholder', this.value + ' Number');
		});
		$('#id_negara').change(function() {
			var id_negara = $(this).val();
			get_prov(id_negara);

		});

		//cari kota
		$('#id_provinsi').change(function() {
			var id_prov = $(this).val();
			get_kota(id_prov);

		});

		//cari kecamatan
		$('#id_kota').change(function() {
			var id_kota = $(this).val();
			get_kecamatan(id_kota);

		});

		function get_prov(id_negara, id_prov = '') {
			$.ajax({
				type: "GET",
				url: "<?php echo base_url() . 'admin/get_prov/'; ?>" + id_negara,
				dataType: 'html',
				success: function(data) {
					$('#id_provinsi').html(data);

					if (id_prov != '') {
						$('#id_provinsi').val(id_prov);
					}
				}
			});
		}

		function get_kota(id_prov, id_kota = '') {
			$.ajax({
				type: "GET",
				url: "<?php echo base_url() . 'admin/get_kota/'; ?>" + id_prov,
				dataType: 'html',
				success: function(data) {
					$('#id_kota').html(data);

					if (id_kota != '') {
						$('#id_kota').val(id_kota);
					}
				}
			});
		}

		function get_kecamatan(id_kota, id_kecamatan = '') {
			$.ajax({
				type: "GET",
				url: "<?php echo base_url() . 'admin/get_kecamatan/'; ?>" + id_kota,
				dataType: 'html',
				success: function(data) {
					$('#id_kecamatan').html(data);

					if (id_kecamatan != '') {
						$('#id_kecamatan').val(id_kecamatan);
					}
				}
			});
		}

		function decimals(angka, prefix) {
			var number_string = angka.toString().replace(/[^,\d]/g, ''),
				split = number_string.split(','),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

			if (angka < 0) {
				return '-' + rupiah;
			} else {
				return rupiah;
			}

		}

		function subtotal() {
			var sum = 0;
			$('.subtotal').each(function() {
				sum += parseFloat($(this).attr('key')); // Or this.innerHTML, this.innerText
			});

			$('.total').attr('key', sum);
			$('.total').text(decimals(sum));
		}



		$('.size-box').change(function() {
			var harga = $(this).attr('key');
			$('.tarif').val(harga);
			$('.bayar').val(harga);
		});

		$('.tarif').change(function() {
			var val = $(this).val();
			$('.bayar').val(val);
		});

		$(".bayar").on("keyup", function(event) {
			$(this).val($(this).val().replace(/[^\d].+/, ""));
			if ((event.which < 48 || event.which > 57)) {
				event.preventDefault();

			}


			var total = parseInt($('.total').attr('key'));
			var hasil = parseInt($(this).val()) - parseInt(total);

			$('.txtkembalian').text(decimals(hasil));

		});

		$('.form-add').submit(function(event) {
			event.preventDefault();

			var formData = new FormData(this);


			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'transaksi/add_temp'; ?>",
				data: formData,
				dataType: 'html',
				processData: false,
				contentType: false,
				cache: false,
				async: false,
				success: function(data) {
					$('#load_barang').html(data);
					subtotal();

				}
			});
		});

		function delete_barang(key) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'transaksi/del_temp'; ?>",
				data: {
					key: key
				},
				success: function(data) {
					$('#load_barang').html(data);
					subtotal();
				}
			});
		}

		$('.btn-bayar').click(function(event) {
			var sender_pasport = $('.sender_pasport').val();
			var sender_nama = $('.sender_nama').val();
			var sender_tlp = $('.sender_tlp').val();
			var sender_wa = $('.sender_wa').val();
			var sender_alamat = $('.sender_alamat').val();
			var to_pasport = $('.to_pasport').val();
			var to_nama = $('.to_nama').val();
			var to_tlp = $('.to_tlp').val();
			var to_wa = $('.to_wa').val();
			var to_pos = $('.to_pos').val();
			var to_alamat = $('.to_alamat').val();

			var to_negara = $('.to_negara').val();
			var to_province = $('.to_provinsi').val();
			var to_kota = $('.to_kota').val();
			var bayar = $('.bayar').val();
			var container = $('input[name=container]').val();


			if (sender_pasport == '') {
				$('.sender_pasport').addClass('is-invalid');
				return false;
			} else {
				$('.sender_pasport').removeClass('is-invalid');
			}

			if (sender_nama == '') {
				$('.sender_nama').addClass('is-invalid');
				return false;
			} else {
				$('.sender_nama').removeClass('is-invalid');
			}

			if (to_nama == '') {
				$('.to_nama').addClass('is-invalid');
				return false;
			} else {
				$('.to_nama').removeClass('is-invalid');
			}


			if (container == '') {
				$('input[name=container]').addClass('is-invalid');
				return false;
			} else {
				$('input[name=container]').removeClass('is-invalid');
			}




			$('.form-submit').submit();
			event.preventDefault();
		});

		$('.form-submit').submit(function(event) {
			event.preventDefault();

			var totalgrand = $('input[name=harga]').val();
			swal({
					title: totalgrand,
					text: "Apakah kamu yakin ingin menyelesaikan transaksi ?",
					icon: "warning",

					buttons: {
						cancel: true,
						confirm: "Selesai",
					},
				})
				.then((willDelete) => {
					if (willDelete) {

						var bayar = $('.bayar').val();
						var id_size = $('input[name=id_size]').val();
						var harga = $('input[name=harga]').val();
						var isi = $('textarea[name=isi]').val();
						var note = $('textarea[name=note]').val();
						var container = $('input[name=container]').val();
						var no_resit_lama = $('.no_resit_lama').val();
						var mata_uang = $('.val_mata_uang').val();

						var formData = new FormData(this);

						formData.append('bayar', bayar);
						formData.append('id_size', id_size);
						formData.append('harga', harga);
						formData.append('isi', isi);
						formData.append('note', note);
						formData.append('container', container);
						formData.append('no_resit_lama', no_resit_lama);
						formData.append('mata_uang', mata_uang);

						$.ajax({
							type: "POST",
							url: "<?php echo base_url() . 'transaksi/add_transaksi'; ?>",
							data: formData,
							dataType: 'json',
							processData: false,
							contentType: false,
							cache: false,
							async: false,
							beforeSend: function() {
								$('.btn-bayar').addClass('btn-secondary').removeClass('btn-success').text('Loading');
							},
							success: function(data) {

								if (data.status == true) {
									if (data.agen == true) {
										window.open('<?php echo base_url() . 'printer/invoice/' ?>' + data.id_transaksi, '_blank', 'location=yes,height=1070,width=520,scrollbars=yes,status=yes');
										location.reload();
									} else {
										window.open('<?php echo base_url() . 'printer/barcode_box/' ?>' + data.no_resit, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
										location.reload();
									}


									var id_transaksi = data.id_transaksi;
									$.ajax({
										type: "GET",
										url: "<?php echo base_url() . 'transaksi/send_wa_transaksi/'; ?>" + id_transaksi,
										dataType: 'json',
										success: function(data) {

										}
									});

									$('.btn-bayar').removeClass('btn-secondary').addClass('btn-success').text('Bayar');
								}

							}
						});

					}
				});

		});;

		$('.cari-pelanggan').click(function() {
			var jenis = $(this).attr('key');
			$('#inputjenis').val(jenis);
			$('#cariModal').modal('show');

		});

		var sample_data = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			prefetch: '<?php echo base_url(); ?>transaksi/fetch',
			remote: {
				url: '<?php echo base_url(); ?>transaksi/fetch/%QUERY',
				wildcard: '%QUERY',
			}
		});

		$('#prefetch .typeahead').typeahead(null, {
			nama: 'sample_data',
			display: 'nama',
			source: sample_data,
			limit: 50,
			templates: {
				suggestion: Handlebars.compile('<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>')
			}
		});

		$('#prefetch').on('typeahead:selected', function(e, data) {
			$(this).val('');
			var code = data.id;
			$('.id_customer').val(code);

			$.ajax({
				type: "POST",
				url: "<?php echo base_url() . 'transaksi/cari_pelanggan'; ?>",
				data: {
					code: code
				},
				dataType: 'json',
				success: function(data) {
					var jenis = $('#inputjenis').val();

					if (jenis == 'pengirim') {
						$('#id_pengirim').val(data.id);
						if (data.nik != '') {
							$('.sender_pasport').val(data.nik);
							$('#identity_type1').prop('checked', true);
						} else {
							$('.sender_pasport').val(data.no_pasport);
							$('#identity_type2').prop('checked', true);
						}
						$('.sender_nama').val(data.nama_lengkap);
						$('.sender_tlp').val(data.no_tlp);
						$('.sender_wa').val(data.no_wa);
						$('.sender_alamat').val(data.alamat_lengkap);
						$('.send_negara').val(data.id_negara).trigger('change');

					} else if (jenis == 'penerima') {
						$('#id_penerima').val(data.id);
						if (data.nik != '') {
							$('.to_pasport').val(data.nik);
							$('#identity_type_penerima1').prop('checked', true);
						} else {
							$('.to_pasport').val(data.no_pasport);
							$('#identity_type_penerima2').prop('checked', true);
						}
						$('.to_nama').val(data.nama_lengkap);
						$('.to_tlp').val(data.no_tlp);
						$('.to_wa').val(data.no_wa);
						$('.to_desa').val(data.desa);
						$('.to_dusun').val(data.dusun);
						$('.to_rw').val(data.rw);
						$('.to_rt').val(data.rt);
						$('.to_pos').val(data.kode_pos);
						$('.to_alamat').val(data.alamat_lengkap);

						get_prov(data.id_negara, data.id_provinsi);
						get_kota(data.id_provinsi, data.id_kabupaten);
						get_kecamatan(data.id_kabupaten, data.id_kecamatan);


						$('.to_negara').val(data.id_negara).trigger('change');
					}

					$('#cariModal').modal('hide');



				}
			});


		});
	</script>
</body>

</html>