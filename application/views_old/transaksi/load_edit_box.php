<form class="col-md-12" id="form_edit_box">
    <input type="hidden" name="id_transaksi" value="<?= $transaksi['id'] ?>">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Barang</th>
                <th>Isi</th>
                <th>Container</th>
                <th width="40%">Note</th>
            </tr>
        </thead>
        <?php foreach ($transaksi_detail as $i) : ?>
            <tbody>
                <tr>
                    <td><?= $i['kode_barang'] ?></td>
                    <td><?= $i['isi'] ?></td>
                    <td><?= $i['container'] ?></td>
                    <td>
                        <textarea name="note[<?= $i['id'] ?>]" class="form-control"><?= $i['note'] ?></textarea>
                    </td>
                </tr>
            </tbody>
        <?php endforeach; ?>
    </table>
    <button type="submit" class="btn btn-success">Simpan Perubahan</button>
</form>
<script>
    $('#form_edit_box').submit(function() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'transaksi/act_edit_box'; ?>",
            data: $(this).serialize(),
            success: function(data) {
                $('#modal-action').modal('hide');
            }
        })
        return false;
    })
</script>