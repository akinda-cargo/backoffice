<form id="form_edit_trans" class="col-md-12">
    <input type="hidden" name="id" value="<?= $id_trans ?>">
    <div class="row p-2">
        <div class="col-md-6">
            <h3>Pengirim</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="identity_type1" name="identity_type" <?= ($identity_type == 'KTP' ? 'checked' : '') ?> class="custom-control-input <?= $identity_type ?>" value="KTP">
                        <label class="custom-control-label" for="identity_type1">NIK</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="identity_type2" name="identity_type" <?= ($identity_type == 'Paspor' ? 'checked' : '') ?> class="custom-control-input" value="Paspor">
                        <label class="custom-control-label" for="identity_type2">Paspor</label>
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="no_pasport">KTP</label>
                    <input type="text" name="no_pasport" class="form-control" value="<?= $no_pasport ?>" id="no_passport">
                </div>
                <div class="col-md-12">
                    <label for="nama_pengirim">Nama Lengkap</label>
                    <input type="text" name="nama_pengirim" class="form-control" value="<?= $nama_pengirim ?>" id="nama_pengirim">
                </div>
                <div class="col-md-12">
                    <label for="hp_pengirim">No Telephone</label>
                    <input type="text" name="hp_pengirim" class="form-control" value="<?= $hp_pengirim ?>" id="hp_pengirim">
                </div>
                <div class="col-md-12">
                    <label for="wa_pengirim">No WhatsApp</label>
                    <input type="text" name="wa_pengirim" class="form-control" value="<?= $wa_pengirim ?>" id="wa_pengirim">
                </div>
                <div class="col-md-12">
                    <label for="id_pengirim">Negara</label>
                    <select class="select2 form-control" name="id_pengirim" id="id_pengirim">
                        <option value="">Pilih Negara</option>
                        <?php
                        $country = $this->db->get('data_negara')->result_array();
                        foreach ($country as $c) : ?>
                            <?php if ($kode_negara == $c['id']) : ?>
                                <option value="<?= $c['id']; ?>" selected><?= $c['nama_negara']; ?></option>
                            <?php else : ?>
                                <option value="<?= $c['id']; ?>"><?= $c['nama_negara']; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label for="alamat_pengirim">Alamat Lengkap</label>
                    <textarea name="alamat_pengirim" class="form-control" id="alamat_pengirim"><?= $alamat_pengirim ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h3>Penerima</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="to_identity_type1" name="identity_type_penerima" <?= ($identity_type_penerima == 'KTP' ? 'checked' : null) ?> class="custom-control-input" value="KTP">
                        <label class="custom-control-label" for="to_identity_type1">NIK</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="to_identity_type2" name="identity_type_penerima" <?= ($identity_type_penerima == 'Paspor' ? 'checked' : null) ?> class="custom-control-input" value="Paspor">
                        <label class="custom-control-label" for="to_identity_type2">Paspor</label>
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="no_pasport_penerima">KTP</label>
                    <input type="text" name="no_pasport_penerima" class="form-control" value="<?= $no_pasport_penerima ?>" id="no_pasport_penerima">
                </div>
                <div class="col-md-12">
                    <label for="nama_penerima">Nama Lengkap</label>
                    <input type="text" name="nama_penerima" class="form-control" value="<?= $nama_penerima ?>" id="nama_penerima">
                </div>
                <div class="col-md-12">
                    <label for="hp_penerima">No Telephone</label>
                    <input type="text" name="hp_penerima" class="form-control" value="<?= $hp_penerima ?>" id="hp_penerima">
                </div>
                <div class="col-md-12">
                    <label for="wa_penerima">No WhatsApp</label>
                    <input type="text" name="wa_penerima" class="form-control" value="<?= $wa_penerima ?>" id="wa_penerima">
                </div>
                <div class="col-md-12">
                    <label for="id_negara_penerima">Negara</label>
                    <select class="select2 form-control" name="id_negara_penerima" id="id_negara_penerima">
                        <option value="">Pilih Negara</option>
                        <?php
                        $country = $this->db->get('data_negara')->result_array();
                        foreach ($country as $c) : ?>
                            <?php if ($id_negara_penerima == $c['id']) : ?>
                                <option value="<?= $c['id']; ?>" selected><?= $c['nama_negara']; ?></option>
                            <?php else : ?>
                                <option value="<?= $c['id']; ?>"><?= $c['nama_negara']; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label for="id_provinsi_penerima">Provinsi</label>
                    <select class="select2 form-control related-select" name="id_provinsi_penerima" id="id_provinsi_penerima" data-target="#id_negara_penerima">
                        <option value="">Pilih Provinsi</option>
                        <?php
                        $prov = $this->db->get('t_provinsi')->result_array();
                        foreach ($prov as $c) : ?>
                            <?php if ($id_provinsi_penerima == $c['id']) : ?>
                                <option class="rs<?= $c['id_negara'] ?>" value="<?= $c['id']; ?>" selected><?= $c['nama']; ?></option>
                            <?php else : ?>
                                <option class="rs<?= $c['id_negara'] ?>" value="<?= $c['id']; ?>"><?= $c['nama']; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label for="id_kota_penerima">Kabupaten / Kota</label>
                    <select class="select2 form-control related-select" name="id_kota_penerima" id="id_kota_penerima" data-target="#id_provinsi_penerima">
                        <option value="">Pilih Kabupaten / Kota</option>
                        <?php
                        $kab = $this->db->get('t_kabupaten')->result_array();
                        foreach ($kab as $c) : ?>
                            <?php if ($id_kota_penerima == $c['id']) : ?>
                                <option class="rs<?= $c['id_prov'] ?>" value="<?= $c['id']; ?>" selected><?= $c['nama']; ?></option>
                            <?php else : ?>
                                <option class="rs<?= $c['id_prov'] ?>" value="<?= $c['id']; ?>"><?= $c['nama']; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label for="kode_pos_penerima">Kode POS</label>
                    <input type="text" name="kode_pos_penerima" class="form-control" value="<?= $kode_pos_penerima ?>" id="kode_pos_penerima">
                </div>
                <div class="col-md-12">
                    <label for="alamat_penerima">Alamat Lengkap</label>
                    <textarea name="alamat_penerima" class="form-control" id="alamat_penerima"><?= $alamat_penerima ?></textarea>
                </div>
                
            </div>
        </div>
        <div class="col-md-12">
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            No Resit : <?= $no_resit; ?> <br>
                            Container : <?= $container; ?> <br>
                        </div>
                        <div class="col-md-6">
                            <textarea name="note" class="form-control" placeholder="Note"><?= $note; ?></textarea>
                        </div>
                    </div>
                </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-success">Simpan Perubahan</button>
        </div>
    </div>
</form>
<script>
    load_related();
    $('.select2').select2({
        theme: 'bootstrap4',
    });
    $('#form_edit_trans').submit(function() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'transaksi/act_edit_trans'; ?>",
            data: $(this).serialize(),
            success: function(data) {
                $('#modal-action').modal('hide');
            }
        })
        return false;
    })
</script>