<?php if ($this->session->flashdata('success')) { ?>
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body alert-success">
            <?= $this->session->flashdata('success'); ?>
        </div>
        <!--toast-body-->
    </div>
    <!--toast-->
<?php } ?>

<style type="text/css">
    .text-content {
        margin: 0;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Transaksi</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>

            <div><a href="<?= base_url('transaksi'); ?>" class="btn btn-info btn-sm">Transaksi Baru</a></div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="list_trans" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Tanggal Transaksi</th>
                            <th>Nomor Resit</th>
                            <th>Resit Lama</th>
                            <th style="width: 10%;">Total</th>
                            <th>Box</th>
                            <th>Nama Pengirim</th>
                            <th>Nama Penerima</th>
                            <th>Alamat Pengirim</th>
                            <th>Alamat Tujuan</th>
                            <th>Status</th>
                            <th style="width: 20%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($transaksi as $row) {
                            ?>
                            <tr>
                                <td><?= date('d/m/Y H:i:s',  strtotime($row['created_at'])); ?></td>
                                <td><?= $row['no_resit']; ?></td>
                                <td><?= $row['no_resit_lama']; ?></td>
                                <td class="text-right"><?= decimals($row['total']) . " ($row[mata_uang])"; ?></td>
                                <td><?= $row['isi']; ?></td>
                                <td><?= $row['nama_pengirim']; ?></td>
                                <td><?= $row['nama_penerima']; ?></td>
                                <td><?= $row['alamat_pengirim'] . ' , ' . strtoupper($row['negara_asal']); ?></td>
                                <td><?= $row['alamat_penerima'] . ' , ' . strtoupper($row['negara_tujuan']); ?></td>
                                <td><a href="#" class="btn-status" key="<?= $row['id']; ?>"><?= $row['nama_status']; ?></a></td>
                                <td>

                                    <div class="dropdown">
                                      <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Detail
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item btn-trans my-1" key="<?= $row['id']; ?>" href="#">Ubah</a>
                                        <a class="dropdown-item btn-detail my-1" key="<?= $row['id']; ?>" href="#">Detail Transaksi</a>
                                        <a class="dropdown-item btn-recipt my-1" key="<?= $row['id']; ?>" href="#"><i class="fas fa-print"></i> Struk</a>
                                        <a class="dropdown-item" href="<?= base_url('printer/invoice/' . $row['id']) ?>" target="_blank"><i class="fas fa-print"></i> Invoice</a>
                                        <a class="dropdown-item" href="<?= base_url() . 'printer/barcode_box/' . $row['no_resit']; ?>" target="_blank"><i class="fas fa-print"></i> Stiker Besar</a>
                                        <a class="dropdown-item" href="<?= base_url() . 'printer/barcode_box_kecil/' . $row['no_resit']; ?>" target="_blank"><i class="fas fa-print"></i> Stiker Kecil</a>

                                    </div>
                                </div>

                                <!-- <a href="#" class="btn btn-secondary btn-sm btn-trans my-1" key="<?= $row['id']; ?>">Ubah</a>
                                <a href="#" class="btn btn-primary btn-sm btn-detail my-1" key="<?= $row['id']; ?>">Detail</a>
                                <button type="button" class="btn btn-info btn-sm btn-recipt my-1" key="<?= $row['id']; ?>"><i class="fas fa-print"></i> Struk</button>
                                <a href="<?= base_url('printer/invoice/' . $row['id']) ?>" target="_blank" class="btn btn-success btn-sm my-1"><i class="fas fa-print"></i> Invoice</a>
                                <a href="<?= base_url() . 'printer/barcode_box/' . $row['no_resit']; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fas fa-print"></i> Stiker Besar</a>
                                <a href="<?= base_url() . 'printer/barcode_box_kecil/' . $row['no_resit']; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fas fa-print"></i> Stiker Kecil</a> -->
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>

<div class="modal fade bd-example-modal-lg" id="modal-action" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title-action"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row align-items-center loading" style="display: flex;justify-content: center;min-height: 400px;width: 100%;">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" role="status" style="width: 3rem;height: 3rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            <div class="row" style="padding:10px;" id="load_action">
            </div>
        </div>
    </div>
</div>
<div class="row div-bill" style="position: fixed;height: 100%;width: 101%;top: 0;right: 0;left: 0;bottom: 0;z-index: 9999;display: none;overflow-x: hidden;
overflow-y: auto;"></div>



<!-- /.container-fluid -->
<script type="text/javascript">
    $('.toast').toast({
        'animation': true,
        'autohide': false
    });
    $('.toast').toast('show');

    setTimeout(function() {
        $('.toast').toast('hide');
    }, 5000);

    $('.btn-status').click(function(){
     id = $(this).attr('key');
     $('#load_action').html('');
     $('#modal-title-action').text('Detail Transaksi Status');
     $('#modal-action').modal('show');
     $('.loading').show();
     $('#load_action').load('<?= base_url('transaksi/load_transaksi_status') ?>/' + id, function() {
        $('.loading').hide();
    }); 
 });

    $('.btn-trans').click(function() {
        id = $(this).attr('key');
        $('#load_action').html('');
        $('#modal-title-action').text('Edit Transaksi');
        $('#modal-action').modal('show');
        $('.loading').show();
        $('#load_action').load('<?= base_url('transaksi/load_edit_trans') ?>/' + id, function() {
            $('.loading').hide();
        });
    })
    $('.btn-box').click(function() {
        id = $(this).attr('key');
        $('#load_action').html('');
        $('#modal-title-action').text('Edit Box');
        $('#modal-action').modal('show');
        $('.loading').show();
        $('#load_action').load('<?= base_url('transaksi/load_edit_box') ?>/' + id, function() {
            $('.loading').hide();
        });
    })
    $('.btn-detail').click(function() {
        $('.loading').show();
        $('#load_action').html('');
        $('#modal-title-action').text('Detail Transaksi')
        var id_transaksi = $(this).attr('key');

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() . 'transaksi/detail/'; ?>" + id_transaksi,
            dataType: 'html',
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                $('#load_action').html(data);
                $('.loading').hide();

            }
        });
        $('#modal-action').modal('show');

    });
    
      $(document).ready(function(){
        <?php if($id_transaksi){ ?>
            $('.loading').show();
        $('#load_action').html('');
        $('#modal-title-action').text('Detail Transaksi')
        var id_transaksi = '<?= @$id_transaksi; ?>';

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() . 'transaksi/detail/'; ?>" + id_transaksi,
            dataType: 'html',
            beforeSend: function() {
                $('.loading').show();
            },
            success: function(data) {
                $('#load_action').html(data);
                $('.loading').hide();

            }
        });
        $('#modal-action').modal('show');
        <?php } ?>
    });


    $('.btn-recipt').click(function() {
        var id_transaksi = $(this).attr('key');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'transaksi/get_detail_struk'; ?>",
            data: {
                id_transaksi: id_transaksi
            },
            dataType: 'html',
            success: function(data) {
                $('.div-bill').html(data).show();

                $('.close-bill').click(function() {
                    $('.div-bill').hide();
                });

                $('.print-struk').click(function() {
                    var id_transaksi = $(this).attr('key');

                    window.open('<?php echo base_url() . 'printer/struk/' ?>' + id_transaksi, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');

                });

            }
        });


    });
</script>