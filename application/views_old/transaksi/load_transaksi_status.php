
    <div class="col-md-12" style="padding-left: 15px;padding-right: 15px;">

        <table class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Petugas</th>
                <th>Note</th>
            </tr>
        </thead>
        <?php foreach ($transaksi_status as $i) : ?>
            <tbody>
                <tr>
                    <td><?= $i['tgl_transaksi'] ?></td>
                    <td><strong><?= $i['status'] ?></strong></td>
                    <td><?= $i['nama_petugas'] ?></td>
                    <td><?= $i['note'] ?></td>
                </tr>
            </tbody>
        <?php endforeach; ?>
    </table>

    </div>

