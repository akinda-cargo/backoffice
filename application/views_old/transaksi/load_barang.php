<?php
$total = 0;
foreach ($transaksi as $row) {
	$total += $row['harga'];
?>
	<tr>
		<td><?= $row['ukuran']; ?></td>
		<td><?= $row['isi']; ?></td>
		<td class="text-right subtotal" key="<?= $row['harga']; ?>"><?= decimals($row['harga']); ?></td>
		<td><button type="button" class="btn btn-danger btn-sm" onclick="delete_barang('<?= $row['id_temp'] ?>')">Hapus</button></td>
	</tr>
<?php
} ?>