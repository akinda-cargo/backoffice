/*
FORM SEND AJAX
*/
$(".form-ajax").submit(function () {
    uri_redirect = $(this).data("redirect");
    button = $(this).find("button[type=submit]");
    btn_text = $(this).find("button[type=submit]").html();
    loading = "<i class='fa fa-spinner fa-spin'></i> Loading";

    $.ajax({
        type: "post",
        url: $(this).data("uri"),
        data: $(this).serialize(),
        dataType: "json",
        beforeSend: function () {
            button.attr("disabled", true);
            button.html(loading);
        },
        success: function (data) {
            if (data.res == "success") {
                window.location = uri_redirect;
            } else if (data.res == "success_msg") {
                show_modal(
                    "basic_link",
                    "success",
                    "notification",
                    data.msg,
                    uri_redirect
                );
            } else {
                button.attr("disabled", false);
                button.html(btn_text);
                $("#modal-ajax-fail").modal("show");
            }
        },
        error: function () {
            button.attr("disabled", false);
            button.html(btn_text);
        },
    });
    return false;
});
$(".form-ajax-file").submit(function () {
    uri = $(this).data("uri");
    uri_redirect = $(this).data("redirect");
    button = $(this).find("button[type=submit]");
    btn_text = $(this).find("button[type=submit]").html();
    loading = "<i class='fa fa-spinner fa-spin'></i> Loading";

    $.ajax({
        type: "post",
        url: uri,
        data: new FormData($(this)[0]),
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () {
            button.attr("disabled", true);
            button.html(loading);
        },
        success: function (data) {
            if (data.res == "success") {
                window.location = uri_redirect;
            } else {
                button.attr("disabled", false);
                button.html(btn_text);
                form.find(":input").removeClass("is-invalid");
                form.find(".clone-feedback").remove();
                $.each(data.validation, function (key, data) {
                    form.find(":input[name=" + key + "]").addClass(
                        "is-invalid"
                    );
                    form.find(":input[name=" + key + "]")
                        .next(".invalid-feedback")
                        .text(data);
                    form.find("#place_" + key).prepend(
                        '<div class="alert alert-danger clone-feedback">' +
                            data +
                            "</div>"
                    );
                });
            }
        },
        error: function () {
            button.attr("disabled", false);
            button.html(btn_text);
        },
    });
    return false;
});

/*
RELATED SELECT JS FUNCTION
*/
aOptRelationSelect = [];
function load_related() {
    $(".related-select").each(function () {
        init_related(this);
    });
}
$(".related-select").each(function () {
    init_related(this);
});
function init_related(obj) {
    // get target
    target = $(obj).data("target");
    // get name
    rsName = $(obj).attr("name");
    aOptRelationSelect[".rshead_" + rsName] = [];

    // hide all option
    $(obj)
        .find("option")
        .each(function (k, i) {
            cArray = [];
            cArray["class"] = $(this).attr("class");
            cArray["value"] = $(this).attr("value");
            cArray["text"] = $(this).text();
            cArray["selected"] = $(this).is(":selected ");
            aOptRelationSelect[".rshead_" + rsName].push(cArray);
        });
    // add class
    $(obj).addClass("rshead_" + rsName);
    $(obj).find("option:not([value=''])").remove();

    // fuction checking injection
    $(target).attr(
        "onchange",
        'change_related(this, ".rshead_' + rsName + '")'
    );

    change_related(target, ".rshead_" + rsName);
    console.log(aOptRelationSelect);
}
function change_related(obj, classRs) {
    classArray = $(classRs).attr("class").split(" ");
    // hide all option
    $(classRs).find("option:not([value=''])").remove();
    // get value target
    rsVal = $(obj).val();
    // cheking target value
    if (rsVal == "") {
        // disabled option
        $(classRs).attr("disabled", true);
    } else {
        // console.log(aOptRelationSelect[classRs]);
        // undisabled option
        $(classRs).attr("disabled", false);
        // showing option selected

        $.each(aOptRelationSelect[classRs], function (key, item) {
            if (item.class == "rs" + rsVal) {
                $(classRs).append(
                    '<option value="' +
                        item.value +
                        '" >' +
                        item.text +
                        "</option>"
                );
            }
            if (item.class == "rs" + rsVal && item.selected == true) {
                $(classRs).val(item.value);
                aOptRelationSelect[classRs][key]["selected"] = false;
            }
        });
    }
    // check selected
    // selectedRs = $(classRs).find("option:selected").attr("class");
    // if (selectedRs != "rs" + rsVal) {
    //     $(classRs).val("");
    //     $(classRs).change();
    // }
}
